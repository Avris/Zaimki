export function nowMillis() {
    return Date.now();
}

/**
 * Gets the current UNIX timestamp in seconds.
 */
export function nowSeconds() {
    return Math.floor(Date.now() / 1000);
}

interface Durations {
    SECOND: number;
    MINUTE: number;
    HOUR: number;
    DAY: number;
    WEEK: number;
    YEAR: number;
}

function durations(second: number): Durations {
    const MINUTE = second * 60;
    const HOUR = MINUTE * 60;
    const DAY = HOUR * 24;
    const WEEK = DAY * 7;
    const YEAR = DAY * 365;
    return {
        SECOND: second,
        MINUTE,
        DAY,
        HOUR,
        WEEK,
        YEAR,
    };
}

export const Seconds: Durations = durations(1);
export const Milliseconds: Durations = durations(1000);

export default { nowMillis, nowSeconds, Seconds, Milliseconds };
