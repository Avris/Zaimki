import { isValidUsername, isValidEmail } from "#self/util/filters";

describe("usernames", () => {
    it("valid usernames", () => {
        expect(isValidUsername("username")).toBeTruthy();
        expect(isValidUsername("tecc")).toBeTruthy();
        expect(isValidUsername("andrea")).toBeTruthy();
    });
    it("too short usernames", () => {
        expect(isValidUsername("")).toBeFalsy();
        expect(isValidUsername("a")).toBeFalsy();
        expect(isValidUsername("ab")).toBeFalsy();
        expect(isValidUsername("abc")).toBeFalsy();
        expect(isValidUsername("dlk")).toBeFalsy();
    });
    it("too long usernames", () => {
        expect(isValidUsername("0123456789abcdefghijandsomemorepadding")).toBeFalsy();
        expect(isValidUsername("incrediblylongusernamethatshouldntbe")).toBeFalsy();
    });
});

describe("emails", () => {
    it("valid emails", () => {
        expect(isValidEmail("john.doe@example.org")).toBeTruthy();
        expect(isValidEmail("tecc+dont@tecc.me")).toBeTruthy();
        expect(isValidEmail("john.doe@gmail.com")).toBeTruthy();
    });
    it("invalid emails", () => {
        expect(isValidEmail(".@.")).toBeFalsy();
        expect(isValidEmail("gma+@example.org")).toBeFalsy();
        expect(isValidEmail("skjadlkusjlkdjalsdjklasdjlkasjdljasldjklkajlskjdlkajdksjaldkjskljdklaj@example.org")).toBeFalsy();
        expect(isValidEmail("hello@asdjaklsjdlajsdjajldsjalkdjskjlkadjslkdjlaksjdlkaksjldjaklsjdlkjaslkdjaklsjdlkjasldjlkajsldjakljlsdjlkajskldjlajsdkljalskjdkljasjdlasjdkajlksdjklajsdlkjaklsjdlaksjdlkjaskljdlaksjdlajskldjaklsjdlsakjldkasjldjasldkjlaskldjalksjdksasdasdasdasdasdasdasdasd.example.org")).toBeFalsy();
        expect(isValidEmail("hello..world@example.org")).toBeFalsy();
        expect(isValidEmail("hello++world@example.org")).toBeFalsy();
        expect(isValidEmail("hello.+world@example.org")).toBeFalsy();
        expect(isValidEmail("hello.@example.org")).toBeFalsy();
        expect(isValidEmail("+hlelo@example.org")).toBeFalsy();
    });
});