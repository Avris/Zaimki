/** @type {import('ts-jest').JestConfigWithTsJest} */
export default {
    testEnvironment: "node",
    testMatch: ["<rootDir>/test/**/*.test.ts"],
    transform: {
        "^.+\\.tsx?$": [
            "ts-jest",
            {
                tsconfig: "<rootDir>/test/tsconfig.json",
            },
        ],
    },
    moduleNameMapper: {
        "^#self/(.*)$": "<rootDir>/src/$1",
    },
};
