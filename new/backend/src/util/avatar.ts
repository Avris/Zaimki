import { User } from "#prisma";
import * as base64lib from "js-base64";
import crypto from "crypto";
import {
    AuthenticatorType,
    findAuthenticatorsByUser,
} from "#self/db/authenticator";
import { isNull } from "@pronounspage/common/util";

const { Base64 } = base64lib;

async function tryGetAvatarUrlFromAuthenticators(
    user: User
): Promise<string | undefined> {
    const authenticator = await findAuthenticatorsByUser(
        user.id,
        user.avatarSource as AuthenticatorType
    );
    if (authenticator.length > 0) {
        const first = authenticator[0];
        if (isNull(first)) return undefined;

        if (typeof first.payload !== "object") return undefined;
        return first.payload.avatarCopy ?? first.payload.avatar ?? undefined;
    }
    return undefined;
}
export async function getAvatarUrlForUser(user: User) {
    if (user.avatarSource) {
        if (user.avatarSource === "gravatar") {
            return getGravatarUrlFromEmail(user.email, user.username);
        }
        if (user.avatarSource.startsWith("https://")) {
            return user.avatarSource;
        }

        return await tryGetAvatarUrlFromAuthenticators(user);
    }
    return getFallbackAvatarUrl(user.username);
}

export function getFallbackAvatarUrl(code: string, size: number = 240) {
    const filename = Base64.encode(code)
        .replace(/\+/g, "-")
        .replace(/\//g, "_");
    return `https://avi.avris.it/shape-${size}/${filename}.png`;
}

export function getGravatarUrlFromEmail(
    email: string,
    fallbackCode: string,
    size: number = 240
) {
    const hash = crypto.createHash("md5").update(email).digest("hex");
    return getGravatarUrlFromHash(hash, fallbackCode, size);
}

export function getGravatarUrlFromHash(
    emailHash: string,
    fallbackCode: string,
    size: number = 240
) {
    return `https://www.gravatar.com/avatar/${emailHash}?d=${encodeURIComponent(
        getFallbackAvatarUrl(fallbackCode, size)
    )}&s=${size}`;
}
