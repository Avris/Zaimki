import { Milliseconds } from "@pronounspage/common/util/time";
import { CookieSerializeOptions } from "@fastify/cookie";

export enum Cookie {
    Token = "token",
}

export function cookieSettings(
    expiry: number = Milliseconds.YEAR
): CookieSerializeOptions {
    return {
        expires: new Date(Date.now() + expiry),
    };
}
