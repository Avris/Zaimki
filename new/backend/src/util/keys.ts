import {
    KeyObject,
    createPublicKey,
    createPrivateKey,
    createSecretKey,
    generateKeyPair,
} from "node:crypto";
import fsp from "node:fs/promises";
import log from "#self/log";
import { getConfig } from "#self/config";
import fs from "node:fs";

export async function loadSecretKey(keyPath: string): Promise<KeyObject> {
    log.trace(`Loading secret key from ${keyPath}`);
    const data = await fsp.readFile(keyPath, null);
    return createSecretKey(data);
}

export async function loadPublicKey(keyPath: string): Promise<KeyObject> {
    log.trace(`Loading public key from ${keyPath}`);

    const data = await fsp.readFile(keyPath, null);

    return createPublicKey(data);
}

export async function loadPrivateKey(path: string): Promise<KeyObject> {
    log.trace(`Loading private key from ${path}`);
    const data = await fsp.readFile(path, null);
    return createPrivateKey(data);
}

let publicKey: KeyObject;
let privateKey: KeyObject;

function generateKeys(): Promise<{ public: KeyObject; private: KeyObject }> {
    return new Promise((resolve, reject) => {
        generateKeyPair(
            "rsa",
            {
                modulusLength: 4096,
            },
            (err, publicKey, privateKey) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve({
                    public: publicKey,
                    private: privateKey,
                });
            }
        );
    });
}

export async function loadKeys(): Promise<void> {
    const config = getConfig();

    const publicKeyExists = fs.existsSync(config.security.publicKey);
    const privateKeyExists = fs.existsSync(config.security.privateKey);

    if (publicKeyExists !== privateKeyExists) {
        throw new Error(
            `One of the keys exist (the ${
                publicKeyExists ? "private" : "public"
            } one, specifically), but not the other. Perhaps there's a typo?`
        );
    }
    if (!publicKeyExists) {
        // Since both are the same, we can check one to check both
        if (config.security.allowGenerateKeys) {
            log.info(`Generating keys`);
            const keys = await generateKeys();
            publicKey = keys.public;
            privateKey = keys.private;
            log.info(`Keys generated - writing keys`);
            await fsp.writeFile(
                config.security.publicKey,
                publicKey.export({ type: "pkcs1", format: "pem" })
            );
            await fsp.writeFile(
                config.security.privateKey,
                privateKey.export({ type: "pkcs1", format: "pem" })
            );
        } else {
            throw new Error(
                "Key files do not exist, and automatic key generation is disabled."
            );
        }
    } else {
        publicKey = await loadPublicKey(config.security.publicKey);
        privateKey = await loadPrivateKey(config.security.privateKey);
    }
}

export function getPublicKey(): KeyObject {
    if (publicKey == null) {
        throw new Error("Attempted to get public key before keys were loaded");
    }

    return publicKey;
}

export function getPrivateKey(): KeyObject {
    if (privateKey == null) {
        throw new Error("Attempted to get public key before keys were loaded");
    }
    return privateKey;
}
