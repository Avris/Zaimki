import { EMAIL_REGEX } from "@pronounspage/common/util/filters";
import { Resolver } from "dns/promises";
const dns = new Resolver();

/**
 * E-mail validator.
 * Functions similarly to the common package's `isValidEmail` function,
 * but also checks for MX records.
 */
export async function validateEmail(email: string): Promise<boolean> {
    if (email.endsWith(".oauth")) {
        return false;
    }

    const match = EMAIL_REGEX.exec(email);
    if (match == null) {
        return false;
    }

    const localPart = match[1];
    if (localPart.length > 64) {
        return false;
    }

    const hostname = match[5];
    if (hostname.length > 255) {
        return false;
    }

    try {
        const addresses = await dns.resolveMx(hostname);
        return addresses.length > 0;
    } catch {
        return false;
    }
}
export function obfuscateEmail(email: string): string | null {
    const match = EMAIL_REGEX.exec(email);
    if (match == null) {
        return null;
    }
    const localPart = match[1],
        hostname = match[5];

    const hostnameParts = hostname.split(".");
    const tld = hostnameParts[hostnameParts.length - 1];
    if (tld.toLowerCase() === "oauth") {
        return null;
    }
    const obfuscatedLocalPart = localPart.substring(
        0,
        localPart.length <= 5 ? 1 : 3
    );

    return `${obfuscatedLocalPart}`;
}
