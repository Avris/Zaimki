import speakeasy from "speakeasy";

export function verify(token: string, secret: string): boolean {
    return speakeasy.totp.verify({
        token,
        secret,
        encoding: "base32",
        window: 6,
    });
}
