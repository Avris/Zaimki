import { getConfig } from "#self/config";

export async function validateCaptcha(token: string) {
    const config = getConfig();
    const res = await fetch(
        "https://challenges.cloudflare.com/turnstile/v0/siteverify",
        {
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded",
            },
            body: `response=${encodeURIComponent(
                token
            )}&secret=${encodeURIComponent(config.turnstile.secret)}`,
        }
    );
    const body = await res.json();
    return body["success"] as boolean;
}
