import { pino, LoggerOptions } from "pino";
import { getConfig, Environment } from "#self/config";

const loggerConfigurations = {
    [Environment.DEVELOPMENT]: {
        level: "debug",
        transport: {
            target: "pino-pretty",
            options: {
                translateTime: "HH:MM:ss Z",
                ignore: "pid,hostname",
            },
        },
    },
    [Environment.PRODUCTION]: {
        level: "info",
    },
} satisfies Record<Environment, LoggerOptions>;

let environment, logLevel;
try {
    const config = getConfig();
    environment = config.environment;
    logLevel = config.logLevel;
} catch (e) {
    environment = Environment.DEVELOPMENT;
}

const configuration = loggerConfigurations[environment];
if (logLevel) {
    configuration.level = logLevel;
}
export const log = pino(configuration);

export default log;
