import type {
    FastifyInstance,
    FastifyPluginAsync,
    FastifyPluginOptions,
    FastifyReply,
    FastifyRequest,
    RawReplyDefaultExpression,
    RawRequestDefaultExpression,
    RawServerDefault,
} from "fastify";
import type { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import type { log } from "#self/log";
import type * as prisma from "#prisma";

type RawServer = RawServerDefault;
type RequestExpr = RawRequestDefaultExpression<RawServer>;
type ReplyExpr = RawReplyDefaultExpression<RawServer>;
type Log = typeof log;
type TypeProvider = TypeBoxTypeProvider;

declare global {
    type AppInstance = FastifyInstance<
        RawServer,
        RequestExpr,
        ReplyExpr,
        typeof log,
        TypeProvider
    >;
    type AppPluginAsync<
        Options extends FastifyPluginOptions = Record<never, never>,
    > = FastifyPluginAsync<Options, RawServer, TypeProvider, Log>;
    // type AppRequest = Fastify;
    type AppReply = FastifyReply<RawServer, RequestExpr, ReplyExpr>;

    type Awaitable<T> = T | PromiseLike<T>;
}

declare module "fastify" {
    interface FastifyRequest {
        v1: import("#self/server/v1").V1Data;
    }
}
