/*
 * This file is for interop with the old locales.js file to reduce duplication.
 */

import { getConfig } from "#self/config";
import * as path from "node:path";
import * as suml from "@pronounspage/common/suml";
import fsp from "node:fs/promises";
import log from "#self/log";
import * as csv from "csv-parse/sync";
import {
    capitalizeFirstLetter,
    compileTemplate,
    isNotBlank,
    isNotNull,
    parseBool,
    toJSONGeneric,
} from "@pronounspage/common/util";
import { DepGraph } from "dependency-graph";
import time from "@pronounspage/common/util/time";

export interface LocaleDescription {
    code: string;
    name: string;
    url: string;
    published: boolean;
    pseudo: boolean;
    /**
     * This locale's language family.
     *
     * @experimental
     * This is currently not used for published versions.
     */
    symbol?: string;
    /**
     * This locale's language family.
     *
     * @experimental
     * This is currently not used for published versions.
     *
     * @privateRemarks
     * This could technically be an enum but for now it's just a string as there
     * doesn't seem to be any apparent benefit to including locales
     */
    family?: string;
}

let expectedTranslations: Array<string>;

let loadedDescriptions: Array<LocaleDescription>;
let indexedDescriptions: Record<string, LocaleDescription>;
let activeLocaleDescriptions: Array<LocaleDescription>;

export async function loadLocaleDescriptions(
    force: boolean = false
): Promise<Array<LocaleDescription>> {
    if (loadedDescriptions == undefined || force) {
        const config = getConfig();
        loadedDescriptions = (
            await import(
                "file://" + path.resolve(config.locale.dataPath, "locales.ts")
            )
        ).default;
        for (const description of loadedDescriptions) {
            description.pseudo = false;
        }

        const fallbackLocale = config.locale.fallbackLocale;

        if (!loadedDescriptions.some((v) => v.code === fallbackLocale)) {
            loadedDescriptions.push({
                code: fallbackLocale,
                name: `GENERATED: ${fallbackLocale}`,
                pseudo: true,
                published: false,
                url: `${fallbackLocale}.pseudo.pronouns.page`, // A pseudovalue
            });
            log.debug(`adding pseudolocale ${config.locale.fallbackLocale}`);
        }

        // The following code was deprecated because someone decided to do something
        // somewhat reasonable in a commit not too long ago. Sigh...
        /*expectedTranslations = (
            await import(
                "file://" +
                    path.resolve(
                        config.locale.dataPath,
                        "expectedTranslations.js"
                    )
            )
        ).default;*/

        refreshLocaleDescriptionCaches();
    }
    return loadedDescriptions;
}

export function refreshLocaleDescriptionCaches() {
    indexedDescriptions = Object.fromEntries(
        loadedDescriptions.map((v) => [v.code, v])
    );
    activeLocaleDescriptions = [];
    for (const locale of loadedDescriptions) {
        if (willBeActive(locale)) {
            activeLocaleDescriptions.push(locale);
        }
    }
}

export function getLocaleDescriptions(): Array<LocaleDescription> {
    return loadedDescriptions;
}

export function getActiveLocaleDescriptions(): Array<LocaleDescription> {
    return activeLocaleDescriptions;
}

export function getExpectedTranslations(): Array<string> {
    if (!Array.isArray(expectedTranslations)) {
        throw new Error("Expected translations have not been loaded yet");
    }
    return expectedTranslations;
}

export function getLocaleDescription(
    localeCode: string
): LocaleDescription | null {
    return indexedDescriptions[localeCode];
}

function willBeActive(locale: LocaleDescription): boolean {
    if (locale == undefined) return false;
    if (locale.pseudo) return false;
    if (!locale.published && !getConfig().locale.allowUnpublished) {
        return false;
    }

    return true;
}

export function isLocaleActive(localeCode: string): boolean {
    // TODO: Not use iteration for this - just check with the config or something.
    const desc = indexedDescriptions[localeCode];
    return desc ? willBeActive(desc) : false;
}

export interface PronounVariant {
    written: string;
    pronounced: string;
}
export interface PronounForm extends PronounVariant {
    variants?: Array<PronounVariant>;
}
export interface Pronoun {
    keys: Array<string>;
    description?: string;
    history?: string;
    source?: string;
    isNormative: boolean;
    isPlural: boolean;
    isPluralHonorific: boolean;
    isPronounceable: boolean;

    thirdForm?: string;
    smallForm?: string;

    forms: Record<string, PronounForm>;

    canonicalName: string;
}

export function allPronounVariants(form: PronounForm): Array<PronounVariant> {
    const arr: Array<PronounVariant> = [
        {
            written: form.written,
            pronounced: form.pronounced,
        },
    ];
    if (form.variants) {
        form.variants.map((v) => arr.push(v));
    }
    return arr;
}

export function parsePronounFromParts(
    parts: Array<string>,
    forms: Array<string>
): Pronoun | null {
    if (parts.length !== forms.length) {
        return null;
    }

    const pronounForms: Pronoun["forms"] = {};
    for (let i = 0; i < forms.length; i++) {
        const [written, pronounced] = parts[i].split("|");

        const writtenVariants = written.split("&");
        const pronouncedVariants = pronounced?.split("&") ?? [];

        const current: PronounForm = {
            written: writtenVariants.shift()!,
            pronounced: pronouncedVariants.shift()!,
        };

        if (writtenVariants.length || pronouncedVariants.length) {
            const length = Math.max(
                writtenVariants.length,
                pronouncedVariants.length
            );
            const lastWritten = writtenVariants.length - 1,
                lastPronounced = pronouncedVariants.length - 1;
            const variants: PronounForm["variants"] = [];
            for (let i = 0; i < length; i++) {
                variants.push({
                    written:
                        writtenVariants[Math.max(i, lastWritten)] ??
                        current.written,
                    pronounced:
                        pronouncedVariants[Math.max(i, lastPronounced)] ??
                        current.pronounced,
                });
            }
            current.variants = variants;
        }
        pronounForms[forms[i]] = current;
    }

    return {
        forms: pronounForms,
        isNormative: false,
        keys: [],
        isPronounceable: false,
        isPlural: false,
        isPluralHonorific: false,
        canonicalName: `${parts[0]}/${parts[1]}`,
    };
}
export function parsePronounFromString(s: string, forms: Array<string>) {
    return parsePronounFromParts(s.split("/"), forms);
}

export interface PronounExample {
    singular?: string;
    plural?: string;
    isHonorific: boolean;
}

interface LocaleLogo<PNG> {
    png: PNG;
    path: string;
    text: string;
}
export type LocaleLogoSource = LocaleLogo<Buffer>;
export type LocaleLogoUrl = LocaleLogo<string>;

export interface Translations extends Record<string, Translations | string> {}

export class Locale {
    private _code: string;
    private _fallbackLocale: string | null;
    private _dataDirectory: string;
    private _lastLoaded: number | null = null;

    constructor(
        code: string,
        fallbackLocale: string | null,
        dataDirectory: string
    ) {
        this._code = code;
        this._dataDirectory = dataDirectory;
        this._fallbackLocale = fallbackLocale;
    }

    private path(pathParts: Array<string>) {
        return path.resolve(this._dataDirectory, ...pathParts);
    }

    private async readFile(parts: Array<string>): Promise<string>;
    private async readFile<T>(
        parts: Array<string>,
        parser: (value: Buffer) => T
    ): Promise<T>;
    private async readFile<T>(
        pathParts: Array<string>,
        parser?: (value: Buffer) => T
    ) {
        // Some may complain about this
        const filePath = this.path(pathParts);
        try {
            log.trace(`[${this.code}] Loading file ${filePath}`);
            const data = await fsp.readFile(filePath);
            if (parser) {
                return parser(data);
            } else {
                return data.toString("utf-8");
            }
        } catch (e) {
            const err = new Error(
                `[${this.code}] Could not load file ${filePath}: ${e}`
            );
            err.cause = e;
            throw err;
        }
    }

    private async importFile(parts: Array<string>): Promise<any> {
        const filePath = this.path(parts);
        try {
            log.trace(`[${this.code}] Importing file ${filePath} for locale`);
            return await import(`file://${filePath}`);
        } catch (e) {
            const err = new Error(
                `[${this.code}] Could not import file ${filePath}: ${e}`
            );
            err.cause = e;
            throw err;
        }
    }

    private _config: any | null = null;
    private _translations: Translations | null = null;
    private _tosContentPart: string | null = null;
    private _pronouns: Array<Pronoun> = [];
    private _pronounsByAlias: Record<string, number> = {};

    private _examples: Array<PronounExample> = []; // TODO: Type these properly
    private _morphemes: Array<string> = [];
    private _logo: LocaleLogoUrl | null = null;
    private _logoSource: LocaleLogoSource | null = null;

    public async load() {
        this._lastLoaded = time.nowMillis();
        const tsvParse = (data: Buffer) =>
            csv.parse(data, {
                columns: true,
                cast: false,
                relaxColumnCount: true,
                relaxQuotes: true,
                delimiter: "\t",
            });
        const sumlParse = (v: Buffer) => suml.parse(v.toString("utf-8"));
        this._config = await this.readFile(["config.suml"], sumlParse);
        this._translations = (await this.readFile(
            ["translations.suml"],
            sumlParse
        )) as Translations;
        const violations = this.getTranslation(
            "terms.content.content.violationsExamples",
            1
        ) as Translations;
        if (violations != null) {
            delete violations["miscellaneous"];
            this._tosContentPart =
                this.getTranslation("terms.content.content.violations") +
                " " +
                Object.values(violations).join(", ");
        }

        const logoDir = "img/logo/";
        const logoSource = (this._logoSource = {
            png: await this.readFile([logoDir, "logo-full.png"], (v) => v),
            path: await this.readFile([logoDir, "logo-full-path.svg"]),
            text: await this.readFile([logoDir, "logo-full-text.svg"]),
        });
        this._logo = {
            png:
                "data:image/png;base64," + logoSource.png.toString("base64url"),
            path: "data:image/svg+xml," + encodeURIComponent(logoSource.path),
            text: "data:image/svg+xml," + encodeURIComponent(logoSource.text),
        };

        this._morphemes = (await this.importFile(["pronouns/morphemes.ts"]))
            .default as string[];
        this._pronouns = [];
        this._pronounsByAlias = {};
        const pronouns = await this.readFile(
            ["pronouns/pronouns.tsv"],
            tsvParse
        );
        for (const pronoun of pronouns) {
            const partial: Partial<Pronoun> = { forms: {} };
            for (const [key, value] of Object.entries<string>(pronoun)) {
                switch (key) {
                    case "key":
                        partial.keys = value.split(",");
                        break;
                    case "normative":
                        partial.isNormative = parseBool(value);
                        break;
                    case "plural":
                        partial.isPlural = parseBool(value);
                        break;
                    case "pluralHonorific":
                        partial.isPluralHonorific = parseBool(value);
                        break;
                    case "pronounceable":
                        partial.isPronounceable = parseBool(value);
                        break;
                    case "description":
                    case "history":
                    case "thirdForm":
                    case "smallForm":
                        partial[key] = isNotBlank(value) ? value : undefined;
                        break;
                    case "sourcesInfo":
                        partial.source = value;
                        break;
                    default:
                        if (!this._morphemes.includes(key)) {
                            throw new Error(
                                `[${this.code}] Unknown key ${key}`
                            );
                        }
                        const [written, pronounced] = value.split("|");
                        partial.forms![key] = { written, pronounced };
                        break;
                }
            }
            const i = this._pronouns.length;
            const obj = partial as Pronoun;
            this._pronouns.push(obj);
            for (const key of obj.keys) {
                this._pronounsByAlias[key] = i;
            }
            obj.canonicalName = obj.keys[0];
        }
        const examples = await this.readFile(
            ["pronouns/examples.tsv"],
            tsvParse
        );
        this._examples = [];
        for (const example of examples) {
            this._examples.push({
                singular: isNotBlank(example.singular)
                    ? example.singular
                    : undefined,
                plural: isNotBlank(example.plural) ? example.plural : undefined,
                isHonorific: example.isHonorific,
            } satisfies PronounExample);
        }
    }

    public get code() {
        return this._code;
    }

    public get description() {
        return indexedDescriptions[this.code];
    }

    public get fallbackLocale(): Locale | null {
        return this._fallbackLocale ? getLocale(this._fallbackLocale) : null;
    }

    public get lastLoaded(): number {
        return this._lastLoaded ?? 0;
    }

    public get tosContentPart(): string {
        return (this._tosContentPart ?? this.fallbackLocale?.tosContentPart)!;
    }
    public get logo(): LocaleLogoUrl {
        return this._logo!;
    }
    public get logoSource(): LocaleLogoSource {
        return this._logoSource!;
    }

    public get config() {
        return this._config;
    }

    public get pronouns() {
        return Object.values(this._pronouns);
    }

    public pronoun(key: string): Pronoun | null {
        const index = this._pronounsByAlias[key];
        if (index == null) {
            return null;
        }
        return this._pronouns[index];
    }

    public pronounNameVariants(pronoun: Pronoun): Array<string> {
        // NOTE(tecc): This is an attempt at decoding the code from classes.js.
        //             I make no guarantee regarding the accuracy.
        const variants: Set<string> = new Set();

        function allVariants(form: PronounForm): Array<string> {
            return [
                form.written,
                ...(form.variants?.map((v) => v.written) ?? []),
            ];
        }

        const firstMorpheme = allVariants(pronoun.forms[this.morphemes[0]]);
        if (this.morphemes.length === 1) {
            return firstMorpheme;
        }

        const secondMorpheme = allVariants(pronoun.forms[this.morphemes[1]]);
        const thirdMorpheme =
            this.morphemes.length > 2
                ? allVariants(pronoun.forms[this.morphemes[2]])
                : [];

        let thirdFormMorpheme = pronoun.thirdForm
            ? allVariants(pronoun.forms[pronoun.thirdForm])
            : [];

        let thirdMorphemeThreeForms = thirdMorpheme;
        if (this.code === "ru" || this.code === "ua") {
            thirdMorphemeThreeForms = thirdMorpheme.map((x) => `[-${x}]`);
        }

        for (let i = 0; i < firstMorpheme.length; i++) {
            let firstPart = firstMorpheme[i];
            let secondPart =
                secondMorpheme[Math.min(i, secondMorpheme.length - 1)];
            if (
                firstPart === secondPart &&
                thirdMorpheme.length &&
                !this.config.pronouns.threeForms
            ) {
                secondPart =
                    thirdMorpheme[Math.min(i, thirdMorpheme.length - 1)];
            }
            let variant = firstPart + "/" + secondPart;
            if (this.config.pronouns.threeForms) {
                variant +=
                    "/" +
                    thirdMorphemeThreeForms[
                        Math.min(i, thirdMorphemeThreeForms.length - 1)
                    ];
            } else if (pronoun.thirdForm) {
                variant +=
                    "/" +
                    thirdFormMorpheme[
                        Math.min(i, thirdFormMorpheme.length - 1)
                    ];
            }

            variants.add(variant);
        }
        return [...variants];
    }
    public pronounName(pronoun: Pronoun, separator: string = ","): string {
        return this.pronounNameVariants(pronoun).join(separator);
    }

    public get examples() {
        return this._examples;
    }
    public get morphemes() {
        return this._morphemes;
    }

    public getTranslation(
        key: string,
        fallbacksRemaining = Infinity
    ): Translations | string | undefined {
        let translation = this.getTranslationNoFallback(key);
        if (translation != undefined) {
            return translation;
        }

        if (this._fallbackLocale == undefined || fallbacksRemaining < 1) {
            return undefined;
        }
        const fallback = getLocale(this._fallbackLocale, true);
        if (fallback == null) {
            log.warn(
                `Could not find locale ${this._fallbackLocale} as fallback for locale ${this.code}`
            );
            return undefined;
        }
        return fallback.getTranslation(key, fallbacksRemaining - 1);
    }

    public getTranslationNoFallback(
        key: string
    ): Translations | string | undefined {
        let current: Translations | string | null = this._translations;
        if (current == undefined) return undefined;
        for (const part of key.split(".")) {
            if (typeof current === "string") return undefined;
            current = current[part];
            if (current == undefined) {
                // log.warn(`undefined key '${key}' in locale ${this.code}`);
                return undefined;
            }
        }
        return current;
    }

    public getTranslationKeys(): Array<string> {
        const keys: Array<string> = [];
        const pushKeys = (object: Translations, prefix: string) => {
            for (const key of Object.keys(object)) {
                const value = object[key];
                if (typeof value === "string") {
                    keys.push(prefix + key);
                } else if (typeof value === "object") {
                    pushKeys(value, prefix + "." + key);
                }
            }
        };
        pushKeys(this._translations!, "");
        return keys;
    }

    public toJSON() {
        return toJSONGeneric(this);
    }
}

export function examplesFor(
    pronoun: Pronoun,
    examples: Array<PronounExample>
): Array<string> {
    const finished = [];

    const templating: Record<string, string> = {};
    for (const [key, value] of Object.entries(pronoun.forms)) {
        templating[key] = value.written;
        templating[`'${key}`] = capitalizeFirstLetter(value.written);
    }

    for (const example of examples) {
        let template = example.singular;
        if (pronoun.isPlural && example.plural) {
            template = example.plural;
        }
        if (template == null) {
            continue;
        }
        finished.push(compileTemplate(template, templating));
    }
    return finished;
}

const loadedLocales: Record<string, Locale> = {};

/**
 * Load a locale.
 * This will only succeed if the locale is both
 * @param localeCode The locale code
 * @param allowAnyLocales Whether to allow any locales (even pseudolocales).
 *                        Pseudolocales are locales that don't actually exist,
 *                        not to be confused with unpublished locales.
 */
export function getLocale(
    localeCode: string,
    allowAnyLocales: boolean = false
): Locale | null {
    const config = getConfig();
    // If it allows pseudolocales, anything goes
    if (allowAnyLocales) {
        if (indexedDescriptions[localeCode] == undefined) {
            return null;
        }
    } else {
        if (!isLocaleActive(localeCode)) {
            return null;
        }
    }
    let locale = loadedLocales[localeCode];
    if (locale == null) {
        locale = new Locale(
            localeCode,
            localeCode === config.locale.fallbackLocale
                ? null
                : config.locale.fallbackLocale,
            path.resolve(config.locale.dataPath, localeCode)
        );
        loadedLocales[localeCode] = locale;
    }
    return locale;
}

export async function loadAllLocales() {
    const config = getConfig();
    const descriptions = await loadLocaleDescriptions();

    const localesToBeLoaded = descriptions
        .map((v) => getLocale(v.code, true))
        .filter(isNotNull) as Array<Locale>;

    // Using a dependency graph might be overkill, but it's future-proof: in case
    // the locales can have different fallbacks, this will ensure that everything is
    // loaded in exactly the order they need to be.
    const graph = new DepGraph();
    for (const locale of localesToBeLoaded) {
        // log.trace(`adding locale ${locale.code} to dependency graph`);
        graph.addNode(locale.code, locale);
        const fallback = locale.fallbackLocale;
        if (fallback) {
            if (!graph.hasNode(fallback.code)) {
                graph.addNode(fallback.code);
            }
            graph.addDependency(locale.code, fallback.code);
        }
    }

    if (graph.entryNodes().length < 1) {
        throw new Error("All locales depend on another - cannot load.");
    }

    const overallOrder = graph.overallOrder().reverse();
    log.debug(`${overallOrder}`);
    // const promises = [];
    for (const code of overallOrder) {
        const locale = getLocale(code, true);
        if (locale) {
            await locale
                .load()
                .then(() =>
                    log.debug(`Successfully loaded locale ${locale.code}`)
                );
        }
    }
    // await Promise.all(promises);
    log.info("All active locales have been loaded into memory");

    // NEW! Special step to add expectedTranslations. Yay.
    const fallback = getLocale(config.locale.fallbackLocale);
    if (fallback) {
        expectedTranslations = fallback.getTranslationKeys();
    }
}
