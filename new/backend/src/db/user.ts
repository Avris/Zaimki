import { db, User } from "#self/db";
import { getAvatarUrlForUser } from "#self/util/avatar";
import { normalise } from "@pronounspage/common/util";

export interface ExtendedUser extends User {
    avatarUrl: string;
    hasMfa: boolean;
}

export async function extendUser(
    user: User & Partial<ExtendedUser>
): Promise<ExtendedUser> {
    user.avatarUrl = await getAvatarUrlForUser(user);
    return user as ExtendedUser;
}

export async function getUserById(id: string): Promise<ExtendedUser | null> {
    const user = await db.user.findFirst({
        where: {
            id: id,
        },
    });
    if (user == null) {
        return null;
    }
    return await extendUser(user);
}
export async function getUserByUsername(
    username: string
): Promise<ExtendedUser | null> {
    const user = await db.user.findFirst({
        where: {
            usernameNorm: normalise(username),
        },
    });
    if (user == null) {
        return null;
    }
    return await extendUser(user);
}
