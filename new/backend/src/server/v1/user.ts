import {
    ApiError,
    localeSpecific,
    replyError,
    TokenData,
} from "#self/server/v1";
import { db } from "#self/db";
import {
    AuthenticatorType,
    findAuthenticatorById,
    findLatestEmailAuthenticator,
    invalidateAuthenticator,
    saveAuthenticator,
} from "#self/db/authenticator";
import { Type } from "@sinclair/typebox";
import { validateCaptcha } from "#self/util/turnstile";
import {
    isNotNull,
    makeId,
    normalise,
    NUMERIC_CHARS,
} from "@pronounspage/common/util";
import {
    isValidEmail,
    isValidUsername,
} from "@pronounspage/common/util/filters";
import { Environment, getConfig } from "#self/config";
import { obfuscateEmail, validateEmail } from "#self/util/email";
import * as jwt from "#self/util/jwt";
import { deduplicateEmail, sendLocalisedEmail } from "#self/util/mailer";
import { getLocale } from "#self/locales";
import { isInBanArchive } from "#self/db/ban";
import { processToken } from "#self/server/util/v1";
import {
    createUserJwt,
    isAuthenticatedUserJwt,
    issueUserJwt,
    isUserJwt,
} from "#self/util/auth";
import { getUserById } from "#self/db/user";
import { Cookie } from "#self/util/cookie";

function isSpamLike(usernameOrEmail: string) {
    return usernameOrEmail.length > 128;
}

interface LoginPayload {
    username: string | null;
    email: string | null;
    emailObfuscated: string;
    codeKey: string;
}

export const plugin = async function (app) {
    const config = getConfig();
    app.get(
        "/:locale/user/current",
        {
            schema: {
                params: localeSpecific,
                querystring: Type.Object({
                    no_cookie: Type.Optional(Type.Boolean()),
                }),
            },
        },
        async (req, reply) => {
            const noCookie = req.query.no_cookie ?? false;
            const token = req.v1.token;
            req.log.info({ noCookie, token });
            if (token == null) {
                if (!noCookie) {
                    reply.clearCookie(Cookie.Token);
                }
                return reply.send(null);
            }
            const payload = token.payload;
            if (!isAuthenticatedUserJwt(payload)) {
                if (!noCookie) {
                    reply.clearCookie(Cookie.Token);
                }
                return reply.send(null);
            }

            const user = await getUserById(payload.id);
            if (user == null) {
                if (!noCookie) {
                    reply.clearCookie(Cookie.Token);
                }
                return reply.send(null);
            }

            // I haven't a clue why the original code does this,
            // but in my effort to be as compatible as possible it shall remain.
            const newTokenString = issueUserJwt(user, false);
            if (!noCookie) {
                reply.cookie(Cookie.Token, newTokenString);
            }
            const newToken = jwt.validateToken(newTokenString);

            return reply.send({
                id: user.id,
                username: user.username,
                usernameNorm: user.usernameNorm,
                email: user.email,
                roles: user.roles,
                avatarSource: user.avatarSource,
                avatar: user.avatarUrl,
                bannedReason: user.bannedReason,
                bannedTerms: user.bannedTerms,
                lastActive: user.lastActive,
                inactiveWarning: user.inactiveWarning,
                adminNotifications: user.adminNotifications,
                loginAttempts: user.loginAttempts,
                socialLookup: user.socialLookup,
                mfa: user.hasMfa,
                // authenticated: true is a guarantee from isAuthenticatedUserJwt,
                authenticated: true,
                iat: newToken.iat,
                exp: newToken.exp,
                aud: newToken.aud,
                iss: newToken.iss,
                token: newToken,
            });
        }
    );
    app.post(
        "/:locale/user/init",
        {
            schema: {
                params: localeSpecific,
                body: Type.Object({
                    usernameOrEmail: Type.String(),
                    captchaToken: Type.String(),
                }),
            },
        },
        async (req, reply) => {
            const locale = getLocale(req.params.locale);
            if (locale === null) {
                return replyError(reply, ApiError.INVALID_LOCALE);
            }

            let identifier = req.body.usernameOrEmail;
            if (isSpamLike(identifier)) {
                // This is different from how the old code does it
                // The old code does `req.socket.end()` (equivalent is `req.raw.socket.end()`)
                return replyError(reply, ApiError.BAD_REQUEST);
            }

            // In the dev environment, you can append a + to the end of the login identifier.
            // This will make the code always be 999999.
            let isTest =
                identifier.endsWith("+") &&
                config.environment === Environment.DEVELOPMENT;
            identifier = isTest
                ? identifier.substring(0, identifier.length - 1)
                : identifier;

            const isEmail = isValidEmail(identifier);
            const isUsername = isValidUsername(identifier);
            if (!isEmail || !isUsername) {
                return replyError(reply, ApiError.USER_NOT_FOUND);
            }

            // We validate captcha after because it's considerably less time-consuming hypothetically
            // This requires an entire network request
            if (!(await validateCaptcha(req.body.captchaToken))) {
                return replyError(reply, ApiError.BAD_REQUEST); // Yet another difference: old code has a separate error for captcha failures
            }

            const identifierNormalised = normalise(identifier);
            const user = await db.user.findFirst({
                where: isEmail
                    ? {
                          email: identifierNormalised,
                      }
                    : {
                          usernameNorm: identifierNormalised,
                      },
            });
            const userWasFound = isNotNull(user);

            if (!userWasFound && !isEmail) {
                return replyError(reply, ApiError.USER_NOT_FOUND);
            }

            // TODO: Login attempts

            const username = isEmail
                    ? userWasFound
                        ? user.username
                        : null
                    : identifier,
                email = isEmail ? identifierNormalised : user!.email,
                code = isTest ? "999999" : makeId(6, NUMERIC_CHARS);

            if (!(await validateEmail(email))) {
                return replyError(reply, ApiError.INVALID_EMAIL);
            }

            const keyPayload = {
                username,
                email,
                code,
            };
            let key: string | undefined = undefined;
            if (isTest) {
                const authenticator = await saveAuthenticator(
                    AuthenticatorType.Email,
                    user,
                    keyPayload,
                    15
                );
                key = authenticator.id;
            } else {
                await deduplicateEmail(
                    email,
                    async () => {
                        const authenticator = await saveAuthenticator(
                            AuthenticatorType.Email,
                            user,
                            keyPayload,
                            15
                        );
                        key = authenticator.id;

                        await sendLocalisedEmail(email, locale, "confirmCode", {
                            code,
                        });

                        // TODO: Audit log stuff
                        // await auditLog({user}, 'auth/requested_email_code', {email});
                    },
                    async () => {
                        const auth = await findLatestEmailAuthenticator(
                            email,
                            "email"
                        );
                        key = auth ? auth.id : undefined;
                        // await auditLog({user}, 'auth/requested_email_code_duplicate', {email});
                    }
                );
            }

            if (key == undefined) {
                return replyError(reply, ApiError.INTERNAL_ERROR);
            }

            if (
                !user &&
                (await isInBanArchive(AuthenticatorType.Email, email))
            ) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            return reply.send(
                jwt.signPayload<LoginPayload>(
                    {
                        username,
                        email: isEmail ? email : null,
                        emailObfuscated: obfuscateEmail(email)!,
                        codeKey: key,
                    },
                    15 * 60
                )
            );
        }
    );

    app.post(
        "/:locale/user/validate",
        {
            schema: {
                params: localeSpecific,
                body: Type.Object({
                    code: Type.String(),
                }),
            },
        },
        async (req, reply) => {
            const tokenData = req.v1.token as TokenData<LoginPayload>;
            if (tokenData == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }
            const payload = tokenData.payload;
            if (!payload.codeKey) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            const authenticator = await findAuthenticatorById(
                payload.codeKey,
                AuthenticatorType.Email
            );
            if (authenticator == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            // TODO: Login attempts

            if (authenticator.payload.code !== req.body.code) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            if (authenticator.userId == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }
            const user = await getUserById(authenticator.userId);
            if (user == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            await invalidateAuthenticator(authenticator.id);

            // TODO: Audit log

            const token = await issueUserJwt(user, true);

            return reply.send({
                token: token,
            });
        }
    );
} satisfies AppPluginAsync;
export default plugin;
