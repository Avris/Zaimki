import { isNull } from "@pronounspage/common/util";
import { FastifyReply, FastifyRequest } from "fastify";
import jwt, { ValidJWTPayload } from "#self/util/jwt";
import { createUserJwt, UserJwt } from "#self/util/auth";
import { ExtendedUser, getUserById } from "#self/db/user";
import { Milliseconds } from "@pronounspage/common/util/time";
import { TokenData } from "#self/server/v1";
import { Cookie, cookieSettings } from "#self/util/cookie";

export function processToken<T extends ValidJWTPayload>(
    req: FastifyRequest
): TokenData<T> | null {
    const authorization = req.headers.authorization;
    if (authorization) {
        const [type, remaining] = authorization.split(" ", 1);
        if (type !== "Bearer") {
            return null;
        }

        const payload = jwt.validateToken<T>(remaining);
        if (payload == null) {
            return null;
        }
        return {
            method: "header",
            payload,
        };
    }

    const token = req.cookies[Cookie.Token];
    if (token) {
        const validated = jwt.validateToken<T>(token);
        if (validated == null) {
            return null;
        }
        return {
            method: "cookie",
            payload: validated,
        };
    }

    return null;
}

export const authenticateV1: AppPluginAsync = async (instance) => {
    instance.addHook("preHandler", async (req, reply) => {
        req.log.error("NOTICE ME SENPAI");
        req.v1.token = processToken(req);
        return;
    });
};

export async function reloadUserTokenV1<
    Request extends FastifyRequest,
    Reply extends FastifyReply,
>(
    req: Request,
    reply: Reply
): Promise<{ user: ExtendedUser; token: UserJwt } | null> {
    const token = req.cookies[Cookie.Token];
    if (isNull(token)) {
        return null;
    }

    const requestToken = jwt.validateToken<UserJwt>(token);
    if (requestToken == null) {
        reply.clearCookie(Cookie.Token);
        return null;
    }

    let effectiveToken = requestToken;
    const user = await getUserById(requestToken.id);
    if (user == null) {
        reply.clearCookie(Cookie.Token);
        return null;
    }

    if (
        user.username !== requestToken.username ||
        user.email !== requestToken.email ||
        user.avatarSource !== requestToken.avatarSource ||
        user.bannedReason !== requestToken.bannedReason ||
        user.hasMfa !== requestToken.mfa
    ) {
        effectiveToken = createUserJwt(user, false);
        const encoded = jwt.signPayload(effectiveToken);
        reply.setCookie(Cookie.Token, encoded, cookieSettings());
    }

    return {
        user,
        token: effectiveToken,
    };
}
