import { register } from "node:module";
// import tsNode from "ts-node";
register("ts-node/esm", import.meta.url);

import { startServer } from "#self/server";
import { getConfig, validateConfig } from "#self/config";
import log from "#self/log";
import { exit, pid, ppid } from "node:process";
import {
    getActiveLocaleDescriptions,
    loadAllLocales,
    loadLocaleDescriptions,
} from "#self/locales";
import { loadKeys } from "#self/util/keys";
import { loadStaticData } from "./util/static.js";

log.info(`Pronouns.page Backend Server - PID: ${pid}, PPID: ${ppid}`);

try {
    const validationError = validateConfig(getConfig());
    if (validationError != undefined) {
        log.error(`Configuration is invalid: ${validationError}`);
        exit(1);
    }
} catch (e) {
    log.error(e, "Could not validate configuration");
    exit(1);
}

try {
    const localeDescriptions = await loadLocaleDescriptions();
    const active = getActiveLocaleDescriptions();
    await loadAllLocales();
    log.info(
        `${localeDescriptions.length} locale(s) are registered, of which ${active.length} are active`
    );
} catch (e) {
    log.error(e, "Could not load locales");
    exit(1);
}

try {
    await loadKeys();
    log.info("Loaded necessary keys");
} catch (e) {
    log.error(e, "Could not load keys");
    exit(1);
}

try {
    await loadStaticData();
    log.info("Loaded static data");
} catch (e) {
    log.error(`Could not load static data: ${e}`);
    exit(1);
}

const memUsage = process.memoryUsage();
const memUsagePercent = (memUsage.heapUsed / memUsage.heapTotal) * 100;
log.info(
    `Finished preparations - system stats: MEM ${memUsagePercent.toFixed(2)}%`
);
await startServer();
