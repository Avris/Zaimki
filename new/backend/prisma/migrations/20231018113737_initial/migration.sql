-- CreateTable
CREATE TABLE "audit_log" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT,
    "username" TEXT,
    "event" TEXT NOT NULL,
    "payload" TEXT
);

-- CreateTable
CREATE TABLE "audit_logs_new" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "userId" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "loggedAt" INTEGER NOT NULL,
    "locale" TEXT NOT NULL,
    "actionType" TEXT NOT NULL,
    "actionData" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "authenticators" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT,
    "type" TEXT NOT NULL,
    "payload" TEXT NOT NULL,
    "validUntil" INTEGER,
    CONSTRAINT "authenticators_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "ban_proposals" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT NOT NULL,
    "bannedBy" TEXT,
    "bannedTerms" TEXT NOT NULL,
    "bannedReason" TEXT NOT NULL,
    CONSTRAINT "ban_proposals_bannedBy_fkey" FOREIGN KEY ("bannedBy") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION,
    CONSTRAINT "ban_proposals_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "ban" (
    "type" TEXT NOT NULL,
    "value" TEXT NOT NULL,

    PRIMARY KEY ("type", "value")
);

-- CreateTable
CREATE TABLE "inclusive" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "insteadOf" TEXT NOT NULL,
    "say" TEXT NOT NULL,
    "because" TEXT NOT NULL,
    "locale" TEXT NOT NULL,
    "approved" INTEGER NOT NULL,
    "base_id" TEXT,
    "author_id" TEXT,
    "categories" TEXT,
    "links" TEXT,
    "deleted" INTEGER NOT NULL DEFAULT 0,
    "clarification" TEXT,
    CONSTRAINT "inclusive_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "links" (
    "url" TEXT PRIMARY KEY,
    "expiresAt" INTEGER,
    "favicon" TEXT,
    "relMe" TEXT,
    "nodeinfo" TEXT
);

-- CreateTable
CREATE TABLE "names" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "locale" TEXT NOT NULL,
    "origin" TEXT,
    "meaning" TEXT,
    "usage" TEXT,
    "legally" TEXT,
    "pros" TEXT,
    "cons" TEXT,
    "notablePeople" TEXT,
    "links" TEXT,
    "namedays" TEXT,
    "namedaysComment" TEXT,
    "deleted" INTEGER NOT NULL DEFAULT 0,
    "approved" INTEGER NOT NULL DEFAULT 0,
    "base_id" TEXT,
    "author_id" TEXT
);

-- CreateTable
CREATE TABLE "nouns" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "masc" TEXT NOT NULL,
    "fem" TEXT NOT NULL,
    "neutr" TEXT NOT NULL,
    "mascPl" TEXT NOT NULL,
    "femPl" TEXT NOT NULL,
    "neutrPl" TEXT NOT NULL,
    "approved" INTEGER NOT NULL,
    "base_id" TEXT,
    "locale" TEXT NOT NULL DEFAULT 'pl',
    "author_id" TEXT,
    "deleted" INTEGER NOT NULL DEFAULT 0,
    "sources" TEXT,
    CONSTRAINT "nouns_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "profiles" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT NOT NULL,
    "locale" TEXT NOT NULL,
    "names" TEXT NOT NULL,
    "pronouns" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "birthday" TEXT,
    "links" TEXT NOT NULL,
    "flags" TEXT NOT NULL,
    "words" TEXT NOT NULL,
    "active" INTEGER NOT NULL,
    "teamName" TEXT,
    "footerName" TEXT,
    "footerAreas" TEXT,
    "customFlags" TEXT NOT NULL DEFAULT '{}',
    "card" TEXT,
    "credentials" TEXT,
    "credentialsLevel" INTEGER,
    "credentialsName" INTEGER,
    "cardDark" TEXT,
    "opinions" TEXT NOT NULL DEFAULT '{}',
    "timezone" TEXT,
    "sensitive" TEXT NOT NULL DEFAULT '[]',
    CONSTRAINT "profiles_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "reports" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT,
    "reporterId" TEXT,
    "comment" TEXT NOT NULL,
    "isAutomatic" INTEGER,
    "isHandled" INTEGER,
    "snapshot" TEXT,
    CONSTRAINT "reports_reporterId_fkey" FOREIGN KEY ("reporterId") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION,
    CONSTRAINT "reports_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "social_lookup" (
    "userId" TEXT NOT NULL,
    "provider" TEXT NOT NULL,
    "identifier" TEXT NOT NULL,
    CONSTRAINT "social_lookup_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "sources" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "locale" TEXT NOT NULL,
    "pronouns" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "author" TEXT,
    "title" TEXT NOT NULL,
    "extra" TEXT,
    "year" INTEGER,
    "fragments" TEXT NOT NULL,
    "comment" TEXT,
    "link" TEXT,
    "submitter_id" TEXT,
    "approved" INTEGER DEFAULT 0,
    "deleted" INTEGER DEFAULT 0,
    "base_id" TEXT,
    "key" TEXT,
    "images" TEXT,
    "spoiler" INTEGER NOT NULL DEFAULT 0,
    CONSTRAINT "sources_submitter_id_fkey" FOREIGN KEY ("submitter_id") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "stats" (
    "id" TEXT NOT NULL,
    "locale" TEXT NOT NULL,
    "users" INTEGER NOT NULL,
    "data" TEXT NOT NULL,

    PRIMARY KEY ("id", "locale")
);

-- CreateTable
CREATE TABLE "subscription_messages" (
    "id" TEXT PRIMARY KEY,
    "subscription_id" TEXT NOT NULL,
    "campaign" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "terms" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "term" TEXT NOT NULL,
    "original" TEXT,
    "definition" TEXT NOT NULL,
    "locale" TEXT NOT NULL,
    "approved" INTEGER NOT NULL,
    "base_id" TEXT,
    "author_id" TEXT,
    "deleted" INTEGER NOT NULL DEFAULT 0,
    "flags" TEXT NOT NULL DEFAULT '[]',
    "category" TEXT,
    "images" TEXT NOT NULL DEFAULT '',
    "key" TEXT,
    CONSTRAINT "terms_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "users" ("id") ON DELETE SET NULL ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "user_connections" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "from_profileId" TEXT NOT NULL,
    "to_userId" TEXT NOT NULL,
    "relationship" TEXT NOT NULL,
    CONSTRAINT "user_connections_to_userId_fkey" FOREIGN KEY ("to_userId") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT "user_connections_from_profileId_fkey" FOREIGN KEY ("from_profileId") REFERENCES "profiles" ("id") ON DELETE CASCADE ON UPDATE NO ACTION
);

-- CreateTable
CREATE TABLE "user_messages" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT NOT NULL,
    "adminId" TEXT NOT NULL,
    "message" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "users" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "username" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "roles" TEXT NOT NULL,
    "avatarSource" TEXT,
    "bannedReason" TEXT,
    "suspiciousChecked" tinyint NOT NULL DEFAULT 0,
    "usernameNorm" TEXT,
    "bannedTerms" TEXT,
    "bannedBy" TEXT,
    "lastActive" INTEGER,
    "banSnapshot" TEXT,
    "inactiveWarning" INTEGER,
    "adminNotifications" INTEGER NOT NULL DEFAULT 7,
    "loginAttempts" TEXT,
    "timesheets" TEXT,
    "socialLookup" INTEGER NOT NULL DEFAULT 0
);

-- CreateIndex
CREATE INDEX "authenticators_userId" ON "authenticators"("userId");

-- CreateIndex
CREATE INDEX "authenticators_type" ON "authenticators"("type");

-- CreateIndex
CREATE INDEX "ban_proposals_userId" ON "ban_proposals"("userId");

-- CreateIndex
CREATE INDEX "inclusive_insteadOf" ON "inclusive"("insteadOf");

-- CreateIndex
CREATE INDEX "inclusive_locale" ON "inclusive"("locale");

-- CreateIndex
CREATE INDEX "names_locale" ON "names"("locale");

-- CreateIndex
CREATE INDEX "names_name" ON "names"("name");

-- CreateIndex
CREATE INDEX "names_name_locale" ON "names"("locale", "name");

-- CreateIndex
CREATE INDEX "nouns_masc" ON "nouns"("masc");

-- CreateIndex
CREATE INDEX "nouns_locale" ON "nouns"("locale");

-- CreateIndex
CREATE INDEX "profiles_footerAreas" ON "profiles"("footerAreas");

-- CreateIndex
CREATE INDEX "profiles_footerName" ON "profiles"("footerName");

-- CreateIndex
CREATE INDEX "profiles_teamName" ON "profiles"("teamName");

-- CreateIndex
CREATE INDEX "profiles_locale_userId" ON "profiles"("locale", "userId");

-- CreateIndex
CREATE INDEX "profiles_userId" ON "profiles"("userId");

-- CreateIndex
CREATE INDEX "profiles_locale" ON "profiles"("locale");

-- CreateIndex
Pragma writable_schema=1;
CREATE UNIQUE INDEX "sqlite_autoindex_profiles_2" ON "profiles"("userId", "locale");
Pragma writable_schema=0;

-- CreateIndex
CREATE INDEX "reports_userId" ON "reports"("userId");

-- CreateIndex
CREATE INDEX "reports_isHandled" ON "reports"("isHandled");

-- CreateIndex
CREATE INDEX "reports_isAutomatic" ON "reports"("isAutomatic");

-- CreateIndex
CREATE INDEX "social_lookup_provider_identifier" ON "social_lookup"("provider", "identifier");

-- CreateIndex
CREATE INDEX "sources_locale" ON "sources"("locale");

-- CreateIndex
CREATE INDEX "terms_term" ON "terms"("term");

-- CreateIndex
CREATE INDEX "terms_locale" ON "terms"("locale");

-- CreateIndex
CREATE INDEX "user_connections_to_userId" ON "user_connections"("to_userId");

-- CreateIndex
CREATE INDEX "user_connections_from_profileId" ON "user_connections"("from_profileId");

-- CreateIndex
CREATE UNIQUE INDEX "users_username" ON "users"("username");

-- CreateIndex
CREATE UNIQUE INDEX "users_email" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "users_usernameNorm" ON "users"("usernameNorm");
