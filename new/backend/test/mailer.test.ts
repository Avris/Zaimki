import * as locales from "#self/locales";
import { applyTemplate, DEFAULT_MAXWIDTH, MINIFY_OPTIONS, TemplateContext } from "#self/util/mailer";
import { prepareTestEnvironment } from "./util.ts";
import { Locale } from "#self/locales";
import { LOGO_COLOR } from "@pronounspage/common/constants";
import { minify } from "html-minifier-terser";
import Handlebars from "handlebars";
import { getStaticData, StaticData } from "#self/util/static";

let staticData: StaticData;
let en: Locale, enSubjectPrefix: string;
beforeAll(async () => {
    await prepareTestEnvironment();
    en = locales.getLocale("en")!;
    expect(en).toBeDefined();
    enSubjectPrefix = en.getTranslation("title") + SEPARATOR;
    staticData = getStaticData();
})

const SEPARATOR = " » ";
describe("mail templates", () => {
    it("'confirmCode' (en)", async () => {
        const code = "123456";
        expect(
            await applyTemplate(en, "confirmCode", TemplateContext.Subject, { code })
        ).toBe(enSubjectPrefix + `Your login code is ${code}`);
        expect(
            await applyTemplate(en, "confirmCode", TemplateContext.Text, { code })
        ).toBe(`${en.getTranslation("user.login.email.instruction")}

${code}

${en.getTranslation("user.login.email.extra")}`);
        expect(
            await applyTemplate(en, "confirmCode", TemplateContext.Html, { code })
        ).toBe(await minify(`
            <div style="margin: 36px auto; width: 100%; max-width: ${DEFAULT_MAXWIDTH}; border: 1px solid #aaa;border-radius: 8px;overflow: hidden;font-family: Nunito, Quicksand, Helvetica, sans-serif;font-size: 16px;box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15)">
                <div style="padding: 16px; padding-top: 10px; background: #f8f8f8; border-bottom: 1px solid #aaa;font-size: 20px;color: ${LOGO_COLOR};">
                    <img src="${staticData.logo.primary}" style="height: 24px;width: 24px; position: relative; top: 6px; margin-right: 6px;" alt="Logo"/>
                    ${Handlebars.Utils.escapeExpression(en.getTranslation("title") as string)}
                </div>
                <div style="padding: 8px 16px; background: #fff;">
            <p>${Handlebars.Utils.escapeExpression(en.getTranslation('user.login.email.instruction') as string)}</p>
            <p style="border: 1px solid #aaa;border-radius: 8px;overflow: hidden;text-align: center;user-select: all;font-size: 24px; padding:8px;letter-spacing: 8px; font-weight: bold;">${code}</p>
            <p style="font-size: 12px; color: #777">${Handlebars.Utils.escapeExpression(en.getTranslation('user.login.email.extra') as string)}</p>
                </div>
            </div>
        `, MINIFY_OPTIONS));
    });
});


