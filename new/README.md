# `/new`

This is the working directory for the Pronouns.page rewrite.

## Setup instructions

> Note that this section will assume you are using [pnpm](https://pnpm.io).

1. Install dependencies using `pnpm install`.
2. Run the `setup.mjs` script using `node ./setup.mjs`.
   If you are using a package manager _other_ than pnpm, you should specify that as an argument (`node ./setup.mjs yarn`).
   This is not recommended, however.
3. Build the `common` package (`pnpm build`). This step has to be done every time you change it.
4. The next step depends _entirely_ on what part of the project you're working on:
    1. For the backend: Run `pnpm dev`. This will automatically build and restart the server when you change the code.
       It's actually quite fast.

## Development environment

### `setup.mjs`

Run `setup.mjs` for a quick setup. It'll symlink some directories and will work as the equivalent of `make switch` et al.
The reason for making this a script is that make was causing problems for Windows users.

This script requires a package manager to run, but does not have any non-builtin dependencies.
If you are using a package manager other than `pnpm`, you should specify it as the first argument to the script.

```
node setup.mjs [pnpm/npm/yarn/bun]
```

### Package managers

pnpm should work without any prior setup.
I haven't tested yarn and npm here, and I don't think I will either.
Just use pnpm for as little friction as possible.

### Style and lints

Prettier and ESLint are used for formatting and linting respectively.

You can run `pnpm format` from the top-level directory of the rewrite to format all the code easily.

### Other notes

Currently, the packages here are using `"type": "module"`. This is mostly experimental and can be changed if it turns out to be annoying/code-breaking.

## Process and collaboration

Most decisions should be discussed in the team Discord.
