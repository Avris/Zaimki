export const colours = ['', 'pink', 'red', 'orange', 'brown', 'yellow', 'green', 'teal', 'blue', 'purple', 'grey'];
export const styles = ['', 'bold-italics', 'bold', 'italics', 'small'];
