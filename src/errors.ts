export class LoadScriptError extends Error {
    constructor(scriptName: string, src: string, type: string) {
        super(`failed to load ${scriptName} (${src}): ${type}`);
        this.name = this.constructor.name;
    }
}
