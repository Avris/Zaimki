import { DateTime } from 'luxon';

export type Timesheet = Record<number, Record<string, Record<string, number>>>;
export interface TimesheetData {
    timesheet: Timesheet;
    transfer: keyof typeof TRANSFER_METHODS | '';
    details: {
        bank_name: string;
        bank_iban: string;
        bank_bic: string;
        paypal_email: string;
        charity_name: string;
        charity_url: string;
        notes: string;
    };
}

const now = DateTime.now();
const prevMonth = now.minus({ months: 1 });
export const max = now;
export const min = DateTime.local(2020, 1, 1);
export const closed = DateTime.now().minus({ months: 1 })
    .startOf('month');

export const AREAS = [
    'translation',
    'moderation',
    'content creation',
    'coding',
    'devops',
    'user support',
    'social media',
    'media interviews',
    'design',
    'sensitivity reviews',
    'administration',
    'blog',
    'census',
    'workshops',
    'merch',
    'documentation',
    'community',
    'other',
] as const;

export const MONTHS = {
    1: 'Jan',
    2: 'Feb',
    3: 'Mar',
    4: 'Apr',
    5: 'May',
    6: 'Jun',
    7: 'Jul',
    8: 'Aug',
    9: 'Sep',
    10: 'Oct',
    11: 'Nov',
    12: 'Dec',
};

export const TRANSFER_METHODS = {
    bank: 'Bank transfer',
    paypal: 'PayPal',
    charity: 'Charity',
    skip: 'Skip allowance',
};

export const PERIODS = {
    '2020-2022': [2020, 1, 2022, 12],
    '2023': [2023, 1, 2023, 12],
    '2024': [2024, 1, 2024, 12],
    '2025': [2025, 1, 2025, 12],
    'previous month': [prevMonth.year, prevMonth.month, prevMonth.year, prevMonth.month],
    'current month': [now.year, now.month, now.year, now.month],
};
