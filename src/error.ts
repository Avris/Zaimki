import type { IncomingMessage } from 'connect';
import type { Request } from 'express';

import { newDate } from './helpers.ts';

export default (err: Error, req: Request | IncomingMessage | undefined): string => {
    return `[${
        newDate().toISOString()
    }] [${
        req ? `${req.method} ${req.url}` : ''
    }] ${err.message || err} ${err.stack}`;
};
