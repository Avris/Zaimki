import fs from 'node:fs';

import Papa from 'papaparse';

export const tsvParseConfig = {
    dynamicTyping: true,
    header: true,
    skipEmptyLines: true,
    delimiter: '\t',
};

export const loadTsv = <T = unknown>(filename: string): T[] => {
    return Papa.parse(fs.readFileSync(filename).toString('utf-8'), tsvParseConfig).data as T[];
};
