-- Up

CREATE TABLE audit_log (
    id       TEXT not null primary key,
    userId   TEXT,
    username TEXT,
    event    TEXT not null,
    payload  TEXT
);

-- Down
