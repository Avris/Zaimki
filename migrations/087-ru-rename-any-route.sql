-- Up

UPDATE profiles
SET pronouns = REPLACE(REPLACE(pronouns, 'любые', 'any'),
    -- urlencoded "любые"
    '%\%D0\%BB\%D1\%8E\%D0\%B1\%D1\%8B\%D0\%B5%', 'any')
WHERE locale = 'ru';

-- Down
