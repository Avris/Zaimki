-- Up

create table blog_reactions
(
    id      TEXT not null
        primary key,
    locale  TEXT not null,
    slug    TEXT not null,
    emoji   TEXT not null,
    user_id TEXT not null
        references users
            on delete cascade
);

-- Down
