import { useRuntimeConfig } from 'nuxt/app';

import { useMainStore } from '../store/index.ts';

export default () => {
    const runtimeConfig = useRuntimeConfig();
    const store = useMainStore();

    const buildSocialLoginConnectLink = (
        locale: string,
        provider: string,
        providerOptions: Record<string, any>,
        instance: string | null = null,
    ): string => {
        const url = providerOptions.redirectViaHome
            ? new URL(`/api/user/social-redirect/${provider}/${locale}`, runtimeConfig.public.homeUrl)
            : new URL(`/api/connect/${provider}`, runtimeConfig.public.baseUrl);

        if (providerOptions.instanceRequired && instance) {
            url.searchParams.append('instance', instance ?? '');
        }

        if (providerOptions.redirectViaHome && store.token) {
            // if a logged in user on a domain that's not *.pronouns.page connects a new oauth account,
            // home domain doesn't have the token (cross-domain cookie) and instead of connecting
            // we create a brand new account. let's just pass the token to it
            url.searchParams.append('token', store.token);
        }

        return url.toString();
    };

    return {
        buildSocialLoginConnectLink,
    };
};
