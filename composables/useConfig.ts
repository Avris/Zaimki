import type { Config } from '~/locale/config.ts';

export default (): Config => {
    return useNuxtApp().$localeConfig;
};
