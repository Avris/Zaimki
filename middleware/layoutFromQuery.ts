import { defineNuxtRouteMiddleware, setPageLayout } from 'nuxt/app';

export default defineNuxtRouteMiddleware((to) => {
    if (to.query.layout === 'basic') {
        setPageLayout('basic');
    }
});
