# syntax=docker/dockerfile:1.4
FROM node:20 as base

RUN apt-get update && apt-get install -y openssh-client tini sudo

WORKDIR /app

# Add build arguments for USER_ID and GROUP_ID. These are used to create a user
# with the same UID and GID as the host user, thereby avoiding permission issues
# with mounted volumes.
ARG USER_ID
ARG GROUP_ID

ENV USER_ID=${USER_ID:-1000}
ENV GROUP_ID=${GROUP_ID:-1000}

# Create a user with the same UID and GID as the host user.
RUN set -eux; \
    if ! getent group "$GROUP_ID" >/dev/null; then \
        groupadd -g "$GROUP_ID" usergroup; \
    fi; \
    groupname=$(getent group "$GROUP_ID" | cut -d: -f1); \
    \
    if ! id -u "$USER_ID" >/dev/null 2>&1; then \
        useradd -m -u "$USER_ID" -g "$GROUP_ID" user; \
    fi; \
    username=$(getent passwd "$USER_ID" | cut -d: -f1); \
    \
    echo "$username ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers; \
    \
    chown -R "$USER_ID":"$GROUP_ID" /app; \
    \
    homedir=$(getent passwd "$username" | cut -d: -f6); \
    mkdir -p "$homedir"; \
    chown -R "$USER_ID":"$GROUP_ID" "$homedir"

USER $USER_ID

# Enable corepack for pnpm.
RUN sudo corepack enable
RUN corepack prepare pnpm@9.11.0 --activate

# Set up ssh and grab the gitlab.com host key.
RUN homedir=$(getent passwd "$(id -un)" | cut -d: -f6) && \
    mkdir -p -m 0700 "$homedir/.ssh" && \
    ssh-keyscan gitlab.com >> "$homedir/.ssh/known_hosts"

# Set pnpm store directory to a consistent location.
ENV PNPM_STORE_PATH=/app/.cache/pnpm-store
RUN mkdir -p "$PNPM_STORE_PATH" && pnpm config set store-dir "$PNPM_STORE_PATH"

# Install Playwright browsers (just Chromium for now).
ENV PLAYWRIGHT_BROWSERS_PATH=/app/.cache/playwright
RUN pnpm dlx playwright install --with-deps chromium

# Tell puppeteer (used by pageres) to use the Playwright-installed Chromium.
RUN ln -s "$(find $PLAYWRIGHT_BROWSERS_PATH -type f -name 'chrome' | head -n 1)" /app/.cache/playwright/chromium

ENV PUPPETEER_EXECUTABLE_PATH=/app/.cache/playwright/chromium
ENV PUPPETEER_SKIP_DOWNLOAD=true

# Copy the source code.
COPY --chown=${USER_ID}:${GROUP_ID} . .

# Run install script now that the code is copied over.
RUN --mount=type=ssh,uid=${USER_ID},gid=${GROUP_ID} CI=true APP_ENV=development RUN_SNAPSHOT_TESTS=true make build

# Set environment variables.
ENV APP_ENV=development
ENV RUN_SNAPSHOT_TESTS=true

# Initialize the database with a test user, then set up the entrypoint script.
RUN export TEST_JWT_TOKEN=$(pnpm --silent run-file server/initTestDatabase.ts) \
    && echo "export TEST_JWT_TOKEN=$TEST_JWT_TOKEN" > /app/entrypoint.sh \
    && echo "pnpm test -- --no-watch test/snapshot.test.ts \$@" >> /app/entrypoint.sh \
    && chmod +x /app/entrypoint.sh

ENTRYPOINT ["/usr/bin/tini", "--", "/app/entrypoint.sh"]
