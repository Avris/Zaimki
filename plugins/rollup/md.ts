import fs from 'node:fs';

import marked from 'marked';
import type { TransformResult, Plugin } from 'rollup';

export default (): Plugin => {
    return {
        name: 'transform-md',
        async transform(code, id): Promise<TransformResult> {
            if (/\.md$/.test(id)) {
                const source = await fs.promises.readFile(id, 'utf8');
                return {
                    code: `export default ${JSON.stringify(marked(source))}`,
                    map: { mappings: '' },
                };
            }
            return null;
        },
    };
};
