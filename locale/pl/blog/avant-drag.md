# Avant-Drag! – „Grecja musi umrzeć, abyśmy mogły żyć!”

<small>2024-05-11 | [@T_Vos](/@T_Vos), [@andrea](/@andrea)</small>

![Sekwencja tytułowa filmu: tekst „Avant-Drag! – Radykalne performerki reinterpretują Ateny” na lśniącym tle](/img/pl/blog/avant-drag/avant-drag.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Ostrzeżenia dotyczące treści filmu:</strong>
    migające światła, krew, samookaleczenia, przemoc, homofobia, ksenofobia,
    nienawiść, morderstwo, policja, problemy medyczne, narkotyki.
</div>

Nasza rzeczywistość jest ciężka, zwłaszcza dla osób queer. Na szczęście w społeczności queerowej
zawsze znajdą się osoby gotowe walczyć i zdolne przezwyciężyć strach.

Miałośmy przyjemność rozmawiać z [Thanasis McMorait](https://www.instagram.com/mcmorait), jedną z osób artystycznych przedstawionych w dokumencie Avant-Drag!.
To odważne polityczne queen z Salonik, znane ze performensów z intensywnym punkowo-anarchistycznym przekazem.
Z rozmowy wiele się dowiedziałośmy, będziemy wplatać przemyślenia McMorait w tę recenzję.

Avant-Drag! to grecka produkcja prezentująca dziesięć undergroundowych dragowych osób artystycznych z Grecji –
w pełnej krasie, bez filtrów. W filmie poznajemy ich poprzez różnorodne wyraziste historie i działania, ich polityczne oświadczenia;
są gotowe być punkowe, rebelianckie, i domagać się od społeczeństwa uznania swojej wartości.
Każda z postaci jest wyjątkowa, ale wszystkie zmierzają w jednym kierunku: przyszłość. Przyszłość, która jest queerowa.
Przyszłość, o którą toczy się walka na ulicach, w mediach społecznościowych, w gmachach parlamentów na całym świecie.
Walka toczona przez, jak to ujęło McMorait: „wiele osób queer, które zdołały być dumne z tego, kim są teraz,
a nie kim być może potencjalnie mogła być ich praprababka”.

![](/img/pl/blog/avant-drag/avant-drag-2-er-libido.png)

W filmie widzimy undergroundową scenę greckiego dragu. Tę, która często pozostaje niezauważona na arenie międzynarodowej,
mniej dostępna dla turystów, mniej widoczna na Instagramie, mniej przyjazna algorytmom,
za to bardziej odważna, chaotyczna, zabawna, niedoskonała, ludzka.
Poznajemy społeczność, która na pierwszy rzut oka wydaje się szorstka i ekscentryczna, ale która wspiera się nawzajem.
Jest rywalizacja o występy i szanse, ale jest też siostrzeństwo. McMorait opowiada o swojej społeczności jako o ciepłej, przyjmującej –
to pozwala nowym osobom wykonawczym doskonalić swoje występy i daje poczucie przynależności oraz motywację do bycia lepszymi.

Ale nie wszystko jest różowe.

Avant-Drag! porusza wiele problemów wpływających na społeczność, które są głęboko zakorzenione w tradycjach i starych zwyczajach –
co jest powszechne w wielu, wielu krajach w Europie i na całym świecie. Jak często słyszymy w Polsce o starych archetypach?
O husarzach i królach, o bohaterach wojen światowych i walce o niepodległość?
Trzymamy się starych czasów w poszukiwaniu chwały, nie zdając sobie sprawy, że nowe czasy wymagają nowych archetypów.

W filmie słyszymy, jak grecki patriotyzm i patriarchat podtrzymują mechanizmy opresji,
jak starożytna chwała jest używana do radykalizowania ludzi dzisiaj. Bohaterki filmu biorą ten stary radykalizm i stawiają go przed lustrem.
Nie boją się łączyć tradycyjnych elementów religijnych z symboliką seksualną i wychodzić na ulice, by chwalić aborcję.
Dlaczego? Aby zmusić do myślenia. Aby kwestionować autorytet, który uważany jest za niekwestionowalny.
Mają odwagę wyjść i krzyczeć do nikogo o sprawiedliwości społecznej, odważnie stawiając czoła konserwatyzmowi, patriarchatowi,
rasizmowi i seksizmowi, drwiąc z ksenofobii i czasami po prostu mówiąc głośno to, co zwykle jest niewypowiedziane.
Widzimy, jak ich występy odbywają się na ulicach, jak domagają się przestrzeni i są dumne z tego, kim są. Autentycznie.

Oglądanie Avant-Drag! przypomniało mi rzeczywistość w Polsce sprzed kilku lat, gdy tam mieszkałem.
Pamiętam memoriał zamordowanego geja z mojej społeczności w Szczecinie,
widziałem cierpienie i trudności mojej społeczności, beznadziejność, która temu towarzyszyła.
Pamiętam strach przed ogromnym queerowym światem na zewnątrz. Nie dlatego, że jest straszny czy zły, ale dlatego,
że skoro tak mała odmienność od cishetnormatywnej narracji, jak bycie gejem, wystarczała, by zostać zabitym,
to jak radzić sobie w jeszcze szerszym, często mniej akceptowanym, queerowym świecie pełnym pięknych i unikalnych tożsamości?
A potem poznałem lokalne drag queens, odstające od społeczeństwa, próbujące dokonać zmiany w świecie – 
mój lokalny odpowiednik obsady Avant-Drag!. Niezgrabne w swoich krokach, ale z wewnętrzną motywacją do wolnego życia,
tłumione i zatrzymywane przez zewnętrzne czynniki, również wychodziły na ulice, czasami samotnie. W tej samej walce o przetrwanie.

Tu muszę wspomnieć imię Zaka Kostopoulosa – Zackie Oh – greckiej drag queen zamordowanej na ulicy w Atenach w 2018 roku.
W filmie McMorait korzysta z dużo swojej platformy, aby opowiedzieć historię żalu, bólu i straty.
6 lat później wciąż toczą się procesy sądowe. 4 lata po morderstwie dwóch cywilów otrzymało wyroki – ale nie czterech policjantów.

Avant-Drag! pokazuje, że walka społeczności queerowej jest uniwersalna w cywilizacji europejskiej.
Nie ma znaczenia, czy to kraj prawosławny, katolicki czy protestancki, wszyscy mamy do czynienia z tą samą religią, która nas nie chce.
Nie możemy się zamknąć w małych bańkach – bo jest nas dużo i jesteśmy wyjątkowo piękne w naszej autentyczności.
Nieważne, czy Polska, Grecja czy niderlandzki “pas biblijny”, mamy do czynienia z tą samą obojętnością moralnych autorytetów i władz.
Na naszych ulicach krzyczymy i protestujemy o uznanie naszych praw. Mamy różne tła, ale nasza walka nas jednoczy.

![](/img/pl/blog/avant-drag/avant-drag-5-mcmorait-murder-site.png)

To walka, z którą osoby tworzące ten dokument zmagają się osobiście.
McMorait opowiedziało nam o bardzo ciepłym przyjęciu, jakie film otrzymał od społeczności queerowej,
ale także o protestach, które miały miejsce przed jego pokazem na Festiwalu Filmów Dokumentalnych w Salonikach,
o obawach o reperkusje (szczególnie ze względu na scenę, gdzie Aurora zamalowywuje flagę Grecji na kolory flagi Albanii),
i braku wsparcia ze strony organizatorów festiwalu.
McMorait szuka teraz szczęścia i bezpieczeństwa poza Grecją – i życzymy nu wszystkiego najlepszego w nowym życiu w Holandii!
Ale nigdy nie odniosłem wrażenia, żeby żałowało opowiedzenia swojej historii w filmie, wręcz przeciwnie.
Prawda i rzeczywistość mogą być przerażające, niewielu ma odwagę opowiadać gorzkie historie życia.

Zanim zakończyłośmy rozmowę, zapytałośmy McMorait, co daje nu siłę, by kontynuować walkę pomimo przeciwności?
Otrzymałośmy trzy piękne filary wsparcia, które myślę, że mogą być dobrą podstawą dla nas wszystkich w naszych walkach:

> Co daje mi siłę, zwłaszcza w chwilach ciemności i rozpaczy, to głównie trzy rzeczy.
> Pierwszą z nich jest **siostrzeństwo**. Żyjemy w epoce, kiedy słyszę, jak małe dziewczynki nazywają się nawzajem „bro”.
> I naprawdę nie mogę tego znieść. Dlaczego potrzebujemy tej kultury „bro”, „bro codes”, bardzo męskich mężczyzn,
> nawet w naszych bardziej dziewczęcych, bardziej fagasowych relacjach? Więc posiadanie siostrzeństwa, relacji troski, refleksji nad sobą,
> wolnej przestrzeni do komunikacji i bycia w złym miejscu i złym nastroju oraz tego, że ludzie mogą cię zaakceptować i pokazać swoje wsparcie,
> to jest bardzo ważna rzecz, i to może się zdarzyć tylko, moim zdaniem, w siostrzeństwie.

> Drugą rzeczą są **społeczności**. Jestem jedną z osób, które nie wierzą w tak zwaną społeczność LGBTQIA+.
> Uważam, że jest to bardzo neoliberalne i bardzo białe spojrzenie na queerowe tożsamości.
> Wymazuje wszystkie inne tożsamości i wszystkie inne perspektywy, które osoby queer mogą posiadać. […]
> Myślę więc, że ludzie powinni zrozumieć, że musimy tworzyć nasze własne społeczności i być aktywnymi ich częściami.
> Ponieważ nie każde może zaoferować te same rzeczy w jednej społeczności, i nie każde ma te same rzeczy do zaoferowania przez cały czas.
> W tych społecznościach musimy tworzyć przestrzenie, gdzie tradycyjne formacje władzy nie mają miejsca,
> i gdzie osoby mogą czuć się bezpiecznie, istnieć i komunikować wszystko, co może być nie tak, aby można było to naprawić.

> Trzecią rzeczą jest **intersekcjonalność**, która znowu jest kluczem do zrozumienia, dlaczego żyjemy w okresach ciemności i rozpaczy.
> To jedno znaleźć przestrzeń do istnienia i znaleźć odwagę w tych czasach, a drugie zrozumieć, dlaczego te złe czasy się zdarzają.
> Dla mnie posiadanie intersekcjonalnej perspektywy na wszystko, co się dzieje, pomaga nam zrozumieć, dlaczego rzeczy się dzieją
> i jak wpływają na innych ludzi. Bo ja mogłom czuć rozpacz wiele razy,
> ale mogę zrozumieć, że są ludzie, którzy za każdym razem doświadczają tego samego, co mnie doprowadziło do rozpaczy,
> musiało być dla nich jeszcze trudniejsze. Morderstwo Zackie Oh było bardzo ważnym momentem w Grecji, który pokazał, że musimy
> być bardzo intersekcjonalne w tym względzie. Bo najpierw powiedzieli, że „to był imigrant”, nie wiedzieli, że to Grek został zamordowany.
> Potem próbowali powiedzieć, że Zackie była pod wpływem narkotyków w momencie, gdy została zamordowana, więc mogła umrzeć na atak serca
> lub coś takiego z powodu narkotyków. To znowu okazało się kłamstwem, według badań medycznych.
> Więc kiedy starałyśmy się komunikować, co się stało, musiałyśmy przekazać, że najpierw była to kwestia rasizmu,
> potem homofobii i femmefobii oraz seksizmu, potem nienawiści do osób uzależnionych od narkotyków, tego, w jaki są postrzegane jako mniej ludzkie.
> To była sprawa, w której intersekcjonalność była kluczowa do zrozumienia tego, co się stało, i wierzę, że jest kluczowa w prawie wszystkich przypadkach.

> Więc siostrzeństwo, społeczności, intersekcjonalność. Tak.

Wszystkie mamy siebie nawzajem. Avant-Drag! przypomina nam o tym w najbardziej awangardowy sposób, poprzez dziesięć historii i wiele artystycznych wyrażeń.

Chcę zakończyć ten artykuł okrzykiem stworzonym podczas protestów po morderstwie Zackie, okrzykiem, który naprawdę zapadł nam w pamięć:
„Grecja musi umrzeć, abyśmy mogły żyć”.

Stare archetypy, które nie pasują do naszego nowoczesnego zrozumienia, muszą zostać zastąpione. Pomagały nam w przeszłości – ale teraz nas krzywdzą.
To przypomina mi kolejny cytat, z Willema De Kooninga:
„Muszę się zmienić, aby pozostać tym samym”. Musimy dostosować się do nowych czasów, aby pozostać sobą.

Z całego serca polecamy Avant-Drag! Pokazuje prawdziwe emocje splecione z lękiem o przyszłość i docenieniem swojej społeczności.
Dokument możesz zobaczyć podczas [festiwalu DAG w Warszawie](https://mdag.pl/21/pl/warszawa/movie/Avant-Drag%2521) oraz podczas Xposed Queer Film Festival Berlin.

<a href="https://mdag.pl/21/pl/warszawa/movie/Avant-Drag%2521" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-5">
    <span class="fal fa-ticket-alt"></span>
    Kup bilety tutaj
</a>

<a href="https://www.instagram.com/avant.drag.film/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-instagram-square"></span>
    Obserwuj na Instagramie
</a>

<a href="https://www.facebook.com/avantdrag" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-facebook-square"></span>
    Obserwuj na Facebooku
</a>

{gallery={
    "/img/pl/blog/avant-drag/avant-drag-1-kangela.png":               "Kangela",
    "/img/pl/blog/avant-drag/avant-drag-1-kangela-art.png":           "Kangela",
    "/img/pl/blog/avant-drag/avant-drag-2-er-libido.png":             "Er Libido",
    "/img/pl/blog/avant-drag/avant-drag-2-er-libido-mary.png":        "Er libido",
    "/img/pl/blog/avant-drag/avant-drag-3-aurora.png":                "Aurora",
    "/img/pl/blog/avant-drag/avant-drag-3-aurora-flaga.png":          "Aurora",
    "/img/pl/blog/avant-drag/avant-drag-4-parakatyanova.png":         "Parakatyanova",
    "/img/pl/blog/avant-drag/avant-drag-4-parakatyanova-sergay.png":  "Parakatyanova & SerGay", 
    "/img/pl/blog/avant-drag/avant-drag-4-sergay.png":                "SerGay",
    "/img/pl/blog/avant-drag/avant-drag-5-mcmorait.png":              "McMorait",
    "/img/pl/blog/avant-drag/avant-drag-5-mcmorait-murder-site.png":  "McMorait",
    "/img/pl/blog/avant-drag/avant-drag-6-cotsos.png":                "Cotsos",
    "/img/pl/blog/avant-drag/avant-drag-6-cotsos-konstatntinos.png":  "Cotsos",
    "/img/pl/blog/avant-drag/avant-drag-7-lala-kolopi.png":           "Lala Kolopi",
    "/img/pl/blog/avant-drag/avant-drag-8-veronique.png":             "Veronique",
    "/img/pl/blog/avant-drag/avant-drag-interview.png":               "",
}}

