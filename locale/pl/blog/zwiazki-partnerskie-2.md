# Znamy kluczowe założenia ustawy o związkach partnerskich!

<small>2024-09-25 | [@Tess](/@Tess)</small>

![Zdjęcie grupowe osób uczestniczących w spotkaniu](/img/pl/blog/kprm-2-0.jpg)

**Kolejne spotkanie w Kancelarii Prezesa Rady Ministrów, skromniejsze, lecz wciąż całkiem liczne grono. Ministra ds. Równości ponownie zaprosiła queerowe osoby aktywistyczne do rozmowy o ustawie o związkach partnerskich. I chyba w końcu mamy konkrety.**

Od dłuższego czasu frustracja w naszej społeczności narastała, nie da się tego ukryć. Wicepremier Władysław Kosiniak-Kamysz grzmiał o tym, jak PSL nie zgodzi się na adopcję dzieci, na przysposobienie wewnętrzne, na ceremonię w USC… i właściwie na wszystko. Ba, nawet określenie „związki partnerskie” okazało się dla niego zbyt postępowe, wymyślił zamiast tego kuriozalny „status osoby najbliższej”. Do tego premier Donald Tusk udawał, że temat nie istnieje. Niedługo minie rok od wyborów, a osoby LGBTQ+ nie odczuły żadnej różnicy spowodowanej zmianą rządu. Brak konkretów nie pomagał.

Historyczne spotkanie z Ministrą ds. Równości Katarzyną Kotulą z reprezentacją osób aktywistycznych odbyło się 18 marca. Dyskusja na Sali Kolumnowej im. Anny Walentynowicz w Kancelarii Prezesa Rady Ministrów z kilkudziesięcioma osobami queerowymi była nową jakością w polskiej polityce, ale po upływie pół roku można było pomyśleć, że tylko wizerunkową – w końcu na zewnątrz nie było widać nic więcej. W tym czasie organizacje spotykały się z Katarzyną Kotulą i osobami reprezentującymi jej departament w czasie roboczych spotkań online, a dziś – czyli 24 września 2024 roku – ministra dotrzymała słowa i to właśnie tej grupie jako pierwszej zaprezentowała finalne założenia projektu ustawy o rejestrowanych związkach partnerskich.

Spotkanie prowadziły cztery osoby: Katarzyna Kotula, Anna Mazurczak, dyrektorka Departamentu ds. Równego Traktowania, Magdalena Dropek, młodsza kierowniczka z Departamentu ds. Równego Traktowania oraz prawniczka Milena Adamczewska-Stachura. Oprócz nich nad projektem ustawy pracowali Paweł Knut, Jakub Pawliczak i Mateusz Wąsik.

Jaki jest sam projekt? Uważam, że niezły.

Tak, to skandal, że w XXI wieku nie mamy równości małżeńskiej oraz że w projekcie nie znalazły się ani adopcja, ani nawet przysposobienie wewnętrzne (czyli adopcja biologicznego dziecka jednej z osób wchodzących w związek partnerski przez drugą z nich). To nie tak, że projekt przygotowany przez departament Katarzyny Kotuli jest idealny i powinno się go polecać jako wzór dla innych krajów. Ale uważam, że jak na obecną sytuację społeczną i polityczną jest naprawdę odważny. A to już moim zdaniem coś – choć oczywiście możecie się ze mną nie zgodzić.

Zacznę od kwestii istotnej dla osób niebinarnych i transpłciowych: ustawa pozwala na zawarcie rejestrowanego związku partnerskiego bez względu na płeć. Nie musimy więc obawiać się ani kwestii tego, że będziemy zmuszone udawać kobiety czy mężczyzn, ani tego, że ustawa będzie dotyczyć tylko osób będących zgodnie z prawem tej samej płci, co stwarzałoby problemy w przypadku tranzycji już po zawarciu związku. W tej kwestii pojawia się pełna inkluzywność.

Niestety związek partnerski będą mogły zawrzeć wyłącznie dwie osoby, nie można też zawrzeć więcej niż jednego związku, co oznacza, że osoby w związkach poliamorycznych pozostaną niewidzialne z punktu widzenia prawa.

Ustawa wprowadza pojęcie innowacyjne – małej pieczy. Oznacza ona, że wprawdzie osoba partnerska niebędąca formalnie rodzicem nie będzie mogła nabyć praw rodzicielskich nawet wtedy, kiedy drugi z rodziców nie żyje lub został pozbawiony tych praw, ale będzie uprawniona do sprawowania pieczy w zakresie podejmowania decyzji dotyczących życia codziennego dziecka (chyba że nie wyrazi na to zgody którykolwiek z rodziców). Decyzyjność będzie dotyczyć przede wszystkim kwestii edukacyjnych i zdrowotnych, choć nie w pełnym zakresie – osoba taka będzie mogła zabrać dziecko do lekarza, ale już nie wyrazić zgodę na operację. Jeśli rodzic dziecka zmieni nazwisko, wchodząc w związek partnerski, ono też będzie mogło przyjąć nowe nazwisko. W kwestiach spadku i darowizny będzie włączone do tzw. grupy zerowej, czyli nie będzie obciążone podatkiem. Rodzic będzie mógł też wskazać, że to właśnie ta osoba ma zająć się dzieckiem na wypadek jego śmierci; zostanie ona zwolniona z obowiązku odbycia szkolenia w ośrodku adopcyjnym.

Jasne, że dla dzieci w tęczowych rodzinach adopcja lub przysposobienie wewnętrzne byłyby najlepszymi rozwiązaniami, ale cieszę się, że w projekcie ustawy znalazło się wiele zapisów zabezpieczających je. Rozwiązanie, choć nie jest idealne, pozwala realnie uniknąć wielu traum i krzywd, na które dziś dzieci z tęczowych rodzin są skazane. Mała piecza wydaje się też świetnym rozwiązaniem w sytuacji, kiedy adopcja nie byłaby możliwa – czyli drugi rodzic wciąż żyje i nie został pozbawiony praw rodzicielskich. Dziecko nadal będzie miało dwoje rodziców, ale jednocześnie nowa osoba partnerska jednego z rodziców będzie mogła realnie uczestniczyć w opiece nad nim. Osoba zawierająca związek partnerski nie będzie mogła rozliczać się jako osoba samotnie wychowująca dziecko. Nie będzie za to refundacji in vitro dla par jednopłciowych.

Połowicznym sukcesem wydaje się kwestia Urzędu Stanu Cywilnego – otóż projekt ustawy zakłada, że owszem, oświadczenie o zawarciu związku partnerskiego będziemy składać w USC (lub w konsulacie), ale nie ma zapisów o towarzyszącej temu ceremonii. Jeśli zapis ten przejdzie bez zmian, w praktyce będzie on zapewne oznaczać turystykę związkową, co będzie sytuacją podobną do tej, którą dziś osoby niebinarne i transpłciowe obserwują  przypadku zmiany imion. W konserwatywnych USC zapewne skończy się na podpisaniu dokumentów,  progresywnych – na uroczystej ceremonii na sali ślubów, z tą różnicą, że osoba kierownicza USC nie będzie mieć na ramionach łańcucha. Związek partnerski będzie można zawrzeć też poza USC w dowolnym miejscu, po uiszczeniu opłaty urzędowej analogicznej dla tej dla małżeństw. Możliwe będzie złożenie oświadczenia w języku migowym.

Jednocześnie związki partnerskie wprowadzają kolejne innowacyjne rozwiązania, których brak w przypadku małżeństw. Możliwe będzie na przykład... złożenie oświadczeń osobno, w odstępie maksymalnie siedmiu dni. Choć brzmi to jak sztuka dla sztuki, są sytuacje, w których kwestia ta może okazać się kluczowa – na przykład kiedy jedna z osób nie może opuścić kraju, w którym przybywa. Wówczas może ona złożyć oświadczenie w konsulacie, a druga – w USC. Związek będzie ważny i na jego podstawie będzie można objąć osobę przebywającą poza Polską oraz jej dzieci ochroną międzynarodową. To jeden z powodów, który może okazać się istotny dla par jednopłciowych – możliwość zawarcia związku partnerskiego wtedy, kiedy małżeństwo będzie fizycznie niemożliwe. Pozostawanie w związku partnerskim z osobą mającą polskie obywatelstwo będzie przesłanką do udzielenia zgody na pobyt stały

I choć część praw związków partnerskich będzie zbliżonych do praw małżeństw, osoby zawierające je unikną komplikacji związanych z rozwodem. Związek partnerski rozwiązuje się obustronnym porozumieniem lub poprzez jednostronną deklarację (wówczas musi upłynąć 30 dni, po czym USC dostarczy decyzję o rozwiązaniu związku do drugiej osoby; przez komornika, jeśli będzie to konieczne). Jeśli tylko jedna z osób w związku partnerskim będzie posiadać mieszkanie, to w przypadku zakończenia go, druga ma prawo do mieszkania w nim jeszcze przez trzy miesiące. Jeśli jedna z osób znajdzie się w sytuacji niedostatku, może ubiegać się o alimenty, które będą wypłacane przez rok.

Osoby będące w związku partnerskim będą mogły zawrzeć później ślub bez konieczności rozwiązywania związku, ale nie ma możliwości przekształcenia małżeństwa w związek partnerski – ze względu na szczególną ochronę, która obejmuje małżeństwa w Polsce. Związki partnerskie zawarte w innych krajach będą ważne w Polsce po transkrybowaniu aktu zawarcia związku; małżeństwa jednopłciowe zawarte za granicą będą mogły złożyć wniosek o objęcie ich takimi samymi prawami jak osoby będące w związku partnerskim. (W przypadku rozstania rozwiązanie związku partnerskiego nie zastąpi jednak rozwodu).

Wspólnota majątkowa będzie dla osób pozostających w związku partnerskim możliwa, choć w odróżnieniu do małżeństw nie będzie domyślna.

W kwestiach pomocy społecznej czy kodeksu pracy osoby w związku partnerskim będą spełniać definicję rodziny, więc będą uprawnione do świadczeń opiekuńczych, płatnej opieki nad chorą osobą partnerską lub jej dzieckiem, renty rodzinnej, zasiłku pogrzebowego czy wszystkich kwestii dotyczących rent i emerytur, które są dostępne dla małżonków. Osoby w związkach partnerskich będą mogły otrzymać informację o swoim stanie zdrowia, będą też powołane do spadku w pierwszej kolejności, będą też mieć prawo do zachowku, a od spadku nie zapłacą podatku. W przypadku śmierci osoby partnerskiej zadecydują o jej pochówku oraz będzie im przysługiwać prawo do dyspozycji wypłaty środków z jej rachunku.

Bardzo możliwe, że coś pomijam – w czasie spotkania przedstawiony został nam główny zarys ustawy, nie cały jej zapis, a spotkanie trwało ponad dwie godziny, więc trudno je streścić w jednym artykule. Wydaje mi się, że poza kwestiami adopcji dzieci ustawa w dużej mierze spełnia potrzeby naszej społeczności.

Uważam, że warto podkreślić, że spotkanie i dyskusja przebiegły w fantastycznej atmosferze – pisałom już, że wcześniej zdarzały się sytuacje, gdy jedne z osób wyraźnie nie szanowały czasu innych. Tym razem do niczego takiego nie doszło, widać było, że wszystkie osoby uczestniczące dbają o dyscyplinę czasową, a zadawane pytania były celne i wiele wnosiły. Praca w takim gronie to była prawdziwa przyjemność.

Czy projekt ustawy trafi do Sejmu w takim kształcie? Tego oczywiście nie wiemy. Wkrótce rozpoczną się konsultacje społeczne. Właściwie mogłyby się rozpocząć lada dzień, ale klęska powodzi i konieczność pomocy osobom poszkodowanym w naturalny sposób spowodowała odłożenie tej kwestii na później. Wciąż jednak możliwe, że konsultacje społeczne rozpoczną się w ostatnich dniach września lub na początku października. Potrwają trzydzieści dni.

Niestety, tęczowa społeczność zapewne spotka się w tym czasie z ogromnym hejtem, zachęcam więc z jednej strony do czynnego uczestniczenia w konsultacjach (poprzez zamieszczanie komentarzy do projektu ustawy), a z drugiej – przede wszystkim do odcięcia się od social mediów czy kanałów informacyjnych. Dbajmy o siebie i kontaktujmy się z życzliwymi osobami, zamiast narażać się na nienawiść czy przemoc. Walczmy wtedy, kiedy mamy na to siły i zasoby.

Po dzisiejszym spotkaniu z Ministrą ds. Równości Katarzyną Kotulą mogę jednak powiedzieć, że wierzę, że zależy jej na tym projekcie, a osoby pracujące nad nim wykonały niezwykle ciężką pracę.

Stworzenie pojęcia związków partnerskich okazało się wymagać nowelizacji ponad 250 różnych ustaw. To absurd, że cała ta praca jest konieczna, skoro równość małżeńska rozwiązywałaby te komplikacje niemalże jednym podpisem. Skoro jednak ktoś ogrom tej pracy wykonał, widać, że naprawdę wierzy w tej projekt. I choćby w tej kadencji wszystko potoczyło się według najczarniejszych scenariuszy (z takim składem rządu jednak trudno mieć nadzieję na proces legislacyjny usłany różami), to pracy, której wykonano, nikt nie zdoła cofnąć.

— Anna Tess Gołębiowska

{gallery={
    "/img/pl/blog/kprm-2-1.jpg": "",
    "/img/pl/blog/kprm-2-2.jpg": "",
    "/img/pl/blog/kprm-2-3.jpg": "",
    "/img/pl/blog/kprm-2-4.jpg": "",
    "/img/pl/blog/kprm-2-5.jpg": "",
    "/img/pl/blog/kprm-2-6.jpg": "",
}}
