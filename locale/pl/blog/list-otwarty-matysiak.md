# List otwarty do posłanki Pauliny Matysiak 

<small>2025-02-05 | [Zoë Ziemskie](https://bsky.app/profile/zoezed.bsky.social)</small>

W związku z wypowiedzią Pani Posłanki [Pauliny Matysiak](https://bsky.app/profile/polamatysiak.bsky.social)
na łamach podcastu [“Dwie Lewe Ręce”](https://www.youtube.com/@dwielewerece)
[Zoë Ziemskie](https://bsky.app/profile/zoezed.bsky.social)
skierowało do niej [list otwarty](https://bsky.app/profile/zoezed.bsky.social/post/3lhgrxoywzs2k),
którego treść – za zgodą – przedrukujemy poniżej i się pod nią podpisujemy.

---

Szanowna Pani Posłanko,

Jestem osobą niebinarną. Używam zaimków ono/jej.

Jestem jednocześnie przedstawicielką klasy pracującej. Nie jestem uprzywilejowane ekonomicznie i, jako osoba wiele lat mieszkająca na wsi, mam doświadczenie m.in. wykluczenia komunikacyjnego i wykluczenia cyfrowego.

Z głębokim zaniepokojeniem odnoszę się do Pani niedawnej wypowiedzi na łamach podcastu Dwie Lewe Ręce. Przekonywała Pani, że lewica powinna ograniczyć przekaz dotyczący spraw społecznych, argumentując to następująco:

> „Większość ludzi (…) więcej czasu poświęca na to, ile będzie wynosił ich rachunek za prąd w przyszłym miesiącu niż [na] kwestie zaimków - z całym szacunkiem do różnych zaimków - i kopiowanie mody zza oceanu”.

Jest to komunikat wykluczający, obłudny oraz nastawiony na budowanie kapitału politycznego kosztem dyskryminowanej grupy społecznej. Jednocześnie należy wyjaśnić, że wspomniała Pani jedynie o “zaimkach”, ale Pani wypowiedź odczytana w pełnym kontekście ewidentnie odnosi się do całej lewicowej narracji dotyczącej osób LGBTQ+. Jest to szczególnie niepokojące, w kontekście działań mających na celu usunięcie osób niebinarnych i transpłciowych z życia publicznego w Stanach Zjednoczonych.

W swojej wypowiedzi w sposób zawoalowany sugeruje Pani, że wspieranie osób transpłciowych i niebinarnych jest niekompatybilne z wspieraniem osób wykluczonych ekonomicznie. Pani wypowiedź buduje wrażenie, jakby te dwie kwestie rywalizowały o niepodzielne zasoby. Wyciszanie narracji o problemach jednej grupy, żeby rzekomo wzmocnić donośność innej narracji, nie da oczekiwanego przez Panią efektu, a będzie wyłącznie pogłębiało konflikty społeczne. Jawne i premedytowane wygaszanie narracji dotyczącej osób queerowych będzie sugerowało osobom wyborczym, że lewica faktycznie nie może pracować nad kwestiami takimi jak wykluczenie komunikacyjne, w czasie gdy zajmuje się polityką tożsamościową. Pani wypowiedź zwyczajnie sugeruje potencjalnym osobom wyborczym, że “nie będzie autobusów, mieszkań i godnych płac, dopóki lewica mówi o osobach niebinarnych”.

Osoby niebinarne nie są mityczną wielkomiejską klasą średnią oderwaną od reszty społeczeństwa. Niebinarność nie jest „modą zza oceanu”. Moje potrzeby w zakresie ochrony praw społecznych są dla mnie równie ważne co moje potrzeby w zakresie ochrony praw ekonomicznych. Chcę podkreślić, że jestem osobą po trzydziestce i już od dziecka odczuwałom odmienność mojej tożsamości płciowej od tzw. binarnej „normy”. Używane przeze mnie zaimki stanowią ekspresję mojej tożsamości. Są dla mnie źródłem komfortu psychicznego i są zwyczajnie częścią mnie. Ich ignorowanie czy deprecjonowanie stanowi dla mnie oczywisty dyskomfort psychiczny. Żeby mogła to Pani zrozumieć, proszę sobie wyobrazić, jak by się Pani czuła, gdyby publicznie i nagminnie zwracano się do Pani cudzym imieniem (nazwiskiem) albo męskimi zaimkami. Jednocześnie proszę sobie wyobrazić, że zwrócenie przez Panią uwagi na własny dyskomfort zostałoby zbagatelizowane jako “podążanie za modą”.

Chcę jednocześnie podkreślić, że piszę ten list nie tylko ze względu na to, że Pani wypowiedź dotyczy mnie osobiście. Fundamentalnie nie zgadzam się z Pani diagnozą dotyczącą tego, dlaczego lewica nie potrafi dotrzeć do potencjalnych osób wyborczych. Pani teza stanowi nadmierne uproszczenie i przeinaczenie zniuansowanego, wielopłaszczyznowego zagadnienia.

Prawdą jest, że parlamentarna lewica nie ma przekazu, który może skutecznie dotrzeć do polskiej klasy pracującej. Sugerowanie jednak, że przyczyną tego stanu rzeczy jest zbytnie koncentrowanie się na narracji dotyczącej polityki tożsamościowej jest nieuczciwe intelektualnie  i pokazuje, że nie ma Pani rzeczywistego obrazu sytuacji i poglądów osób wykluczonych ekonomicznie i społecznie.

W swojej historii zawodowej pracowałom z osobami, do których rzekomo stara się Pani dotrzeć: z osobami zarabiającymi minimalną krajową, wykluczonymi komunikacyjnie, społecznie i ekonomicznie. Te osoby miały często niewielką lub żadną wiedzę dotyczącą programu partii lewicowych. Nie głosują na lewicę nie dlatego, że odrzuca je temat polityki tożsamościowej, lecz dlatego, że nierzadko nie mają świadomości, że istnieje uczciwa alternatywa wobec największych graczy na zabetonowanej scenie politycznej.

Jednocześnie, te same osoby nie miały żadnego problemu z zaakceptowaniem mojej tożsamości płciowej czy używanych przeze mnie zaimków. Ewidentnie Pani Posłanka ma z zaimkami większy problem niż osoby rzeczywiście należące do klasy pracującej.

Z mojej perspektywy jedną z kluczowych przyczyn – chociaż oczywiście nie jedyną – kiepskiej rozpoznawalności lewicy nie jest treść przekazu, a metoda komunikacji. I tę kwestię Pani Posłanka całkowicie ignoruje, zamiast tego szukając kozła ofiarnego wśród osób queerowych.

Parlamentarna lewica nagminnie usiłuje dotrzeć do wyżej wymienionych osób za pomocą narzędzi, które zwyczajnie do nich nie dosięgają. Trafienie do klasy pracującej wymaga autentyczności, oddolnej pracy, działalności grassroots i zaangażowania twarzą w twarz z osobami wyborczymi w celu zbudowania zaufania i poczucia wspólnoty. Nie da się tej pracy u podstaw zastąpić klipami, podcastami i reklamami w social mediach, czy nawet komunikatami w prasie, radiu i telewizji. Na taki sposób docierania do społeczeństwa mogą sobie pozwolić wyłącznie duże partie z ugruntowaną rozpoznawalnością i silną pozycją polityczną.

Polska lewica potrzebuje zrozumiałej i konkretnej narracji zarówno w zakresie polityki ekonomicznej, jak i tożsamościowej. Przekaz polityczny normalizujący społeczność LGBTQ+ i uświadamiający, z jakimi trudnościami muszą mierzyć się osoby queerowe to przekaz, który wręcz ratuje ludzkie życia. Jest to przekaz u gruntu lewicowy, gdyż dotyczy osób wykluczonych, a to te osoby - bez wyjątków – z definicji stanowią podmiot polityki lewicowej. Wbrew Pani sugestii, każdej mniejszości należy się pełne wsparcie i ochrona, niezależnie od tego, jak często (rzadko) myślimy o innych kwestiach. Argumentowanie, tak jak to Pani robi, że problemów grupy społecznej można nie nagłaśniać, tylko z uwagi na jej wielkość, stanowi czysty koniunkturalizm. Idąc tym tropem możemy również przemilczeć kwestie prawa do aborcji, gdyż większość ludzi częściej myśli o rachunkach za prąd niż o aborcji.

Komunikaty ekonomiczny i tożsamościowy nie wykluczają się wzajemnie, a sugerowanie, że jest inaczej, to tworzenie fałszywej dychotomii i zakłamywanie istoty polityki lewicowej. Wsparcie społeczności LGBTQ+, tak samo jak wspieranie każdej innej dyskryminowanej grupy społecznej, to obowiązek sine qua non lewicy. Kluczowym elementem ochrony praw osób LGBTQ+ jest normalizacja ich istnienia w przestrzeni publicznej i politycznej, głośne mówienie o ich problemach oraz o metodach wsparcia. Już samo tworzenie widoczności osób queerowych jest stanowiskiem politycznym. Tak samo jak decyzja o porzuceniu narracji o osobach queerowych ma wymiar polityczny i, poprzez bezczynność, przesuwa okno Overtona w prawo. W konsekwencji wzmacnia to również “prawicę ekonomiczną”, bo pozwala jej na budowanie siły politycznej poprzez bezkarne atakowanie wyimaginowanych wrogów wśród mniejszości. Arbitralne decydowanie o tym, jaka grupa wykluczona zasługuje, a jaka nie zasługuje na wspomnienie i na pomoc to polityka na wskroś prawicowa, i to taką politykę Pani Posłanka teraz uprawia.

Natomiast, jeżeli Pani Posłanka próbuje przekonywać, że nie posiada nawet tej malutkiej ilości zasobów umożliwiających komunikowanie kilku punktów programowych jednocześnie, to powinna się Pani zastanowić, czy tworzenie autentycznego ruchu lewicowego nie jest zwyczajnie ponad Pani siły, umiejętności i kompetencje.

Wracając jeszcze do treści Pani wypowiedzi, liczę na to, że zacznie Pani podchodzić z całym szacunkiem do ludzi, niezależnie od tego jakich zaimków używają, a nie tylko, jak to Pani ujęła, “do różnych zaimków”. Bo to ludziom należy się szacunek i najwyraźniej o tym Pani Posłanka zapomniała, skupiając się na budowaniu egzotycznych mariaży z prawą stroną sceny politycznej.

Z poważaniem,  
Zoë Ziemskie
