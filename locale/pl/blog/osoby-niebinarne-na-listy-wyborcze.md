# Osoby niebinarne na listy wyborcze! San Kocoń, Irys Barda, Miłosz Rabus i Jan Gierzyński kandydują do Sejmu

<small>2023-09-13 | [@Tess](/@Tess)</small>

![Zdjęcia osób kandydujących, od lewej: Jan Gierzyński, Irys Barda, San Kocoń, Miłosz Rabus](/img/pl/blog/osoby-niebinarne-na-listy-wyborcze.png)

<p class="small">Od lewej: Jan Gierzyński, Irys Barda, San Kocoń, Miłosz Rabus</p>

**To historyczna chwila – po raz pierwszy w kampanii wyborczej w Polsce nie tylko zaistniała tematyka praw osób niebinarnych, ale też same osoby niebinarne kandydują do parlamentu. Jakie są ich postulaty?**

Polityczka używająca feminatywów musi liczyć się z tym, że będzie wytykana za słowa takie jak „naukowczyni”, „gościni” czy nawet „posłanka”. Absolutnie naturalne dla polszczyzny formy, znane i oswojone od lat, dla wielu osób wciąż są sprawą polityczną, „[orwellowską nowomową](https://www.facebook.com/neutratywy/posts/pfbid02vZv9CHWQSox8zU8RNr9EfBDwmbH4n6F9eTMnLjZAdAsERhnG9VMNGJkBrDRYi9MSl)” czy „gwałtem na języku”. A przecież kobiety stanową połowę społeczeństwa! Osoby niebinarne stanowią mniejszość (szacujemy, że w Polsce może być ich [niecałe 200 tysięcy](https://zaimki.pl/blog/ile-jest-osob-niebinarnych)), a język neutralny płciowo bywa krytykowany nawet przez osoby o wydawałoby się postępowych poglądach. Coming out jako niebinie w sferze publicznej stanowi więc wyzwanie – istnieje ogromne ryzyko, że zamiast móc swobodnie mówić o swoich poglądach czy postulatach, dana osoba będzie musiała wciąż i wciąż wyjaśniać, że tak, osoby niebinarne istnieją, że nie, to nie była literówka, naprawdę używam [rodzaju neutralnego](https://zaimki.pl/rodzaj-neutralny).

Temat niebinarnych tożsamości [powoli pojawia się w popkulturze](https://zaimki.pl/blog/popkultura-niebinarno%C5%9B%C4%87) – zarówno w kontekście postaci, jak i osób aktorskich czy piosenkarskich – ale nie istniał do tej pory w polskiej polityce, jeśli nie liczyć [pojedynczych konserwatywnych głosów](https://zaimki.pl/blog/konserwatywny-prezent) próbujących ośmieszyć wszystko, co nie wpisuje się w binarny, patriarchalny porządek świata.

Okazuje się jednak, że wybory parlamentarne z 2023 roku przejdą do historii polskiego queerowego społeczeństwa – po raz pierwszy na listach wyborczych pojawiły się wyoutowane osoby niebinarne. Irys Barda, Miłosz Rabus oraz Jan Gierzyński reprezentują partię Razem na listach Nowej Lewicy, a San Kocoń – partię Zieloni na listach Koalicji Obywatelskiej.

Na osobę niebinarną będzie można zatem oddać głos w czterech okręgach: Irys Barda kandyduje w okręgu nr 40 z siedzibą w Koszalinie (miejsce na liście #3), Miłosz Rabus – w okręgu nr 31 z siedzibą w Katowicach (#10), Jan Gierzyński – w okręgu nr 10 z siedzibą w Piotrkowie Trybunalskim (#8), a San Kocoń – w okręgu nr 13 z siedzibą w Krakowie (#19).

W 2011 roku Anna Grodzka została pierwszą wyoutowaną transpłciową posłanką w Polsce. Mimo że tygodnik „Polityka” umieścił ją w czołówce najlepszych osób poselskich VII kadencji Sejmu, dwukrotnie bezskutecznie ubiegała się o reelekcję, po czym zrezygnowała z polityki.

Aż do tej pory żadna wyoutowana osoba transpłciowa nie powtórzyła jej osiągnięcia. Czy tegoroczne wybory przyniosą zmianę? Nawet jeśli San Kocoń, Irys Barda, Miłosz Rabus i Jan Gierzyński nie zdobędą mandatów, to sama ich obecność na listach jest zmianą – i może mieć wpływ na debatę społeczną.

I chociaż jako Rada Języka Neutralnego zajmujemy się przede wszystkim językoznawczymi aspektami aktywizmu, w pierwszej kolejności pytamy osoby kandydujące o ich poglądy i postulaty. Dlaczego? Bo właśnie tym się zajmują. Język oraz sytuacja osób niebinarnych jest bliska naszym sercom, więc oczywiście poruszyłośmy te tematy, ale zależy nam, by choć symbolicznie postawić akcent na to, o co walczą młode osoby niebinarne w Polsce.

Zapraszamy do zapoznania się z odpowiedziami na cztery pytania, jakie zadałośmy osobom kandydującym.

<h2 class="mb-4">Jeśli uda Wam się dostać do Sejmu, jakie kwestie i postulaty będą dla Was najważniejsze?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Osobiście najbardziej zaangażowane jestem w kwestie polityki oświatowej, kulturalnej i równościowej, więc takimi kwestiami bym się zajmowało w Sejmie. „Odchudzenie” szkolnych podręczników. Mniej liczebne klasy, lepsze wynagrodzenia dla osób nauczycielskich, a także wprowadzenie do szkół tematów związanych z edukacją klimatyczną, seksualną i obywatelską. Mam nadzieję, że w kolejnej kadencji Sejmu w końcu uda się przyjąć ustawę o zawodzie artysty, na którą od wielu lat czeka środowisko artystyczne. Według mnie należałoby zlikwidować dysproporcję w wynagrodzeniu za tę samą pracę w instytucjach podobnego typu, ale prowadzonych przez różnych organizatorów, a także wprowadzić zachęty finansowe dla firm, by bardziej angażowały się w mecenat. Oczywiście ważne są dla mnie postulaty społeczności LGBTQIA+, której jestem częścią. Pełna równość małżeńska, zakaz terapii konwersyjnych, zakaz interwencji chirurgicznych na noworodkach interpłciowych, uproszczenie procesu uzgodnienia płci, refundacja korekty płci oraz umożliwienie oznaczenia w dokumentach płci innej niż męska i żeńska dla osób niebinarnych i interpłciowych.
Niezwykle istotne są dla mnie także prawa zwierząt i prawa kobiet, ale uważam, że mamy w partii Zieloni osoby bardziej rozeznane w tych tematach niż ja.
Jako osoba reprezentującą zieloną politykę zawsze będę za działaniami ukierunkowanymi na walkę ze zmianą klimatu oraz tworzeniem bardziej zielonej Polski. Inwestowanie w OZE, odblokowanie możliwości energetyki obywatelskiej, rozwój komunikacji zbiorowej, budowa inteligentnych sieci przesyłowych, dopłaty do termomodernizacji budynków... Można by tak długo wymieniać. Uważam, że zmiana klimatu i jej konsekwencje stanowią największe wyzwanie dla ludzkości w XXI wieku.

### IRYS BARDA, on/onu (Razem, Nowa Lewica)

Od początku mojego wejścia na rynek pracy byłem żywo zainteresowany kwestiami prawa pracowniczego w naszym kraju. „Zradykalizowałem” się w czasie 1,5 roku pracy w magazynie, gdzie ludzie tracili zdrowie, żeby wyrabiać normy. Tamtejszy związek zawodowy, jak zresztą prawie każdy inny w Polsce, nie był w stanie wiele zaradzić, ponieważ prawo dotyczące związków zawodowych oraz strajków pracowniczych jest niewyobrażalnie ograniczone. Sam doświadczyłem również bezpłatnego stażu studenckiego, w czasie którego utrzymywałem się z oszczędności, miałem całe szczęście też przywilej pomocy finansowej od rodziców przez czas studiów. Stabilne wejście w dorosłość nie powinno zależeć od zamożności rodziny, powinno zapewniać je państwo. Prawo pracy w Polsce wymaga zaktualizowania, ośmiogodzinny dzień pracy został krwawo wywalczony 100 lat temu, koncept „work-life balance” nie powinien znajdować się na liście benefitów ofert pracy, tylko powinien być podstawą każdej umowy, w postaci m.in. zmniejszonego czasu pracy. Pracownicy w instytucjach państwowych będą w stanie zapewnić efektywne usługi społeczne tylko wtedy, kiedy ich wynagrodzenia będą realnie wzrastać. Jako osoba w trakcie hormonalnej terapii zastępczej, leczenia depresji oraz ze zdiagnozowany ADHD, jestem tylko jednym z wielu przypadków osób skazanych na prywatną ochronę zdrowia w celu swobodnego funkcjonowania. Publiczna ochrona zdrowia powinna sprawnie zapewniać wysoki standard świadczeń na NFZ. Autonomia cielesna oraz prawa człowieka nie powinny być poddawane debacie. Oczywiście nie zamierzam zapomnieć o ochronie klimatu, która leży w interesie nas i przyszłych pokoleń, a jej zaniedbywanie tylko pogłębi problemy takie jak rozwarstwienie społeczne.

### MIŁOSZ RABUS, on/jego (Razem, Nowa Lewica)

Trudno jest wybrać takie postulaty, które będą dla mnie najważniejsze. Jestem członkiem Razem, startuję z Lewicy, programy Razem i komitetu wyborczego Nowej Lewicy są bardzo podobne, a ja zgadzam się z nimi w większości kwestii. Jako osoba queerowa, na pewno będę walczył o równość w Sejmie, ale nawet jeśli się nie dostanę, to nie przestanę walczyć. Uważam też, że nie osiągniemy równości kulturowej i społecznej, dopóki nie zaadresujemy nierówności klasowych, które niestety nękają Polskę. Dodatkowo na bieżąco staram się opisywać ważne dla mnie postulaty.

### JAN GIERZYŃSKI, on/jego (Razem, Nowa Lewica)

Szczególnie ważne są dla mnie, jako psychologa, kwestie dotyczące zdrowia psychicznego i jego profilaktyki, m.in. prawne uregulowanie kwestii zawodu psychologa – uchwały, która niestety cały czas jest w legislacyjnej zamrażarce, a która jest nam potrzebna na wczoraj; profilaktyka zdrowia psychicznego i psychoedukacja już na etapie przedszkolnym oraz zwiększenie dostępności do opieki psychologicznej i psychiatrycznej dzieci i młodzieży – np. „psycholog w każdej szkole”, zachęty dla specjalizacji w psychiatrii dziecięcej i młodzieżowej;  profilaktyka zdrowia psychicznego seniorów i aktywizacja społeczna osób starszych, np.  postulat „klub seniora w każdej gminie” oraz ogólnie dbanie o potrzeby osób starszych i z niepełnosprawnościami, aby mogły żyć jak najpełniejszym i godnym życiem.
Kolejnym obszarem, na którym chciałbym się skupić, jest walka z wykluczeniem komunikacyjnym – z naciskiem na rozwój transportu publicznego, głównie kolejowego, jako konkurencyjnej i lepszej alternatywy dla indywidualnego transportu samochodowego, oraz przeciwdziałanie katastrofie klimatycznej, czyli wszelkie działania mogące pomóc w adaptacji i minimalizowaniu negatywnych skutków dla społeczeństwa.
Kwestie dotyczące praw osób LGBTQ również są dla mnie bardzo ważne – równość małżeńska, ochrona przed dyskryminacją – zarówno w instytucjach publicznych, jak i relacjach prywatnych,  edukacja antydyskryminacyjna, absolutny zakaz tzw. terapii konwersyjnych – to podstawy. Oczywiście będę dążyć do uporządkowania istniejącego w Polsce stanu prawnego dotyczącego procesu uzgadniania płci oraz refundowania opieki medycznej związanej z tranzycją (hormonalnej terapii zastępczej).

<h2 class="mb-4">Dlaczego zdecydowaliście się na kandydowanie?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Rok temu szukaliśmy w Ostrej Zieleni, czyli młodzieżowym stowarzyszeniu współpracującym z partią Zieloni, młodych osób, które mogłyby kandydować w wyborach. Dopiero wprowadzałom się do Krakowa. Na spotkaniu koła lokalnego partii dowiedziałom się, że nie ma innych chętnych do startu w wyborach, więc pomyślałom: „może ja?”. Kiedyś musi być ten pierwszy raz. Nam, Polskim Młodym Zielonym, zależy nam na odmładzaniu polityki i wprowadzaniu młodych osób o zielonych poglądach do przestrzeni publicznej. Młodzieżówki nie powinny być tylko od rozdawania ulotek i klikania serduszek w necie ulubionym osobom publicznym.

### IRYS BARDA, on/onu (Razem, Nowa Lewica)

Jako osoba członkowska zarządu okręgu zachodniopomorskiego Partii Razem, kołobrzeżanin z urodzenia oraz zaangażowana społecznie osoba czułem, że na liście nr 40 jest potrzebna kandydatura queerowej, młodej osoby która walczyłaby o lewicowe wartości i cieszę, że okazja się pojawiła. Kandydowanie w moim wypadku wiąże się z reprezentowaniem osób nieheteronormatywnych, co jest odpowiedzialnym zadaniem. Chcę przede wszystkim, żeby władza przestała ignorować i podważać nasze istnienie oraz potrzeby.

### MIŁOSZ RABUS, on/jego (Razem, Nowa Lewica)

Ten kraj potrzebuje zmiany, młodych i ambitnych ludzi, czyli właśnie mnie. Wpasowuję się też w kilka grup społecznych, które są niestety niedoreprezentowane i widzę, jak trudno w tym kraju jest być kimś innym niż bogatym, heteronormatywnym, bogobojnym mężczyzną po czterdziestce. Kandyduję też dla swojej rodziny, chciałbym pokazać, że nawet wywodząc się z niczego, można wyjść na ludzi i nieść realną zmianę. Tak uczyła mnie moja mama i tak chcę żyć.

### JAN GIERZYŃSKI, on/jego (Razem, Nowa Lewica)

Z tego samego powodu, dla którego zaangażowałem się w politykę, czyli frustracji spowodowanej obecnym stanem rzeczy. Uważam, że wszyscy ludzie mają prawo do życia w sposób, który czyni ich szczęśliwymi; każda osoba zasługuje na godne traktowanie. Zgodnie z teorią piramidy potrzeb Maslowa, aby móc się realizować i osiągnąć swój potencjał, muszą być najpierw zapewnione podstawowe potrzeby: fizjologiczne, bezpieczeństwa i przynależności. Niestety, dla wielu osób nie są one możliwe do spełnienia – nie w kraju, gdzie trudno o własne mieszkanie, gdzie miesiącami czeka się na wizytę u lekarza, gdzie politycy otwarcie szczują na swoich obywateli. I to wszystko dzieje się  w świecie, który dosłownie się pali, gdzie przyszłość jest bardzo niepewna, a politycy podchodzą do tych kwestii w sposób (w moim odczuciu) mocno opieszały i lekceważący, skupiając się na bieżączce i ignorując długofalowe konsekwencje swoich działań, oczekując natychmiastowej gratyfikacji. Nie chcę już więcej polityków, którzy myślą wyłącznie o czubku własnego nosa i tylko kombinują, jak być najbliżej koryta.

<h2 class="mb-4">Czy sądzicie, że niebinarność i transpłciowość pojawią się w tej kampanii wyborczej, a jeśli tak, to w jakim kontekście?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Analizując ten tydzień... Już się pojawiły. Jednak patrząc szerzej... Kaczyński podczas swojego wcześniejszego objazdu po Polsce próbował straszyć osobami transpłciowymi i z nich drwić, ale chyba ten temat niezbyt się przyjął. Woli grać kartami „osoby uchodźcze” i „niemiecki Tusk”, bo się sprawdzają. Z drugiej strony postulaty społeczności LGBTQIA+ są coraz lepiej przyjmowane w środowiskach lewicowych i centrowych, więc pojawiają się w programach politycznych, ale nie spodziewam się, żeby nagle stały się głównym tematem dyskursu publicznego. Zieloni od 2004 roku stoją murem za osobami LGBTQIA+, są to dla nas sprawy bardzo ważne, ja od swojej partii dostałom dużo wsparcia w ciągu ostatnich dni. Jednak obecnie najgłośniej mówimy i będziemy mówić o sprawiedliwej transformacji energetycznej.

### IRYS BARDA, on/onu (Razem, Nowa Lewica)

Kampania ma do siebie to że wyciągane są w złej wierze różne aspekty życia danej osoby. Nie ukrywam się ze swoją querrowością, natomiast nie czuję potrzeby odnoszenia się do niej w kampanijnych okolicznościach, kiedy do poruszenia jest wiele innych kwestii. W tych wyborach mamy kilka wyoutowanych osób queerowych, których jakakolwiek aktywność podsyci wokół tych tematów dyskusję, która w dużej części w ogóle nie powinna mieć miejsca. Mam nadzieję, że niezależnie od kontekstu, w jakim pojawią się niebinarność i transpłciowość w tej kampanii, pchnie to dyskurs społeczny w stronę rzeczywistej zmiany prawnej.

### MIŁOSZ RABUS, on/jego (Razem, Nowa Lewica)

Nie tyle sądzę, ja to wiem. Wszyscy chyba już widzieli, z jakim kubłem pomyj spotkało się San, aktywiszcze z Zielonych. Z San mogę nie zgadzać się w niektórych kwestiach, ale wciąż jesteśmy tak samo ludźmi i zasługujemy na szacunek i wsparcie. Stoję solidarnie z San jako dumna osoba queerowa. Jeśli chodzi o przyszłe sytuacje, no cóż, mam nadzieję, że będą dużo bardziej pozytywne. Mam szczerą nadzieję, że przez resztę tej krótkiej kampanii osoby niebinarne i trans będą traktowane z szacunkiem i da się im głos.

### JAN GIERZYŃSKI, on/jego (Razem, Nowa Lewica)

Na ten moment nie podejrzewam, aby kwestia osób transpłciowych i niebinarnych była jedną z ważniejszych w tej kampanii. Po krótkim epizodzie wyszydzania osób trans przez prezesa Kaczyńskiego temat jak na razie ucichł. Gdyby temat miał się ponownie pojawić, to podejrzewam, że niestety byłoby to w kontekście straszenia osobami trans i niebinarnymi jako elementu rzekomej „ideologii” gender i „marksizmu kulturowego”, które zaburzają konserwatywne, binarne postrzeganie płci (oraz świata). Niestety, duże braki w rzetelnej edukacji seksualnej pozwalają na taką manipulację narracyjną, a to, co dla ludzi jest obce i nieznane, naturalnie wzbudza lęk, a na emocjach grać jest najłatwiej.

<h2 class="mb-4">Dlaczego potrzebujemy osób niebinarnych oraz transpłciowych w Sejmie?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Potrzebujemy osób niebinarnych w Sejmie, bo potrzebujemy reprezentacji różnych grup społecznych w polityce. Ludzi różnych doświadczeń. Istotna kwestia — przecież ja nie startuję do Sejmu, bo jestem osobą niebinarną i teraz mam wielki plan szokować wszystkie osoby o konserwatywnych poglądach swoim istnieniem. Zajmuję się polityką młodzieżową i tak przypadkiem się zdarzyło, że jestem osobą niebinarną. Przyznam jednak, że uruchomienie tej całej dyskusji o niebinarności i języku neutralnym w polityce, to bardzo niespodziewany, ale przyjemny dodatek do mojej kandydatury.

###  IRYS BARDA, on/onu (Razem, Nowa Lewica)
Z takiego samego powodu dlaczego w sejmie potrzebujemy żeby zasiadały kobiety osoby z niepełnosprawnościami i z innych marginalizowanych grup społecznych.
Nic o nas bez nas.

### MIŁOSZ RABUS, on/jego (Razem, Nowa Lewica)

Bo to część społeczeństwa. Społeczeństwo musi być w Sejmie i Senacie reprezentowane, a bez osób niebinarnych i transpłciowych nie będzie pełnej reprezentacji. Tu chodzi po prostu o godność i o bezpieczeństwo. Jeśli w polityce znajdzie się więcej osób queerowych, to młode dzieciaki LGBTQIA+ nie będą aż tak bardzo obawiać się o siebie, a to niewątpliwie wpłynie pozytywnie na to,  jak postrzega nas reszta społeczeństwa.

### JAN GIERZYŃSKI, on/jego (Razem, Nowa Lewica)

Tak samo jak potrzebujemy w parlamencie osób należących do innych mniejszości – etnicznych, rasowych, seksualnych – w celu reprezentacji tych grup, a tym samym w normalizacji występowania osób transpłciowych i niebinarnych w społeczeństwie.

## ***

Poza pulą wspólnych pytań skierowanych do wszystkich osób kandydujących, zadałośmy też dodatkowe, nawiązujące do form używanych przez dane osoby lub związane z ich sytuacją.

<h2 class="mb-4">Pierwszy raz w kampanii pojawia się osoba używająca języka neutralnego – czy nie bałoś się tego, jak zareaguje dość konserwatywne polskie społeczeństwo?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Tak szczerze... Absolutnie o tym nie myślałom. Od dwóch lat używam w przestrzeni publicznej zaimków ono/jego, neutratywów i osobatywów. Czasem ludzie są zdziwieni, ale przyzwyczajają się z czasem do używania prawidłowych form. Przed tą głośną już konferencją prasową Koalicji Młodych nie zastanawiałom się, jakiej formy mam użyć, ja po prostu tak o sobie mówię cały czas. Chyba zapomniałom, że poza moją bańką światopoglądową ludzie potrafią być nienawistni tylko dlatego, że ktoś wygląda czy mówi inaczej niż oni.

<h2 class="mb-4">Dlaczego język neutralny jest dla Ciebie ważny?</h2>

### SAN KOCOŃ, ono/jego (Zieloni, Koalicja Obywatelska)

Przyznam się, że czasem mieszam różne formy. Feminatywy i maskulatywy mi nie przeszkadzają, jeśli są używane zamiennie. Jednak mówienie o sobie w formach neutralnych daje mi gender euforię. Podkreślam tym fakt, że nie jestem ani kobietą, ani mężczyzną, a także pokazuję, jak język polski jest niezwykły w swojej różnorodności.

<h2 class="mb-4">Po raz pierwszy w kampanii pojawiły się osoby niebinarne oraz używające zaimków innych niż ona/on – czy nie bałeś się tego, jak zareaguje dość konserwatywne polskie społeczeństwo?</h2>

### IRYS BARDA, on/onu (Razem, Nowa Lewica)

„Bałeś się” to nieadekwatne słowo, spodziewałem się nieprzychylnego odzewu. Każdy nowy czy niepopularny koncept będzie spotykać się z oporem części społeczeństwa niezależnie od okoliczności. Oby było to etapem w jego akceptacji i adaptacji.

<h2 class="mb-4">Czy język neutralny jest dla Ciebie ważny?</h2>

### IRYS BARDA, on/onu (Razem, Nowa Lewica)

Język jest narzędziem które dostosowuje się do reali ludzi którzy się nim posługują. Formy neutralne „byłom/zrobiłom” już są poprawne gramatycznie wg Rady Języka Polskiego. Język się nieustannie kształtuje i rozwija, formy neutralne są tego naturalną konsekwencją. Osobiście jestem dużym jego zwolennikiem, osobatywy stosuję na porządku dziennym a pytanie o preferowane formy gramatyczne powinno być powszechne przy przedstawianiu się.
Szczerze mówiąc, z różnych względów określam się dla uproszczenia jako trans mężczyzna, zdecydowałem się na to z niestety o tyle dołującego powodu, że spłaszczenie przekazu osobistego pozwala mi na bezpieczniejsze poruszanie się w społeczeństwie cisheteronormatywnym, np. w czasie poszukiwania pracy. Nie mówię oczywiście, że jestem w stanie przez to uniknąć wszystkich nieprzyjemności, queerfobia nadal istnieje. W bezpiecznych przestrzeniach określam się jako osoba niebinarna, transmęska z zaimkami on/onu, chciałbym przyłożyć się do tego żeby nasz kraj stał się taką bezpieczną przestrzenią dla wszystkich.

Widzę w postach, że używasz m.in. osobatywów oraz podkreślasz, że w dokumentach powinno znaleźć się oznaczenie X – dlaczego te kwestie są istotne?

### MIŁOSZ RABUS, on/jego (Razem, Nowa Lewica)

Te kwestie są dla mnie istotne, ponieważ sam nie uważam, że wpasowuję się w binarny podział płci. Jest on dla mnie przestarzały. Wiem też, że inkluzywny język daje ludziom poczucie bezpieczeństwa i przynależności. Inkluzywny język może przywrócić komuś wiarę w siebie, czy dać nadzieję. Chciałbym być kandydatem, który jest ostoją tego wszystkiego. Wychowałem się też z językiem angielskim, tam formy bezpłciowe i osobatywy to standard. Co do markerów X, myślę, że to przedłużenie inkluzywnego języka. To symbol. To nadzieja.

<h2 class="mb-4">Zdecydowałeś się kandydować pod deadname’em – nie boisz się, że wprowadzi to chaos i że stracisz przez to głosy? Czy nie byłoby dla Ciebie bezpieczniej poczekać do kolejnych wyborów?</h2>

### JAN GIERZYŃSKI, on/jego (Razem, Nowa Lewica)
Rzeczywiście, na stronie PKW figuruję pod deadname’em i tak będę również widniał na karcie do głosowania. Kampanię prowadzę jednak pod swoim prawdziwym imieniem. W przekazie w mediach społecznościowych zaznaczam, że jestem obecnie w trakcie sądowego procesu uzgadniania płci i w związku z tym w dokumentach wciąż mam nieużywane przeze mnie dane. Chcę jakoś normalizować tę, w gruncie rzeczy, nienormalną sytuację, która sumie dobrze pokazuje trudności, z jakimi mierzą się w codziennym życiu osoby transpłciowe. Nie chcę, aby moja tożsamość mnie ograniczała. Jestem obywatelem tego kraju, posiadającym pełnię praw wyborczych i zamierzam z tego korzystać. Nie zamierzam i nie chcę w swojej kampanii podkreślać swojej tożsamości płciowej. Wynika to z mojego podejścia do własnej transpłciowości, którą traktuję jedynie jako jedną ze swoich cech i która nie determinuje tego, kim jestem jako osoba. Może wynika to ze stresu mniejszościowego, ale zdecydowanie nie chcę być „tokenowym” transem.

## ***

Z większą widocznością osób niebinarnych i transpłciowych w przestrzeni publicznej niestety nieodłącznie wiążą się skierowane przeciwko nim enbyfobiczne i transfobiczne ataki – czego osoby kandydujące już teraz doświadczyły na własnej skórze. Jednocześnie nikt równie skutecznie co osoby niebinarne i transpłciowe nie będzie walczyć o nasze prawa i potrzeby, więc potrzebujemy reprezentacji – tu i teraz. Walczymy o nią w konstrukcjach językowych i w popkulturze; San Kocoń, Irys Barda, Miłosz Rabus i Jan Gierzyński walczą o nią w Sejmie. A już dziś są osobami, które wywalczyły ją na listach wyborczych i w queerowej historii Polski.
