#  Oświadczenie kolektywu „Rada Języka Neutralnego” i Wojciecha Dragana

<small>2024-02-13 | Kolektyw RJN & Wojciech Dragan</small>

![](/img/pl/blog/dragan-statement.png)

W tym miejscu znajdował się pierwotnie inny tekst, będący częścią większego sporu.
Wojciech Dragan zaproponował nam spotkanie, abyśmy spróbowały dojść do porozumienia.
Ponieważ nam również zależy na deeskalacji konfliktu, zgodziłośmy się. Spotkanie zakończyło się w zgodzie,
dlatego edytujemy ten wpis oraz publikujemy wspólnie napisane oświadczenie:

Uznając wspólnie, że wolimy budować mosty, zamiast wznosić mury kolektyw „Rada Języka Neutralnego” oraz Wojciech Dragan oświadczają, że:

 - Obydwu stronom leży na sercu dobrostan psychiczny osób małoletnich biorących udział w Niebinarnym Spisie Powszechnym;
 - Obydwie strony zgadzają się co do tego, że w pewnych sytuacjach nie jest pożądane, ani konieczne zbieranie zgód na udział w badaniu od rodziców małoletnich osób queerowych i – po spokojnej dyskusji uznają – że do takich przypadków można zaliczyć sposób zbierania danych w ramach „Niebinarnego Spisu Powszechnego”;
 - Obydwie strony uznają, że Niebinarny Spis Powszechny, który organizowany jest przez kolektyw „Rada Języka Neutralnego”, ma duże znaczenie dla społeczności osób queerowych w Polsce. Z tego względu ważne jest dążenie do stałego udoskonalania jego jakości;
 - W związku z tym ostatnim,  Wojciech Dragan złożył Kolektywowi propozycję współpracy merytorycznej przy kolejnym Spisie oraz reprezentowania prowadzonego przez nich badania w procesie uzyskiwania opinii od odpowiedniej komisji ds. etyki badań naukowych.

Ponadto Wojciech Dragan przeprasza Kolektyw za krzywdzący ich pracę sposób, w jaki sformułował krytykę przygotowanego przez nich badania.

Obie strony zobowiązują się, by ewentualna dalsza krytyka miała charakter merytoryczny i konstruktywny, a nie formujący podziały.
