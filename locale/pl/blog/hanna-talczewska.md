# Kobiece dane mimo „M” w dowodzie – wywiad z Hanną Talczewską o odważnych wyborach tranzycyjnych

<small>2024-04-29 | [@Tess](/@Tess)</small>

![Zdjęcie profilowe Hanny](/img/pl/blog/hanna-talczewska.png)

Część osób transpłciowych oraz niebinarnych decyduje się na zmianę imienia dwukrotnie – najpierw na imię neutralne płciowo, nadawane zarówno osobom z „M”, jak i z „K” w dowodzie, a dopiero potem, po zakończeniu drogi prawnej, na imię właściwe. Hanna Talczewska uznała, że metody obecnie przyjęte w naszej społeczności są utrudnianiem sobie życia i spróbowała nowego podejścia.

**[ANNA TESS GOŁĘBIOWSKA-KMIECIAK](/@Tess): Udało Ci się zmienić imię i nazwisko na zgodne ze swoją płcią bez wcześniejszego pozwania rodziców. To nietypowa, wręcz wyjątkowa sytuacja. Skąd taki właśnie pomysł?**

**HANNA TALCZEWSKA:** Dziwne pytanie, dla mnie to brzmi jak najbardziej oczywista rzecz – pozew będzie się ciągnął, pochłonie niefajne koszty za samo złożenie, jak sąd będzie chciał, to powoła biegłych za tysiące, stos się będzie sypał. A wniosek do USC kosztuje 37 zł i na rozpatrzenie jest miesiąc, dwa – jeśli to nazwą szczególnie skomplikowaną sprawą. To jest pomysł w rodzaju „generalnie rzecz biorąc lepiej być młodym, pięknym, zdrowym i bogatym niż nie”. No oczywiście, że tak lepiej.

Tak szczerze to mi się na usta ciśnie, że to pokurwiony pomysł, że trzeba pozywać swoich starych. To się powinno załatwić przez korektę w urzędzie i tyle.

Tylko trzymano nas w lęku nawet przed próbowaniem podstępną pułapką: wszyscy ci mówią, że jak raz złożysz wniosek i ci go uwalą, to już więcej nie możesz próbować, koniec świata, mogiła. I ludzie tak bardzo nie chcieli stracić prawa złożenia wniosku, że go nigdy nie składali, żeby nie stracić możliwości, z której nie skorzystają, żeby jej nie stracić. I może różnica była w tym, że ja przejrzałam, że to scam i miałam gotowość iść walczyć, żeby ugrać swoje?

Właśnie czekam w kolejce do fotela bólowego na stomatologii – nie było dobrych miejsc siedzących z widokiem na tablicę z numerkami, więc rozejrzałam się po poczekalni, jak można ją lepiej urządzić, poprosiłam o pomoc stojącego obok faceta i razem przenieśliśmy ławki, które stały pod ścianą z wyświetlaczem, w miejsce, z którego było go wygodnie widać. Jak coś jest źle, to nie akceptuję tego stanu, tylko kombinuję jak to naprawić – a jak jest źle i dotyka mnie, to czuję się upoważniona żeby wprowadzać zmiany.

**A czy słyszałaś kiedykolwiek o osobie, która zrobiła tak jak Ty?**

Do niedawna całkowicie nie, a teraz tak naprawdę dalej nie, ale dla węższego znaczenia „zrobić jak ja”. Było przynajmniej kilku trans facetów (lub osób trans męskich? nie wiem, nie zaglądałam im tak dokładnie w płeć), którzy zmienili końcówkę nazwiska ze „-ska” na „-ski”. Składając swój wniosek, nie wiedziałam o nich, ale tak szczerze to mogłam się spodziewać – sama powoływałam się w argumentacji na moją ciotkę Aśkę Talczewski, budując wątek tradycji rodzinnej nazwisk niezgodnych z płcią przypisaną – ale bardzo miła i pomocna urzędniczka, u której konsultowałam pierwszą wersję mojego wniosku, ostrzegała, że końcówka „-ski” jest uważana w jakiś sposób za bardziej neutralną, żebym ze swoją zmianą nie spodziewała się sukcesu, bo tak się nie robi – więc nie sądzę żeby była inna transfemka, która wcześniej próbowała.

Mam nadzieję, że teraz ruszą. Chcę namawiać do spróbowania osoby, które i tak nie planowały zmiany imienia i nazwiska przed rozprawą – ale do spróbowania w innych miejscach niż Wrocław. Wiemy, że tam są ogarnięci ludzie, którzy nie będą naginać prawa przeciwko nam i to jest super, ale czemu uciekać z całej Polski do Wrocławia? Innych urzędników też można nauczyć jak powinno być – choćby wnioskiem, który im nie zostawi miejsca na wątpliwości, jaka jest dla nich jedyna zgodna z prawem opcja. Ten, który składałam, już był bardzo solidny, uzasadnienie decyzji dostarczyło pięknego materiału do wyszlifowania kolejnych – tylko już nie w mojej sprawie. 

<aside class="card card-primary w-lg-50 float-lg-end m-3">
    <div class="card-header">
        <p class="h4 mb-0">
            Diagnostyka i tranzycja<br>
            <small> – poradnik Hanny*</small>
        </p>
    </div>
    <div class="card-body">
        <ol>
            <li>Złóż pozew o ustalenie płci.</li>
            <li>Złóż wniosek o zwolnienie z kosztów.</li>
            <li>Złóż wniosek o powołanie biegłego, wskazując kompetentną osobę.</li>
            <li>Złóż wniosek o zwolnienie z kosztów.</li>
        </ol>
        <div class="alert alert-warning mb-0">
            <p class="mb-0">UWAGA! Metoda nietestowana, jeśli się zdecydujesz, będziesz pierwszą osobą w Polsce, która uzyska diagnozę w ten sposób.</p>
        </div>
    </div>
    <div class="card-footer small">
        <p class="mb-0">* na podstawie rozmów z Mileną Adamczewską-Stachurą z inicjatywy Prawo Nie Wyklucza, specjalizującej w prawie antydyskryminacyjnym i ochronie praw człowieka</p>
    </div>
</aside>

**Czy zanim złożyłaś wniosek, konsultowałaś się z kimś w tej sprawie? Czy ktoś Ci kibicował, doradzał, a może odradzał?**

W przygotowaniach najpierw radziłam się Mileny Adamczewskiej-Stachury w ramach wsparcia z [Prawo Nie Wyklucza](https://mnw.org.pl/prawo-aplikuj/) (piękna inicjatywa, kłamliwa nazwa). Jak do naszej rozmowy zoomowej dołączyłam jeszcze streaming swojego komputera z zebranymi w sprawie dowodami, to obejrzała je i powiedziała –  i to była super wartościowa pomoc! – że tak, spokojnie z tym mogę się starać o Hannę, a może nawet Talczewską, poradziła składać wniosek we Wrocławiu i resztę konsultacji rozmawiałyśmy o rozprawie sądowej i innym potencjalnym myku – bo wyczuła że ma kogoś kto walczy solidnie i się nie boi, a miała w zanadrzu niesamowity pomysł, którego nikt nie próbował: Chce ktoś załatwić diagnostykę transpłciowości całkiem na koszt państwa i dużo prędzej niż kolejkami NFZ? Da się, przy okazji oznaczenie płci też będzie zmienione!

Potem zapytałam na grupach transowych czy ktoś próbował zmieniać imię na nie neutralne, ale brzmiące jednoznacznie niezgodnie z płcią przypisaną – głównie padały informacje że się nie da, ktoś poradził, że „Hanna” może być uniseks imieniem w okolicy Palestyny czy w języku hebrajskim. Poszłam do łódzkiej gminy żydowskiej, u nich jest to imię tylko kobiece, ale trop na palestyńskich Hannów był dobry, leciałam dalej ośmielona wiedzą, że tę część mam praktycznie w kieszeni.

Kilka miesięcy później z już gotowym wnioskiem napisałam do USC Wrocław z prośbą o konsultację wniosku, wysyłając linka do google docsa – trochę czasu później oddzwoniła do mnie bardzo miła i pomocna Aleksandra Kosieradzka. Powiedziała, że faktycznie udowodniłam mocno, że używam tych danych i że Hanna jest imieniem neutralnym, więc ta część wygląda dobrze, ale żeby na nazwisko ze „-ska” się nie nastawiać.

No to co biedna miałam zrobić, pogodzić się, że się nie da? Kupiłam podręcznik do prawa administracyjnego materialnego, aktywowałam próbny okres LEX-a i się wgryzłam w temat serio głęboko, poznając wcześniejsze orzecznictwo, na jakich to wszystko działa zasadach itp. – aż byłam w stanie wykazać, że właściwie nic w prawie nie zabrania zmienić imienia i nazwiska na niezgodne z płcią przypisaną i sprowadzić to do listy warunków, z których musi być spełniony przynajmniej jeden (spełniałam trzy) i listy warunków, które nie mogą występować (udowodniłam, że albo nie występują albo odmowa mi będzie jeszcze gorsza niż zgoda) i podparłam wyrokami NSA wskazującymi, że jak tak jest i jeśli obywatel mówi, żeby zmienić, to urząd nie ma nic do gadania i ma zmienić.

**Czyli miało się nie dać, a dało się! Na pewno jeszcze wrócę do kwestii nazwiska kończącego się na „-ska”, ale zmieńmy na chwilę temat, zaintrygowałaś mnie ogromnie. Jak wygląda ta kwestia diagnostyki transpłciowości oraz zmiany oznaczenia płci?**

Dzięki! Teraz jak musimy robić rozprawę sądową, to zwykle zabieramy się do tego na samym końcu, unikając biegłych jak ognia, bo niektórzy są serio okropni, a wszyscy sporo kosztują – a co jakby zrobić odwrotnie? Składamy pozew, wnosimy o zwolnienie z kosztów – trzeba dowieść, że się jest bez pieniędzy, ale z wydatkami tranzycji i naszym generalnym bogactwem to nie powinno być dla nikogo problemem. I co robimy? Wnosimy o powołanie biegłego, bo w końcu trzeba wykazać, że jesteśmy trans, co nie? Jak to my o to wnosimy, to możemy wskazać biegłych i problem trafienia na kogoś okropnego się rozwiązuje, przy okazji składamy wniosek o zwolnienie z kosztu biegłych, bo nadal nie mamy pieniędzy. No i sąd co ma zrobić, widzi, że kasy brak, a biegły potrzebny, wyznacza biegłego ze zwolnieniem z opłaty, biegły nas „bada”, stwierdza że jesteśmy trans, i cyk. Diagnoza jest, i to tak solidna, że zrobiona przez biegłego sądowego, nikt nie podskoczy. W prawie wszystkich sądach odbędzie się to szybciej niż czekanie na pierwsza wizytę finansowaną przez NFZ, a dodatkowo mamy z głowy rozprawę! Jeśli podejść do tego jako problemu, to może być koszmar, ale możemy do tego potraktować to jak narzędzie do osiągnięcia swoich celów!

Ja diagnostykę mam zrobioną, a z całością imienia i nazwiska poprawną nie wiem nawet, czy mam potrzebę zmieniać marker płci — a zwłaszcza jak to będzie niosło przy okazji kompletny biurokratyczny pierdolec związany ze zmianą PESEL-u, który w założeniach bardzo wielu systemów ma być niezmienny, więc super dużo rzeczy się na tym wywala.

**Czy to również wiąże się z pozwaniem rodziców? A jeśli nie, to jaki właściwie pozew trzeba złożyć?**

Tak, wszystko przebiega tak samo jak inne pozwy o ustalenie płci, poza tym że sami prosimy o wyznaczenie biegłych bez lat zbierania dokumentacji o tranzycji. Wiesz, że jak w 1991 roku Sąd Najwyższy ustalił procesową metodę zmiany oznaczenia płci, to wskazano, że to tylko tymczasowe rozwiązanie? ale trzeba pamiętać, że ustawodawca jest „bardzo zajęty” przemianami ustrojowymi… Mówienie, że teraz nie jest na to czas i są ważniejsze tematy ma bardzo długą tradycję, powinniśmy w odpowiedzi na to tylko krzyczeć głośniej.

**Czy słyszałaś o kimś, kto próbował tej metody?**

Nie, kompletnie nic. Mam nadzieję, że właśnie rozrzucam ziarna zmiany tego stanu – ale wiem, że to też wymaga kogoś mocno pewnego siebie i swojej tożsamości, a z tym bywa trudno na początku tranzycyjnej podróży. W moim wypadku już za późno żeby tego tak próbować. Zresztą na jakimś poziomie też odkładałam w czasie rozprawę, żeby dać sobie szansę dobrej walki o imię i nazwisko. 

**Wróćmy do nazwiska. Imię „Hanna” przeszło jako neutralne, a jak było z końcówką „-ska”? Jak ją uargumentowałaś?**

To trochę nie tak. W starym trybie imię mogłoby przejść jako neutralne, w tym, co ja w końcu zrobiłam, to imię przeszło jako używane. Wykazuję, że urzędu ma nie obchodzić czy jest neutralne, męskie, damskie czy inne. I tak samo nazwisko – wykazałam, że jest używane pokazując moją dokumentację medyczną, dokumenty z prac, umowę NDA, wskazując moją domenę talczewska.pl. Z uzasadnienia wynika, że niezerową rolę grało dowiedzenie, że moje stare dane nie korespondują z wyglądem i wywołują problemy życiowe, zdrowotne i potencjalnie niebezpieczeństwo, ale z prawnego punktu widzenia kluczowym faktem jest jakie jest imię używane. Ustawa wymienia jako dobry powód do zmiany nazwiska zmianę „na imię lub nazwisko używane”, a jedyne miejsce gdzie płeć się w niej pojawia, to w zdaniu „zmiana nazwiska oznacza zmianę na inne nazwisko, zmianę pisowni nazwiska lub zmianę nazwiska ze względu na formę właściwą dla rodzaju żeńskiego lub męskiego” – dosłownie żeby wymienić, że zmiana formy rodzajowej jest jedną z opcji.

Czy zawierałam we wniosku kawałki dowodzenia neutralności imienia? Tak, bo zbierałam absolutnie wszelką amunicję możliwą, ale nie powinno mieć żadnego znaczenia, zwłaszcza biorąc pod uwagę orzecznictwo i wskazania, które poznałam ze studiowania uzasadnienia decyzji w mojej sprawie.

**Napisałaś o swoim przypadku na grupach wsparcia. Z jakim odbiorem się spotkałaś? Czy dostałaś może wiadomości sugerujące, że ktoś zamierza podążyć Twoim śladem?**

Z bardzo pozytywnym odbiorem! Wiadomości częściej wskazywały chęć, żeby mieć to co ja, niż zrozumienie o co dokładnie chodziło z innym podejściu prawnym, ale docelowo to będzie to samo! Napiszę, być może we współpracy z portalem tranzycja.pl, poradnik z wyłożonym krok po kroku i w bardzo bezpośredni sposób o co chodzi, jak wnioskować z jakim info – i tych osób będzie więcej. Były osoby proszące o pomoc z tym w wiadomościach prywatnych – co trochę rozumiem, ale o rany ich jest dużo, a ja i tak mam mało czasu. Ktoś przypomniał o mojej zrzutce: „A żebym ocipiała!” – i jestem wdzięczna, bo ciężko mi samej przychodzi mówienie o niej na głos. Ludzie poprawiali mnie tam, gdzie było na to miejsce, np. o tym że zmian w stronę ze „-ska” na „-ski” było już trochę, pojawiły wątki, które mogą się stać innymi formami współpracy. To super uczucie, kiedy mogę sięczymś podzielić ze społecznością, a społeczność to radośnie przyjmuje!

**To rzeczywiście świetne wiadomości! A ja bardzo dziękuję za poświęcony czas.**

---

Hanna Talczewska przygotowała poradnik, w jaki sposób można zmienić imię i nazwisko na używane, w załącznikach do niego znajdziecie zarówno jej wniosek do USC, jak i uzasadnienie wniosku wskazujące, które elementy okazały się kluczowe.

<a href="/blog/poradnik-imie" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-file-signature"></span>
    Poradnik: Obowiązujące prawo pozwala zmienić imię i nazwisko na używane — niezależnie od formalnej płci
</a>

---

Tranzycję Hanny Talczewskiej możecie wesprzeć, wpłacając środki na jej zbiórkę [„A żebym ocipiała!”](https://www.facebook.com/hezort/posts/pfbid0UaPiKQkf4GzYcaXYihWeWpNXjroojp3PrYFrqk33fXSuScK2C4u84wd8nWaDhCfal).

<a href="https://zrzutka.pl/cz42tk" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-donate"></span>
    Wesprzyj na Zrzutka.pl
</a>
