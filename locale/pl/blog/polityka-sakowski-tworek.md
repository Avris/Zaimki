# Sprzeciwiamy się dezinformacji na łamach tygodnika „Polityka”

<small>2024-06-22 | [Kolektyw „Rada Języka Neutralnego”](/kolektyw-rjn), [Tess](/@tess)</small>

![](/img/pl/blog/polityka-sakowski-tworek.png)

Wraz z 43 organizacjami LGBT+ podpisałośmy przygotowane przez zespół Tranzycja.pl oświadczenie skierowane do redakcji „Polityki” będące odpowiedzią na publikację pełnego przekłamań artykułu Łukasza Sakowskiego i Dominiki Tworek.

<a href="https://tranzycja.pl/publikacje/oswiadczenie-organizacji-lgbt-tygodnik-polityka/" target="_blank" rel="noopener" class="btn btn-primary d-block-force my-4">
    <span class="fal fa-file-alt"></span>
    Treść oświadczenia
</a>

<a href="https://tranzycja.pl/publikacje/polityka-dezinformacji-sakowski-tworek/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-pencil"></span>
    Szczegółowa odpowiedź Tranzycja.pl na artykuł Sakowskiego i Tworek
</a>

Fakt, że tekst „Tranzycja: nie tak prędko? Może ostrożniej z decyzją o korekcie płci” w ogóle ukazał się na łamach „Polityki”, już jest skandalem. Łukasz Sakowski to chyba najbardziej skompromitowana osoba w kraju zajmująca się tematyką transpłciowości (choć bardziej precyzyjne byłoby stwierdzenie, że zajmuje się on głoszeniem transfobicznej agendy), krytykowana przez Polską Akademię Nauk [sic!], z kolei Dominika Tworek zasłynęła współpracą z portalem Holistic, na którym publikuje teksty takie jak „Edukacja seksualna w szkołach. Fundacje celebrytów wspierają seksbiznes kosztem dzieci”, „Kultura psychiatrycznych diagnoz patologizuje ludzkie doświadczenie”, „Świat koncentruje się na urazach, a kultura ofiar kwitnie. Wszyscy możemy czuć się obrażani”, „Aktywiści zmuszają nas do przyjęcia nowej wersji języka polskiego. Usprawiedliwia ona to, co dawniej było niedopuszczalne” czy „Poliamoria, swingowanie, otwarte związki. Jakie niebezpieczeństwa płyną z normalizacji dewiacyjnych relacji?”.

Na portalu Polityka.pl pojawiła się odpowiedź Karola Jałochowskiego „Tranzycja: trzy razy »tak«. »Polityka« odpowiada na listy po publikacji tekstu”.

<a href="https://www.polityka.pl/tygodnikpolityka/nauka/2260821,1,tranzycja-trzy-razy-tak-polityka-odpowiada-na-listy-po-publikacji-tekstu.read/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-pencil"></span>
    Odpowiedź „Polityki”
</a>

Już sam tytuł stanowi kuriozum – bez publikacji listów redakcja publikuje odpowiedź na nie. Jakby… miała coś do ukrycia?

> Tak – umknęło naszej uwadze, że współautor tekstu znany jest z wypowiedzi, których nie sposób uzasadnić wiedzą naukową. (…) Tak – dzięki doświadczeniu i głębokiej wiedzy specjalistów z i spoza organizacji reprezentujących społeczność LGBT+ dostrzegamy dziś, że autorzy tekstu dopuścili się nadinterpretacji. Decyzja o publikacji artykułu wynikała z zaufania pokładanego w ich kompetencje oraz przekonania, że nauka jest dziedziną żywą – i że konieczne są badania doprecyzowujące szczegóły opieki afirmującej płeć u osób niepełnoletnich.

To tłumaczenie jest skandaliczne – na jakiej podstawie redakcja „pokładała zaufanie w kompetencje” tych dwóch osób? Czy tekst został zamówiony przez redakcję (a jeśli tak, to na jakiej podstawie właśnie u nich), czy też nadesłany (dlaczego redakcja nie podjęła żadnej próby weryfikacji, kim są osoby autorskie)?

Na wszelki wypadek jednak podrzucamy, [co o publikacji napisała Dominika Tworek]( https://www.facebook.com/dominika.tworekk) – tak żeby tym razem to nie umknęło: 

> Kochani, dziś otwieramy szampana! Meeega się cieszę na ten news!

> Razem z dziennikarzem naukowym @Lukasz Sakowski robimy rewolucję w mainstreamowych mediach w myśleniu o transpłciowości i zmianie płci!

> W najnowszym tygodniku Polityka (od jutra w kioskach, od dziś online) znajdziecie nasz tekst CZAS NA ZMIANĘ, w którym - powołując się na najnowsze badania naukowe - piszemy o negatywnych konsekwencjach przedwczesnych tranzycji. Współczesna nauka mówi wprost: zmiana płci nie tylko nie pomaga, ale wręcz szkodzi młodym ludziom.

> Podczas gdy w Polsce przeżywamy jeszcze entuzjazm dotyczący zmiany płci, zachód Europy i USA już mierzą się z jej szkodami – obserwujemy tam obecnie falę detranzycji młodych ludzi. Dlatego tak cieszy mnie ta publikacja. Mam szczerą nadzieję, że między innymi tym tekstem uda się uchronić polskie dzieci przed cierpieniem.

Kłamstwa o „fali detranzycji” (pytana o dane, autorka uparcie odwraca kota ogonem, że w artykule to sformułowanie nie padło), sprzeczne z nauką i transfobiczne określenie „zmiana płci” oraz oczywiście kłamstwa o tym, co mówi nauka. Dominika Tworek nie ukrywa radości z powodu tego, że udało jej się dotrzeć z taką agendą do mainstreamu – w końcu publikacje na portalu Holistic kompromitują się same przez się. Do tego posta Karol Jałochowski nie odniósł się w żaden sposób.

Wróćmy jednak do tego, do czego się odniósł:

> Tak – dyskusję na temat transpłciowych dzieci i ich prawa do opieki medycznej podjęliśmy w sposób niewłaściwy. Uczyniliśmy to jednak nie z intencją ataku na tę być może najwrażliwszą grupę społeczną. Przeciwnie – naszą niezmienną, wielokrotnie wyrażaną na łamach „Polityki” troskę chcieliśmy osadzić w bezdyskusyjnym kontekście bieżących badań naukowych na ten temat.

Naprawdę trudno nam uwierzyć, że temat transpłciowości został podjęty przez redakcję z powodu „troski”, a jednocześnie zupełnym przypadkiem za tekst odpowiadały osoby zajmujące się szerzeniem transfobicznej agendy, przy pominięciu osób aktywistycznych. Zdajemy sobie sprawę, że nie wszystkie osoby muszą być na bieżąco ze stanem wiedzy naukowej dotyczącej transpłciowości, w tym tranzycji osób nieletnich. 

Załóżmy, że redakcja „Polityki” dowiedziała się o publikacji naukowej i zaniepokoiła się, czy transpłciowe dzieci są bezpieczne. Redakcja niebazująca na transfobicznych uprzedzeniach zakładałaby kontakt z osobami eksperckimi oraz aktywistycznymi. Z kim? Na przykład z osobami reprezentującymi portal Tranzycja.pl, Fundację Trans-Fuzja czy Mają Heban ze stowarzyszenia Miłość Nie Wyklucza. Z dr. n. med. Bartoszem Grabskim współredagującym kompendium „Dysforia i niezgodność płciowa” czy z prof. dr hab. Wojciechem Draganem należącym do Rady Upowszechniania Nauki Polskiej Akademii Nauk.  

> Mówiąc trzy razy „tak”, podkreślamy gorącą nadzieję, że wciąż będziemy spotykać się na łamach „Polityki”. A przede wszystkim, że transpłciowi nastolatkowie w Polsce i na świecie będą dorastać jako szczęśliwi transpłciowi dorośli. O nic innego nam nie chodzi.

> Szczegółowe omówienie tekstu Dominiki Tworek i Łukasza Sakowskiego, przygotowane przez zespół [tranzycja.pl](https://tranzycja.pl/publikacje/polityka-dezinformacji-sakowski-tworek/), można znaleźć na tejże stronie. My zaś będziemy wracać do tematu – rzetelnie, rzeczowo, a przede wszystkim wspólnie – w najbliższej przyszłości.

I to jedyna pointa. Redakcja „Polityki” opublikowała na swoich łamach skandaliczny, sprzeczny z wiedzą naukową, transfobiczny tekst, ale nie zdobyła się nawet na słowo: „Przepraszam”. 

Transfobiczny tekst Łukasza Sakowskiego i Dominiki Tworek cały czas jest dostępny na portalu bez słowa sprostowania, ale nie ukazało się tam nadesłane oświadczenie organizacji LGBT+. Pojawiło się „wyjaśnienie” bez przeprosin, z mętną zapowiedzią, że redakcja „będzie wracać do tematu”. W jaki sposób? Czy na łamach papierowej „Polityki” ukaże się oświadczenie naszej społeczności? Czy ukaże się tam tekst zredagowany przez zespół Tranzycja.pl tekst prezentujący, jak ogromnych manipulacji dopuścili się Łukasz Sakowski i Dominika Tworek? Czy doczekamy się chociaż tego minimum przyzwoitości, by na portalu w miejscu transfobicznego artykułu pojawiły się przeprosiny?

W obecnej sytuacji trudno nam uwierzyć, że redakcji „Polityki” chodzi o to, by nastoletnie osoby transpłciowe nastolatkowie mogły dorastać jako szczęśliwe dorosłe osoby transpłciowe. Raczej o damage control (bo nawet o non-apology nie da się tutaj mówić).

– Zespół Tranzycja.pl wykonał TYTANICZNĄ pracę, bite dwie doby siedząc nad tym debunkiem. Opowiadanie kłamstw jest łatwe, prostowanie ich to ogromny wysiłek. DZIĘKUJĘ – napisała Maja Heban, a my możemy się pod tym tylko podpisać.

A do wszystkich osób czytających nas zwracamy się z apelem – udostępniajcie [artykuł zespołu Tranzycja.pl](https://tranzycja.pl/publikacje/polityka-dezinformacji-sakowski-tworek/) i wszelkie informacje na ten temat, tagujcie „Politykę”, wysyłajcie do niej listy. Skoro zabrakło przyzwoitości, niech zadziała nacisk. 
