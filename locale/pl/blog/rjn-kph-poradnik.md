# Jak używać prawidłowych zaimków? Porady dla organizacji pozarządowych'

<small>2024-10-16 | [Kolektyw „Rada Języka Neutralnego”](/kolektyw-rjn) | [Kampania Przeciw Homofobii](https://kph.org.pl/)</small>

![](/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-1.jpg)

{gallery={
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-2.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-3.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-4.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-5.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-6.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-7.jpg": "",
    "/img/pl/blog/rjn-kph-poradnik/rjn-kph-poradnik-8.jpg": "",
}}

<a href="https://www.facebook.com/neutratywy/posts/854362353516080" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fab fa-facebook"></span>
    Post na profilu KPH
</a>
