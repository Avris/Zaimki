# List otwarty od polskiej społeczności asów do Wydawnictwa Czarne

<small>2024-03-03 | [Aseksualni Polska](https://www.facebook.com/groups/aseksualni.polska/)</small>

<div class="alert alert-info">
    <p>
        <span class="fal fa-info-circle"></span>
        To jest wpis gościnny.
    </p>
    <p>
        Drogie Osoby,<br>
        w porozumieniu ze społecznością grupy <a href="https://www.facebook.com/groups/aseksualni.polska/" target="_blank" rel="noopener">Aseksualni Polska</a> publikujemy list otwarty
        do <a href="https://czarne.com.pl/" target="_blank" rel="noopener">Wydawnictwa Czarne</a>, które opublikowało polską edycję książki 
        <a href="https://czarne.com.pl/katalog/ksiazki/asy" target="_blank" rel="noopener">„Asy. Czego może nauczyć nas aseksualność”</a> Angeli Chen
        w przekładzie Aleksandry Kamińskiej. List ukazał się także w mediach społecznościowych Stowarzyszenia Asfera
        (linki: <a href="https://www.instagram.com/p/C4GKZl6obwt/" target="_blank" rel="noopener">instagram</a>, <a href="https://www.facebook.com/aseksualizm/posts/pfbid0Bx9zeSdsyrK2TVq5SnwzECnE9P7QV7VgAoMaQ9uFXfWDbGfh2vzPUqnSkzGiS2Pkl" target="_blank" rel="noopener">facebook</a>). 
    </p>
</div>

Szanowne Wydawnictwo Czarne,

jako polska społeczność osób aseksualnych chcielibyśmy pogratulować wydania przekładu książki Angeli Chen – „Asy. Czego może nas nauczyć aseksualność”.

Z własnego doświadczenia wiemy, jak złożony jest temat aseksualności: podważa on normatywne podejście oraz kulturowo i społecznie zakorzenione założenia, stereotypy i mity dotyczące różnych typów relacji międzyludzkich, związków czy seksualności człowieka. Tym bardziej należy cieszyć się, że jedna z ważniejszych anglojęzycznych publikacji poświęconych tematyce aseksualności ukazała się w języku polskim. Doceniamy też pracę włożoną w to, by przekład był jak najbardziej inkluzywny.  

Informacja o planach wydania tej książki wywołała jednak wśród osób członkowskich naszej społeczności dezorientację i obawę o jakość przekładu. Polska społeczność osób aseksualnych istnieje od wielu lat i przez ten czas wypracowała język oraz sposób rozmowy o aseksualności. Sytuacja, w której ukazuje się przekład publikacji o aseksualności, bez jednoczesnej konsultacji z osobami członkowskimi tejże społeczności, budzi nasze zdziwienie i niezrozumienie. Osoby aseksualne są wciąż najmniej dostrzeganą mniejszością LGBTQIA – pominięcie naszego udziału w przekładzie publikacji dotyczącej nas samych ilustruje istotę tego problemu.

Ponieważ zdecydowana większość terminologii związanej z aseksualnością wywodzi się z języka angielskiego, jesteśmy świadomi problemów, jakie mogą wystąpić w trakcie przekładu. Mogą one wynikać z wciąż niskiej świadomości społecznej i językowej związanej z aseksualnością, jak i problemów charakterystycznych dla języka polskiego.

Jedną z kluczowych trudności jest synonimiczne używanie w języku polskim pojęć pociągu seksualnego, czyli pożądania [ang. sexual attraction, lust] i popędu seksualnego, czyli libido [ang. sex drive]. Znaczenie tych terminów jest odmienne i uświadomienie sobie tej różnicy jest kluczowe dla zrozumienia naszych doświadczeń. Pierwszy z wymienionych terminów odnosi się do pragnienia aktywności seksualnej, które jest mimowolną reakcją emocjonalną ukierunkowaną na konkretną osobę (co definiuje orientację seksualną). Drugi odnosi się do stanu psychofizycznego, który nie jest nakierowany na nikogo. Upraszczając: aseksualność definiowana jest nieodczuwaniem pociągu seksualnego, ale osoba aseksualna może mieć dowolny – również bardzo wysoki – poziom libido.

Nieścisłości pojawiające się w polskim przekładzie „Asów”, zmieniają kluczowy sens niektórych zdań. Przykładowo: „The gold-star asexual will be the savior of us all, the one who can prove that asexuality is legitimate simply because there is not a single other factor that could have caused their lack of sexual attraction”, przełożone zostało jako “As na medal ma zostać naszym zbawcą i ostatecznie udowodnić, że aseksualność to pełnoprawna orientacja, ponieważ to ktoś, kto nie posiada żadnych cech, którymi dałoby się wytłumaczyć jego brak libido”. I dalej: „Lust was not as much of a struggle for him as it was for others. He did not consider that lust might not be a struggle at all, that he had invented a struggle because he was told that he must have something to struggle against”. przełożone jako „Popęd seksualny nie stanowił dla niego takiego wyzwania jak dla innych. Nie przyszło mu jednak do głowy, że może po prostu nie ma się z czym zmagać i że wymyślił sobie trudności, ponieważ wszyscy dookoła kazali mu się ich spodziewać”.

Problemy ze stosowaną terminologią  pojawiają się w publikacji przynajmniej kilkukrotnie, wprowadzając czytelnika w błąd, jednocześnie podważając samą definicję aseksualności. Co ważne, przestrzega przed tym sama autorka. Zdajemy sobie sprawę, że problemy te nie muszą wynikać wprost ze złej woli lub braku kompetencji osoby tłumaczącej – przydatne na tym polu mogą być jednak konsultacje z osobami eksperckimi, zrzeszonymi na przykład w Stowarzyszeniu Asfera.

Jednym z narzędzi opisywania (nie tylko) aseksualności jest Split Attraction Model (SAM), który w swojej publikacji wykorzystuje również Angela Chen. Model ten zakłada możliwość występowania wielu – różnych i niezależnych od siebie – form pociągu: seksualnego, romantycznego, estetycznego, sensualnego czy platonicznego. Tłumaczenie słowa attraction – gdy nie jest ono sprecyzowane jako pociąg seksualny – jako pożądanie lub pociąg seksualny nie zawsze będzie więc w danym kontekście właściwe.

Jak pisze Chen: „A careful balancing act is necessary. Aces understand by isolating, via negativa, by splitting attraction into categories and then calling it a model, but that gives everything a gloss of scientific legitimacy that almost no explanation can fully claim. It is equally important to acknowledge that, all too often, these different attractions blend together and cannot be cleanly extracted”. Autorka, mówiąc o modelu, pisze o jakiejś formie pociągu, a nie wprost o pożądaniu czy pociągu seksualnym. Tymczasem w przekładzie konkluzja brzmi: „Potrzebujemy ostrożnej równowagi. Asy osiągają zrozumienie na drodze eliminacji, via negativa, dzieląc pociąg seksualny na poszczególne kategorie [...] Dlatego równie ważne jest przyznanie, że często mamy do czynienia z mieszanką różnych kategorii pożądania, których nie sposób wyodrębnić’.

Społeczność osób aseksualnych przez lata stworzyła wiele pojęć, terminów i narzędzi, jakimi się posługuje – a także ich polskie odpowiedniki – aby precyzyjnie opisywać własne doświadczenia. Uszanowanie tej pracy jest dla nas ważne. Tymczasem mamy poczucie, że przekład tej książki – pomimo że traktuje ona o nas – w zupełności nie jest nasz, a udział polskiej społeczności osób aseksualnych został z procesu wydawniczego całkowicie wymazany. W myśl zasady „nic o nas bez nas”, nie mogliśmy przejść obok tej kwestii obojętnie. Tym bardziej, że jeszcze przed publikacją osoby aseksualne kontaktowały się z wydawnictwem, oferując pomoc i wskazując na błędy istniejące w opublikowanych w ramach promocji fragmentach i opisie książki.  

Aseksualność nie jest wyłącznie ideą czy pewnym podejściem do rozważenia, które rozszerza świadomość psychoseksualną osób, dla których ta orientacja seksualna dotychczas nie istniała. Dla nas aseksualność to przede wszystkim ludzie – szeroka, otwarta i wzajemnie wspierająca się społeczność, stanowiąca istotną część kultury queer. Funkcjonujemy w społecznościach internetowych i lokalnych, działamy w tęczowych stowarzyszeniach, wreszcie zakładamy własne, jak Stowarzyszenie Asfera.

Według naszej wiedzy w czasie prac poprzedzających wydanie książki żadna z osób członkowskich naszej społeczności, ani żadne tęczowe stowarzyszenie – a przede wszystkim Stowarzyszenie Asfera – nie brały udziału w konsultacjach zarówno nad tłumaczeniem, jak i nad szerszym procesem wydawniczym czy promocyjnym. Książka traktująca o nas została wydana bez uwzględnienia naszego udziału, głosu, uwag i – co najważniejsze – istnienia.

Wciąż jednak – tak jak wyraziliśmy gotowość do konsultacji – wyrażamy gotowość pomocy przy powstaniu erraty.

Z wyrazami szacunku,  
— Osoby reprezentujące polską społeczność osób aseksualnych
