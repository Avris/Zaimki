# Poradnik języka neutralnego płciowo

<small>2021-08-28 | [@Sybil](/@Sybil)</small>

Prezentacja Sybila stworzona na V Kongres Anarchistyczny Kongresono (kliknij na obrazek, aby otworzyć).

<p>
    <a href="/docs/pl/Poradnik języka neutralnego płciowo.pdf" target="_blank">
        <img src="/img/pl/blog/poradnik-języka-neutralnego-płciowo.png" class="hero" alt="Prezentacja w formacie PDF">
    </a>
</p>

<p>
    <a href="/docs/pl/Poradnik języka neutralnego płciowo.pdf" target="_blank" class="btn btn-primary d-block-force">
        <span class="fal fa-file-pdf"></span>
        Prezentacja
    </a>
</p>
