# nieznany i kropka – „TrójJednia”

<small>2024-04-19 | [@andrea](/@andrea)</small>

![Na drewnianym biurku książka o srebrnej okładce stylizowanej na laptopa, obrócona o 90°, jak zamknięty laptop, pośrodku zamiast loga firmy produkującej – czarny garnitur/płaszcz z trzema rękawami.](/img/pl/blog/nieznany-trojjednia.png)

Nie wiem, jak zacząć… Kłębi mi się w głowie wiele nieuczesanych myśli na temat tej książki…
Okay, może tak: jeśli lubicie Gombrowicza, to zapewne polubicie też autora nieznanego.
Przypomina mi Gombrowicza. Przypomina mi też, jak wyglądają moje myśli, gdy się porządnie zjaram
(ech, gdybym tylko miało wtedy siłę je spisywać…). Gdzie umysł pójdzie, tam jest.
Tworzy bez przejmowania się zasadami czy gatunkami. Produkuje kilkaset stron ambitnej prozy poetyckiej.

W TrójJedni Fabuła przegrywa z dygresjami. To strumień świadomości, potok myśli. 
Każda myśl ma prawo do ubrania w słowa – ale zanim trafi na papier,
musi być wielokrotnie przemyślana i dopieszczona. Wszystko jest na temat, wszystko jest na miejscu.

Językowo? TrójJednia to kopalnia słowotwórczych perełek. Autor bawi się skojrzeniami i odważnie ubiera je w literki.
Spisałom sobie na przykład „śmierciątka” czy „męskobiecą twarz”. Językowo jest też inkluzywnie – oczywiście.
Dla przykładu, jak piesa jest płci żeńskiej, to jest „piesą”, a nie żadną „psem”.

Jednak nie zwierzęce feminatywy czy neologizmy re-analizujące rzeczywistość są powodem,
dla którego książka autora nieznanego trafiła w moje łapki
– a fakt, że obok Niej i Niego jedną z trzech głównych postaci TrójJedni jest Ono.
Ono nie jest binarne, wiadomo. Ono przechodzi tranzycję. Ono sprawia lingwistyczne komplikacje.
Ono zasługuje na konsultację przed publikacją.

Użycie zaimka w roli imienia jest o tyle niełatwe, że w normatywnej polszczyźnie w niemal wszystkich formach fleksyjnych
„[ono](/ono)” jest identyczne z „[on](/on)”: „tęsknię za _jego_ śmiechem”, „chciałobym _go_ posłuchać”, „poszłom z _nim_ do szkoły” –
mowa tu o „[on]/nim” czy „[ono]/nim”, huh?

Skoro odmiana sprawia problemy, to można by spróbować nie odmieniać: np. „Dla Ono sukienka nie ma płci” – ale w tak silnie fleksyjnym
języku jak polszczyzna nie brzmi to naturalnie. Podrzuciłom autorowi dwa możliwe rozwiązania:
użycie neologicznych form [ono/jeno](/ono/jeno), np. „Dla Nieno sukienka nie ma płci” – co jednak raczej wymagałoby
jakiegoś wyjaśnienia w (nieistniejącym) wstępie czy przypisie – oraz traktowanie „Ono” jako rzeczownika
odmienianego jak każdy inny rzeczownik rodzaju neutralnego: Ono/Onego/Onemu/z Onym/o Onym, np. „Dla Onego sukienka nie ma płci”.
Autor zdecydował się na tę ostatnią opcję.

Więcej cytatów znajdziecie [w naszym Korpusie Niebinarnej Polszczyzny](https://zaimki.pl/korpus#tr%C3%B3jjednia),
natomiast tutaj pozwolę sobie wkleić jeden, dobrze ilustrujący zarówno użyte formy, jak i ogólny styl książki: 

> Dla Onego sukienka nie ma płci. Dlatego nie zdecydowało się oddać wszystkich swoich przyjaciółek.
> Choć miało ochotę, wesprzeć szafy innych. Chciało sobie odjąć od ust, ale nie mogło. To i kieszeń, i miłość.
> Kieszeń, bo ubrania potrafią odpłacić. Tym, że wciąż są. I dobrze leżą, i dbają o grubszy portfel.
> Pasują Onowemu podejściu do świata – mniejszym śladem węglowym. Ubrania są rozumne, podchodzą z dużą zielenią do Onowych wydatków modowych.

TrójJednia wymyka się ograniczeniom formy i języka tak jak niebinarność wymyka się ograniczeniom patriarchatu.  

Nie wiedziałom jak zacząć – i szczerze mówiąc, nie wiem też jak zakończyć. Chyba najlepiej najprościej: **polecam** 😉

<a href="https://allegro.pl/oferta/trojjednia-niech-umarli-przewracaja-sie-w-grobach-15243156190" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-5">
    <span class="fal fa-shopping-basket"></span>
    Zamów na Allegro
</a>

<a href="https://www.instagram.com/nieznanyikropka/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-instagram-square"></span>
    Śledź autora na Instagramie
</a>

<a href="https://www.facebook.com/nieznanyikropka" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-facebook-square"></span>
    Śledź autora na Facebooku
</a>

