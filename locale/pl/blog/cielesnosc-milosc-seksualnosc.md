# Cielesność, miłość, seksualność – Charline Vermont

<small>2024-03-19 | [@andrea](/@andrea)</small>

![Okładka recenzowanej książki](/img/pl/blog/cielesnosc-milosc-seksualnosc.png)

Podesłano nam do zrecenzowania polskie wydanie książki „Cielesność, miłość, seksualność”
francuskiej seksuolożki Charline Vermont. I muszę przyznać, że czytanie jej co chwilę przynosiło mi uśmiech na twarz.
Gdybym tylko miało dostęp do takich materiałów, gdy samo byłom dzieckiem!

Jest to podręcznik poruszający tematy takie jak ludzkie ciało, intymność, dojrzewanie, zgoda, związki,
czy oczywiście słynne pytanie „skąd się biorą dzieci?”. Nie jest kierowany wyłącznie do rodziców
szukających pomocy, jak te tematy poruszyć ze swoimi dziećmi, ani wyłącznie do dzieci,
które szukają odpowiedzi na nurtujące je pytania – lecz jest to książka do czytania całą rodziną.
Każdy rozdział zawiera wstęp z radami dla rodziców (a na początku ksiązki jest też „Zestaw narzędzi” –
świetnych rad, jak rozmawiać z dzieckiem na trudne tematy) oraz garść pytań i odpowiedzi kierowanych do dzieci,
z podziałem na grupy wiekowe (choć nie jest to podział sztywny, rodzice są zachęcani, by samodzielnie
ocenić dojrzałość emocjonalną dziecka).

Bardzo ważną cechą podręcznika jest fakt używania języka inkluzywnego – co w tematyce płciowości
jest niezmiernie istotne, a niestety rzadkie. Cytując autorkę:

> Dlaczego warto używać języka inkluzywnego?
> 
> Po pierwsze dlatego, że nie ma żadnego powodu, by stawiać rodzaj męski ponad żeńskim. Po drugie ponieważ binarne podejście do płci zostało nam narzucone i możemy to zmienić. A zmiany zachodzą również poprzez słowa, których używamy. 😉
>
> Ponadto gdy mowa o anatomii czy genitaliach, lepiej wspominać o osobach z penisem czy osobach z łechtaczką/macicą: istnienie osób transpłciowych i niebinarnych (tym bardziej dzieci) jest faktem. Dlatego nie możemy zakładać, że dziecko, które przyszło na świat z penisem, jest chłopcem, a urodzone z łechtaczką – dziewczynką.
>
> W książce staramy się używać takiego języka, by nie dopuścić u dzieci do poczucia wykluczenia.
>
> Ale poradnik ten absolutnie nie jest skryptem do wyuczenia się na pamięć. Ma ci towarzyszyć, ma cię wspierać i ma ci pomagać.
>
> Jeśli czujesz się lepiej, stosując słowa „kobiety”, „mężczyźni”, „dziewczynki”, „chłopcy” – używaj właśnie ich!

Niezmiernie cieszy nas promowanie i normalizowanie takiego podejścia do kwestii binarności płci i inkluzywności –
dzięki niemu queerowe dzieci pewnego dnia nie będą już musiały przechodzić przez traumę czucia się inne i wyobcowane,
lecz od samego początku będzie im łatwiej poznać siebie i wiedzieć, że w różnorodności płci i seksualności nie ma nic złego.

Aczkolwiek nie byłobym sobą, gdybym nie dopatrzyło się paru niedociągnięć w kwestii inkluzywnego języka.
Sformułowanie „obu płci” można by zastąpić zwrotem nie sugerującym, że istnieją tylko dwie, np. „wielu płci” lub „wszystkich płci”.
Podrozdziały „Droga Mamo, Drogi Tato” lepiej włączałyby niebinarnych rodziców, gdyby nazywały się po prostu „Drodzy Rodzice”.
A [przestarzałe](/inkluzywny#seksualista) „homoseksualista/homoseksualistka” w słowniczku pojęć warto by zastąpić
przez „osoba homoseksualna”. Ale to już naprawdę pedantyzm z mojej strony, wciąż ogromne plusy za inkluzywny język!

Co również cieszy, to fakt, że dzieci traktuje się poważnie – nawet najmłodsze grupy wiekowe otrzymują
rzetelne informacje podane w sposób przystępny, ale nie infantylny. Chyba najlepiej ilustruje to ten fragment
(kierowany do rodziców):

> Jak nazywasz nos swojego dziecka? A uszy? Prawdopodobnie mówisz „nos” i „uszy”.
>
> W takim razie jak nazywasz intymne części ciała?
>
> Pewnie usłyszę „ptaszek”, „siusiaczek”, „cipcia”, „myszka”. 😉
>
> Zapytałam już wcześniej, dlaczego nazywając tylko te części ciała, używamy sformułowań dziecięcych, a nie anatomicznych, które wybieramy dla pozostałych narządów.
>
> A gdyby tak używać prawdziwych słów?
>
> Wiele osób wyrosło w takim otoczeniu językowym, jak gdyby oczywiste było, że nazw „tego tam między nogami” się nie wypowiada. Kogo chcemy chronić, gdy mówiąc o genitaliach, używamy słów innych niż nazwy anatomiczne? Dzieci czy raczej dorosłych, niechętnych odebraniu tym słowom znaczenia seksualnego?
>
> Penis, pochwa, łechtaczka, jądra są przede wszystkim częściami ludzkiego ciała, a dopiero w dalszej kolejności źródłem przyjemności i fantazji czy obiektem pożądania.
>
> Zmiany zachowań społecznych zaczynają się od zmian w języku!
>
> Ewolucja (czy rewolucja?) w edukacji, dzięki której możemy być pierwszym pokoleniem rodziców wychowującym dzieci w duchu pozytywnej edukacji seksualnej, zaczyna się od doboru odpowiednich słów!

Podręcznik wyjaśnia nie tylko cishormatywne „podstawy” wiedzy o seksualności człowieka,
ale przybliża również kwestie transpłciowości, niebinarności i interpłciowości.
W rozdziale o związkach tłumaczy nie tylko, jak działają te „konwencjonalne”, ale również,
że w porządku jest kochać osoby różnych płci, nie chcieć być w związku albo chcieć być w kilku związkach naraz.

Autorka tłumaczy kwestie pytania o zgodę oraz stawiania granic nie tylko w kontekście seksu (co jest oczywiście niezmiernie ważne),
ale też tłumacząc na przykład, dlaczego ciotka próbująca na siłę dać ci buziaka nie ma prawa tego robić.
Kwestie intymności, wstydu i zgody przewijają się przez cały podręcznik, a hasło „Twoje ciało – Twój wybór” powtarza się jak mantra.

W podręczniku poruszone są chyba wszystkie tematy, których po tego typu publikacji należało by się spodziewać:
wspomnę choćby o menstruacji, owłosieniu ciała, antykoncepcji, przebiegu ciąży czy pomocy w odróżnieniu pornografii od rzeczywistości.
Natomiast jest parę kwestii, których obecność mnie bardzo pozytywnie zaskoczyła:
dzieci są uczone, jak budować poczucie własnej wartości ❤️,
pytanie o zakochaniu zilustrowane jest chłopcem na wózku inwalidzkim ❤️,
a dzieci nie muszą się martwić o to, że ich intymne części ciała są „dziwne”
dzięki załączeniu rysunków przedstawiających naturalną różnorodność piersi, penisów i sromów ❤️

Szczerze mówiąc, czuję się trochę staro, gdy widzę, jak w przeciągu pokolenia diametralnie zmienia się nasze podejście
do edukacji seksualnej 😅 Ale zmienia się na lepsze – więc choć trochę młodszym pokoleniom zazdroszczę,
to jednak przede wszystkim się cieszę z tego, jak wiele problemów zmyślonych przez patriarchat, queerfobię i tabu
ich już w dużej części nie bedzie dotykało. A książka Charline Vermont jest zdecydowanie istotnym elementem
tej pożytecznej zmiany społecznej.

<a href="https://purana.com.pl/ebook-cielesnosc-milosc-seksualnosc-120-pytan-ktore-zada-ci-twoje-dziecko-charline-vermont-kopia/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-5">
    <span class="fal fa-shopping-basket"></span>
    Zamów na stronie Wydawnictwa Purana
</a>
