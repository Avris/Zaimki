# Renata Kim – „Pomiędzy. Historie osób niebinarnych”

<small>2024-04-01 | [@andrea](/@andrea)</small>

![Na czerwonym tle okładka książki: przeplatane palce, z lewej strony ze szpiczastymi paznokciami pomalowanymi na czerwono, z prawej ucięte krótko i niepomalowane. U góry napis „Renata Kim”, na dole „Pomiędzy”, poniżej „Historie osób niebinarnych” i logo wydawnictwa Luna](/img/pl/blog/renata-kim-pomiedzy.png)

Cudownie, gdy osoba publiczna z dużą platformą i dużymi zasięgami podejmuje się tematu niebinarności, wprowadzając go coraz szerzej do mainstreamu.
Jeszcze lepiej – gdy zamiast mówić o nas bez nas, robi to, oddając głos samym osobom niebinarnym, ich bliskim oraz ekspertkom.
24 kwietnia premierę będzie miała książka Renaty Kim „Pomiędzy. Historie osób niebinarnych” – a [Kolektyw „Rada Języka Neutralnego”](/kolekty-rjn)
z przyjemnością udziela jej mecenatu!

Niebinarność to nie tylko zaimki. To nie moda, nie trend, nie dziwactwo, nie wynik indoktrynacji.
Wiele osób myśli tak tylko dlatego, że nie zwyczajnie nie miały okazji, czasu czy potrzeby zapoznać się z tematem –
a przecież osoby niebinarne istniały i były akceptowane w wielu kulturach na przestrzeni wieków.
Byłyśmy, jesteśmy i będziemy. Rozmowy Renaty Kim sprawiają jednak, że możemy być bardziej widoczne i lepiej zrozumiane.

> Każdy jest w stanie sobie wyobrazić, jak to jest być niewidomym, bo każdy z nas zamyka oczy.
> Ale nie wie, jakie to uczucie posiadać niepasujące narządy płciowe.
>
> — Lu (ona/jej)

„Pomiędzy” podchodzi do tematu niebinarności wszechstronnie: jest o odkrywaniu swojej tożsamości, poszukiwaniu źródeł informacji,
o coming outach, życiu codziennym, o zaimkach, języku, imionach, o sytuacji prawnej, o stanowisku nauki,
o romansach, czy wreszcie: o dyskryminacji i wykluczeniu.

> – Jak to powiedzieć ładnie? Czuję się wykluczona ze sfery prawnej, bo ja w polskim prawie nie istnieję.
> Chcę się zajmować prawami człowieka, żeby nikt się nie czuł wykluczony przez to, jaki się urodził.
> Bo w Polsce niestety często tak jest. Nie tylko osoby LGBT+, ale też na przykład osoby z niepełnosprawnościami,
> ich rodzice czy imigranci. Wszyscy oni są wykluczeni. Nie ma na to mojej zgody. Polska nie może tak wyglądać.
> 
> — Renée (on/jego, ona/jej)

Ale choć życie jako wyoutowana osoba niebinarna pełne jest kłód rzucanych przez społeczeństwo pod nogi,
to gdy czytałom wywiady z „Pomiędzy”, bardziej mi się rzucały w oczy motywy pozytywne – przede wszystkim uczucie ulgi:
czy to związanej z odkryciem wreszcie, kim się jest, czy z faktem zaakceptowania siebie i bycia akceptowanx przez otoczenie,
czy z poczuciem, że nie jest się samx, czy z przejściem coming outu lub różnych rodzajów tranzycji.
Osobiście bardzo do mnie przemówiła następująca wypowiedź
o kochaniu siebie i wiedzeniu najlepiej, co dla kogo jest dobre:

> Ludziom o wiele łatwiej jest uwierzyć, że istnieje jakaś istota wyższa, jakiś Pan Bóg, który nas kocha i akceptuje,
> który zna nas jak nikt inny i zawsze przy nas będzie, nawet jeśli wszyscy inni nas zostawią,
> i który wie, co jest dla nas dobre, niż uwierzyć, że my sami jesteśmy tym wszystkim dla siebie.
>
> Doznałem objawienia, bo już stałem się na nie gotowy w drodze do samoakceptacji.
> Zrozumiałem, że tym Bogiem cały czas byłem ja. To nie jest tak, że Pan Jezus mnie kocha, a ja się nauczyłem przyjmować jego miłość.
> To ja siebie kocham i siebie akceptuję. I wiem, co jest dla mnie dobre.
>
> — Hihi (on/jego)

Oprócz wywiadów, książka zawiera też wstęp od autorki, poradnik, jak mówić do i o osobach niebinarnych, zdjęcia wypowiadających się osób,
glosariusz przydatnych terminów oraz listę organizacji LGBTQ+, organizacji oferujących całodobowe wsparcie i publikacji naukowych nt. niebinarności
przygotowaną przez [Szymona Miśka](/@szymon) z naszego Kolektywu. 
Koniecznie muszę też wspomnieć o cudownych ilustracjach autorstwa Dawida Grzelaka!
Załączam trzy spośród tych udostępnionych nam przez wydawnictwo do celów promocyjnych:

{gallery={
    "/img/pl/blog/pomiedzy-ilustracja-1.png": "",
    "/img/pl/blog/pomiedzy-ilustracja-2.png": "",
    "/img/pl/blog/pomiedzy-ilustracja-3.png": "",
}}

„Pomiędzy” to zbiór fascynujących rozmów, które nie tylko edukują, ale też w wielu miejscach napawają optymizmem na przyszłość.
Dziękujemy serdecznie za podzielenie się treścią książki, ale przede wszystkim:
dziękujemy za danie głosu osobom niebinarnym!❤️

<a href="https://www.empik.com/pomiedzy-historie-osob-niebinarnych-kim-renata,p1468843855,ksiazka-p" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-5">
    <span class="fal fa-shopping-basket"></span>
    Zamów na Empik.pl
</a>

