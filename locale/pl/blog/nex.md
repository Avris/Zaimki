# Nex Benedict. Say their name

<small>2024-02-22 | [@Tess](/@Tess)</small>

![](/img/pl/blog/nex-benedict.png)

Niebinarne Nex Benedict zmarło, mając zaledwie 16 lat. Wcześniej zostało napadnięte przez trzy uczennice z tej samej szkoły. Nex kochało koty, lubiło rysować, grało w „Minecrafta” oraz „ARK: Survival Evolved”.

_[ UWAGA: Tekst mówi o queerfobicznej przemocy. ]_

Nex Benedict ([they/them](https://zaimki.pl/t%C5%82umaczenie)) było osobą uczniowską z drugiej klasy szkoły średniej w Owasso w stanie Oklahoma. Zmarło 8 lutego, dzień po tym, jak zostało napadnięte w szkolnej toalecie przez koleżanki z klasy.

Anonimowe źródło (przedstawiło się jako przyjaciółka matki Nex) poinformowało lokalny serwis »KJRH«, że Nex zostało zaatakowane przez trzy starsze uczennice, nie było w stanie samodzielnie dojść do pielęgniarki szkolnej, a mimo to osoby pracujące w szkole nawet nie wezwały karetki. Nex zostało zabrane do szpitala dopiero przez jego babcię, dzień później zmarło. Komisariat policji w Owasso zamiast o napaści, mówi o bójce, twierdzi też, że wszystkie osoby zdołały dotrzeć do pielęgniarki o własnych siłach, ale potwierdza, że ta po zbadaniu Nex oświadczyła, że wezwanie karetki nie jest konieczne.

W wiadomościach o śmierci Nex wielokrotnie było ono misgenderowane, podawano też jego deadname. „Ponieważ wielu z nas dowiaduje się o strasznych wieściach z Owasso w Oklahomie, wiele serwisów informacyjnych, a więc też wiele z nas, używa deadname’u. Jego imię to Nex Benedict. Dobrze się uczyło. Lubiło koty. (…) Zasługiwało na życie. Oby teraz odnalazło spokój” – opublikowano 19 lutego na koncie partii demokratycznej z Oklahomy.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">As many are learning of the horrific news out of Owasso, OK, many news outlets, and therefore, many of you, are using their dead name. <br><br>Their name is Nex Benedict. <br><br>They were a 4.0 student. They liked cats. They were Cherokee. They deserved to live.<br><br>May they find peace now.</p>&mdash; OK County Democrats (@OklaCountyDemos) <a href="https://twitter.com/OklaCountyDemos/status/1759647724721287669?ref_src=twsrc%5Etfw">February 19, 2024</a></blockquote>

Queerowe media i organizacje takie jak Freedom Oklahoma, Los Angeles Blade, Daily Kos czy LGBTQ Nation) zwróciły uwagę, że czas napaści na Nex Benedict nie jest przypadkowy.

W Oklahomie w maju 2022 roku weszła w życie ustawa wymuszająca na osobach uczniowskich ze szkół publicznych korzystanie z toalet zgodnych z płcią przypisaną przy narodzeniu (z AGAB-em). Babcia Nex, Sue Benedict, powiedziała w rozmowie z „The Independent”, że prześladowania wobec Nex zaczęły się na początku roku szkolnego 2023.

Od 2023 roku funkcję kuratora ds. nauczania publicznego w w Oklahomie pełni Ryan Walters, wcześniej sekretarz edukacji Oklahomy. Jest on członkiem partii republikańskiej, przeciwnikiem praw osób LGBT+, sprzeciwia się badaniom o tym, że rasizm ma charakter systemowy, współpracuje też z konserwatywną organizacją Moms for Liberty, określaną przez Southern Poverty Law Center jako ekstremistyczna i antyrządowa.

W styczniu Ryan Walters wprowadził stan nadzwyczajny, aby uniemożliwić osobom uczniowskim posługiwanie się prawdziwą płcią w dokumentach szkolnych oraz mianował queerfobiczną Chayę Raichik do rady doradczej Departamentu Edukacji Stanu Oklahoma kontrolującej stanowe biblioteki szkolne, mimo że Raichik nawet nie mieszka w Oklahomie.

Wspomnienie o Nex Benedict opublikowała organizacja Freedom Oklahoma.

<div class="fb-post" data-href="https://www.facebook.com/FreedomOklahoma.org/posts/pfbid02at7G1YfWoRCi6QZXgnHYkDHwLuwx7HRRUoy4M9Kf5XbPV4rE135Wvzewh66rMd74l" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/FreedomOklahoma.org/posts/715760807356606" class="fb-xfbml-parse-ignore"><p>Last week we were made aware of a possible hate-motivated attack in the Owasso school district, leading to the death of...</p>Posted by <a href="https://www.facebook.com/FreedomOklahoma.org">Freedom Oklahoma</a> on&nbsp;<a href="https://www.facebook.com/FreedomOklahoma.org/posts/715760807356606">Monday, February 19, 2024</a></blockquote></div>

„W zeszłym tygodniu otrzymaliśmy_łyśmy informację o możliwym ataku na tle nienawiści, który doprowadził do ataku na osobę uczniowską. Chociaż informacje, które udało nam się zebrać, wciąż pozostają niepełne, już wiemy, że Nex Benedict, zmarła osoba uczniowska, doświadcza misgenderowania oraz deadname’ingu po straszliwym ataku, który zabił Nex, najprawdopodobniej z powodu jego tożsamości TGNC+ [„Transgender and Gender Nonconforming People”, czyli osoby transpłciowe oraz kwestionujące ideę płci – przyp. Tess].

Kontynuując składanie fragmentów w pełną historię, chcemy dotrzeć do naszej społeczności zmagającej się z tą straszliwą krzywdą oraz żałobą, które wszyscy_tkie podzielamy, gdy rozmyślamy nad narastającymi nastrojami anty-2SLGBTQ+ [2S to skrót od „two-spirit”, czyli dosłownie „o dwóch duszach”, termin wykorzystywany do opisania osób queerowych wśród rdzennych osób zamieszkujących Amerykę Północną – przyp. Tess], z którymi najmłodsze osoby należące do naszej społeczności spotykają się coraz częściej, podsycanymi przez prawo stanowe, związaną z nim retorykę, słowa i zachowania osób urzędujących w naszym stanie oraz rosnącą platformę, którą ci u władzy dają ludziom takim jak Chaya Raichik, która wciąż wykorzystuje swoje zasięgi tak, by grozić realną krzywdą dzieciom z Oklahomy.

Zdajemy sobie sprawę, że Nex zasługuje zarówno na uczczenie jego pamięci, jak i na zbadanie przyczyn jego śmierci, oraz z tego, że żyjemy w czasach, w których niekompletne fragmenty tej historii są wykorzystywane do wywoływania ruchu na stronach oraz clickbaitów. Zawsze staramy się zachować ostrożność w kwestii równowagi między opowiadaniem historii a potencjalnym podsycaniem normy, w której o tragedii pisze się dla klików, bez szacunku dla żałoby osób dotkniętych tą sytuacją czy osób, które same doznały krzywdy.

Oto kilka wspomnień o Nex od osób, które zwróciły się do nas z troską, przerażeniem i żalem, ale też w nadziei, że Nex zostanie zapamiętane nie tylko z powodu tragicznej śmierci, ale także z powodu dążenia do autentycznego życia: Nex kochało muzykę rockową oraz chętnie nawiązywało kontakty z innymi osobami wymachującymi głowami. Nex było niezawodnie życzliwe i zawsze szukało w ludziach tego, co najlepsze. Nex należało do Czoktawów (Choctaw)”.

„Nex Benedict, 16-letnia osoba uczniowska z liceum, kochało przyrodę oraz oglądać serial »The Walking Dead«. Nex lubiło rysować, czytać oraz grać w »Ark« i »Minecrafta«. Nex miało kota Zeusa, którego bardzo kochało. Korzenie rodziny Nex częściowo wywodziła się z Czoktawów; byli oni w drodze do lepszego zrozumienia tożsamości Nex – podobnie jak wielu innych rodziców młodzieży transpłciowej i niebinarnej”, wspomina Jose Soto z „Human Rights Campaign”.

<div class="fb-post" data-href="https://www.facebook.com/humanrightscampaign/posts/pfbid02oBgqFn6vmfYrNjRjDcjtim4UCSahTuxFoeViT6h1pWxhFNzQrWqzab2188b5jSMTl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/humanrightscampaign/posts/940610857433995" class="fb-xfbml-parse-ignore"><p>Nex Benedict was a non-binary high school student who enjoyed nature and watching The Walking Dead. They loved their cat...</p>Posted by <a href="https://www.facebook.com/humanrightscampaign">Human Rights Campaign</a> on&nbsp;<a href="https://www.facebook.com/humanrightscampaign/posts/940610857433995">Wednesday, February 21, 2024</a></blockquote></div>

Nex Benedict miało 16 lat. Kochało „Minecrafta” i „ARK” oraz było prześladowane za niebinarność”, pisze Abby Monteil z „Them”.

<div class="fb-post" data-href="https://www.facebook.com/them/posts/pfbid02tzGo4QCL1JtmmgLT1ze7NFqmbjh324u3czgZshJ8gn52Ezivu1bdasChpuDATizFl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/them/posts/949706753181619" class="fb-xfbml-parse-ignore"><p>16-year old Nex Benedict died after being attacked in a school bathroom. The nonbinary teen loved &#039;Minecraft&#039; and were a member of Cherokee nation.</p>Posted by <a href="https://www.facebook.com/them">Them</a> on&nbsp;<a href="https://www.facebook.com/them/posts/949706753181619">Wednesday, February 21, 2024</a></blockquote></div> 


