# Niebinarne rodzicielstwo, część II. Niebinarny rodzic w języku

<small>2023-08-27 | [@Tess](/@Tess)</small>

**Odkąd napisałom na swoim blogu, że jestem w ciąży, zaczęłom otrzymywać pytania: „to kim właściwie będziesz?”. 
Ich forma i kontekst nie pozostawiały wątpliwości – między wierszami był tam przekaz: póki nie masz dziecka,
możesz się „wygłupiać” z niebinarnością, ale jeśli jesteś w ciąży, to jesteś kobietą, będziesz matką, kropka.
Jak łatwo się domyślić, absolutnie się z tym nie zgadzam.**

O niebinarnym rodzicielstwie mówi się niewiele – ponieważ niebinarność wciąż istnieje na marginesie marginesów społeczeństwa.
W queerfobicznej Polsce, w której nawet binarne osoby transpłciowe mają problem z dostępem do prawnego oznaczenia płci,
rozwiązania prawne specyficzne dla osób niebinarnych po prostu nie istnieją. Dopiero zaczynamy istnieć w języku –
to na tej części aktywizmu skupiamy się, tworząc projekt zaimki.pl i działając jako Rada Języka Neutralnego.
Nasza reprezentacja w popkulturze jest znikoma – więc skoro mało kto wie, że istniejemy, tym bardziej mało kto wie, że możemy zostać rodzicami.

![Zdjęcie przedstawia Tess, osobę autorską linkowanego tekstu. Na zdjęciu widoczna jest uśmiechnięta osoba prowadząca wózek dziecięcy. Wózek jest szary, zajmuje dół kadru. W centrum znajduje się osoba: ubrana w szarą czapkę z kocimi uszkami, oczami i uśmiechniętą mordką, w czarną ramoneskę oraz czarno-czerwoną bluzę z napisem „Witcher”. Na ramieniu ma szarą torbę. W tle widoczne jest drzewo i zielona trawa.](/img/pl/blog/tess-wózek.jpg)

Wpisuję w wyszukiwarkę „niebinarny rodzic” i pierwszym wynikiem… jest mój tekst.
To „Niebinarne rodzicielstwo, część I. Niebinarna ciąża”. Google podpowiada też związane z tematyką pytania.
Z czterech propozycji, dwie w ogóle nie dotyczą rodzicielstwa („Co to znaczy być niebinarny?”, „Jak się mówi na osoby niebinarne?”),
trzecia – „Czy osoba niebinarna może mieć dzieci?” – znów odsyła do mojego artykułu,
w końcu czwarta – „Co to jest niebinarny rodzic?” – odsyła poza zaimki.pl.

To artykuł Wiktorii Beczek z Gazeta.pl zatytułowany: [„Kim jest ‘dziabko’? To rodzic naszego rodzica, ale jest osobą niebinarną”](https://wiadomosci.gazeta.pl/wiadomosci/7,114883,29357242,kim-jest-dziabko-to-rodzic-naszego-rodzica-ale-jest-osoba.html)
„Słownik Empatyczny Języka Polskiego, Rada Języka Neutralnego i Słownik Neutratywów Języka Polskiego zaproponowały
w ubiegłym roku słowo ‘dziabko’. Dziabko, czyli niebinarny odpowiednik babci i dziadka, czy też niebinarna osoba,
będąca rodzicem naszego rodzica. Eksperci zaproponowali też obchodzenie Dnia Dziabka po Dniu Babci i Dniu Dziadka –
23 stycznia” – wyjaśnia autorka. Czyli znów wracamy do nas…

Potem natrafiłom jednak na dwa ważne teksty.

„Co to znaczy być rodzicem demigender? Powiem wam, dlaczego jest to dla dziecka najlepsze”, [pisze Kacper Peresada z dadHERO.pl](https://dadhero.pl/287735,samotni-niebinarni-rodzice-jak-wychowywac-dziecko-gdy-jestes-demigender).
„W jednej z grup ojcowskich samotny tata postanowił napisać post o tym, że od momentu rozstania z matką dzieci określa się mianem osoby demigender.
To stwierdzenie nie powinno nikogo szokować, w końcu rola samotnego rodzica jest w dużym stopniu próbą połączenia w sobie dwóch płci.
Jednak, jak zwykle, wywołała ono sporo negatywnych emocji”. Tekst nie zawiera wypowiedzi wspomnianego samodzielnego rodzica,
nie wiemy więc, czy rodzicielstwo zaowocowało u niego coming outem, czy po prostu w ten sposób określa, że musi swojemu dziecku zastępować matkę.
W tekście padają jednak ważne słowa: „Nie jesteś matką ani ojcem. Jesteś sobą”. Choć nie jest jednoznaczne, czy tekst Kacpra Peresady
można uznać za reprezentację niebinarnych rodziców, to bez wątpienia pokazuje on, że w ogóle istniejemy.
Oraz że stereotypy płciowe w rodzicielstwie mogą być wręcz szkodliwe.

„Leszek jest niebinarnym tatą. „Nasz syn nie ma uprzedzeń i narzuconych ram’” to artykuł, 
który [ukazał się w ramach cyklu „Niebinarnx” na Noizz.pl](https://noizz.pl/lgbt/leszek-jest-niebinarnym-tata-nasz-syn-nie-ma-uprzedzen-i-narzuconych-ram/8b7ylrz).
Autorem materiału jest Jakub Wojtaszczyk.

To dla mnie niezwykle ważny tekst, bo chyba jedyny opowiadający o konkretnej osobie niebinarnej doświadczającej rodzicielstwa – 
jedyny, oprócz mojego początku cyklu. I pojawia się w nim zagadnienie, które towarzyszyło też mnie – kim właściwie jestem?
Kim jestem na płaszczyźnie językowej?

> **JAKUB WOJTASZCZYK: Cztery lata temu, czyli mniej więcej, gdy zaczynałeś rozumieć swoją niebinarność, pojawił się twój syn Julek. Definiujesz się jako tata czy rodzic?**
> 
> LESZEK: Tak i tak, ale nie mam problemu ze słowem ‘tata’. Być może, gdyby moja podróż z niebinarnością rozpoczęła się wcześniej, byłoby inaczej. Jednak od początku życia Julka jestem jego tatą i to mi odpowiada. Myślę, że pomaga też fakt, że czuję jego bezgraniczną i czystą miłość. Bycie ukochanym tatusiem sprawia, że nie mam żadnych negatywnych konotacji z tym tytułem.
> 
> **Jak to jest bycie tatą w trakcie tożsamościowej eksploracji?**
> 
> Chciałbym powiedzieć, że jest to skomplikowane, ale nie jest (śmiech). Z Natalią jesteśmy przed Julkiem otwarci. Na wszystkie jego pytania odpowiadamy szczegółowo i naukowo, oczywiście na poziomie dziecka. Staramy się do prozy życia wplatać tematy o niebinarności i traspłciowości. Na przykład, kiedy dowiedział się, że w brzuchu kobiety może rosnąć dzidziuś, powiedzieliśmy mu, że może też rosnąć w brzuchu nie-kobiety, a nawet tatusia. Jak się o tym dowiedział, przez jakiś czas mówił, że też urodzi dzidziusia. Ze swobodą opowiadamy mu, że dziecko może mieć mamę i tatę, dwie mamy, dwóch tatusiów, kilku rodziców, albo jednego. Może być wychowywane przez osoby, które nie są jego rodzicami.
> Początkowo stawialiśmy na naturalne barwy z ubiorem i zabawkami, tak by nic nie narzucać. A odkąd był na tyle duży, żeby móc wybierać samemu, pozwalamy mu decydować. Teraz patrzy na to, co ja noszę, więc uwielbia kolor różowy. A od mamy przejął malowanie paznokci — najchętniej każdy na inny kolor. Staram się być rodzicem, którego ja zawsze potrzebowałem. W jakimś stopniu działa to na mnie terapeutycznie — dbanie o Julka jako dbanie o małego Leszka. Czuję się bardzo dobrze odnajdując się w tych stereotypowo niemęskich aspektach rodzicielstwa.

Da się też znaleźć – ale to wymaga już przeszukiwania social mediów – [historię Kaśka z grupy „My, rodzice”](https://www.facebook.com/myrodziceosobLGBTQIA/posts/pfbid02vSNcWqH3SNgE3k1KLvsVXQXHzdNFSc5PRhEWug4L4maNB3o6DjuqHoPZbfTwvENjl).

> Pojęcie niebinarności pierwszy raz, w moim życiu pojawiło się w dniu coming outu mojego dziecka.
> Zresztą, transpłciowość też była czymś nowym. Chwila, w której moje zestresowane tym, jak mogę zareagować
> dziecko tłumaczyło mi te pojęcia była momentem szoku, radości i strachu. (…) Dzień wyjścia z szafy mojego dziecka,
> przede mną, jest dniem kiedy wszystko w moim życiu nabrało sensu, kiedy zrozumiałam, kim jestem i co się ze mną działo przez całe moje życie.
>
> Jestem Kasiek, jestem niebinarnym rodzicem niebinarnego dziecka. Dziecka, które jest moim przewodnikiem w tęczowym świecie, nauczycielem i wsparciem. (…)
>
> Dziś wiem, że wolno mi być tym kim czuję, że jestem – obecnie najbezpieczniej mi pod parasolem niebinarności,
> a w zasadzie przy określeniu genderqueer. Nie wiem co jest moje, a co zostało mi wpojone, wmówione.
> Odczuwam dysforię, marzę by kiedyś rozpocząć terapie hormonalną.
>
> Myślę, że moje doświadczenia z młodych lat pomagają mi troszkę współodczuwać z Edenem, rozumieć bardziej jego, ale i siebie, swoje decyzje sprzed lat.
> Nasza relacja niewiele się zmieniła po wspólnym coming oucie, zawsze była przyjacielska.
>
> Eden: ‘Wszyscy mówią, że zazdroszczą mi rodzica, który – tak jak ja – jest niebinarny’.

Zaczęłom obszerną dygresją o tym, jak wygląda niebinarne rodzicielstwo w Polsce (a raczej jego reprezentacja w sieci),
by nakreślić poczucie osamotnienia, jakie mi towarzyszy. Tęczowe rodzicielstwo to temat poruszany dość rzadko,
ale jeśli już, to najłatwiej w nim o reprezentację lesbijek. Osoby niebinarne są wtłaczane w binarne role kobiety i mężczyzny,
mamy i taty. W powszechnej świadomości nie istnieją ani w rzeczywistości, ani w języku.

Jest to na swój sposób fascynujące, ponieważ… trudno o równie queerowe słowo co „rodzic”!
Co oznacza to słowo? „Ten, który rodzi”! A jednocześnie jest ono niezwykle popularne:
mówimy o rodzicielstwie, rodzicielstwie zastępczym, w szkole odbywają się zebrania z rodzicami,
w wielu formularzach niezbędny jest podpis rodzica lub opiekuna. To forma rodzaju męskiego
i choć to nie tak, że nie istnieje feminatyw – najstarszy zachowany wraz z melodią tekst poetycki,
średniowieczna pieśń „Bogurodzica”, pokazuje nam, że był w użyciu, ale obecnie nie rozróżniamy przecież językowo
„rodzica” i „rodzicy”. Z racji tego, że ze sferą rodzinną i domową stereotypowo kojarzymy kobiety,
to i „rodzic” często utożsamiany jest z „matką”. Czyż to pomieszanie rodzajów i znaczeń nie jest cudownie queerowe?

Choć nie używam maskulatywów, mówiąc o sobie, to właśnie dla „rodzica” robię wyjątek.
To dla mnie podstawowe słowo oznaczające, że mam dziecko. Jestem rodzicem.

A jak z innymi określeniami? Skoro są maskulatyw (rodzic) i feminatyw (rodzica), możemy pokusić się o utworzenie dalszych form.
[Neutratyw](/neutratywy) – rodzicze, [dukatyw](/dukatywy) – rodzicu,
[iksatyw](/iksatywy) – rodzicx, [osobatyw](/osobatywy) – osoba rodzicielska. Dla każdego (niebinarnego) coś miłego!

A co, jeśli brakuje nam synonimów? Wszak każda matka i każdy ojciec może być rodzicem.

Osobiście na drugim miejscu stawiam „mamuś” – to mamuś – i taką zabawę proponuję wszystkim osobom potrzebującym.
Męskie lub neutralne określenia pochodzące od „matki”, żeńskie określenia pochodzące od „ojca”.
To mamuś, ten mamuś, ta ojca czy nawet ta tata – wszak to słowo odmienia się według żeńskiego wzorca!

 - „Moja tata jest bardzo mądra”.
 - „Jego mamuś odebrał go ze szkoły”.
 - „Mamuś Nikity zabrało nas na wycieczkę”.

Są wreszcie formy binarne. W [tekście o tym, że zdarza mi się automisgendering](https://zaimki.pl/blog/samoz%C5%82op%C5%82cenie),
czyli misgenderowanie samego siebie, wspominałom, że używam słowa „mama”. Dlaczego? To akurat dość łatwe.
Moje dziecko dopiero uczy się mówić, używa prostych zbitek sylabowych: tata, mama, baba, dziadzia, dzidzi, brum-brum (autko),
mniam-mniam (jedzenie), łoł-łoł (pies), ci-ci (zabawka) ciuch-ciuch (kolejka). Trudno byłoby oczekiwać,
że wypowie skomplikowane dla siebie „rodzic” czy choćby „mamuś”. Trudno też uważać, że niemowlę wypowiadające
proste sylaby misgenderuje kogokolwiek – tak samo jak „dziadzia” jest uproszczeniem słowa „dziadek”,
a nie sugerowaniem, że dziadek używa żeńskich form. Przy dziecku mówię o sobie „mama”, żeby jego świat był łatwiejszy,
konsekwentny i spójny. Na tłumaczenie mu większych zawiłości świata przyjdzie jeszcze czas. 
Świadomie podejmuję decyzję o tym, że czasem bywam mamą, żeby moje dziecko w Dzień Matki nie czuło się gorsze. 
Jeśli będzie chciało mi złożyć życzenia w Dzień Kobiet – przyjmę je, choć nie czuję, by było to moje święto.  
Nie zmienia to faktu, że moje święta przypadają 18 kwietnia – to Dzień Niebinarnego Rodzica – oraz 9 marca, 
kiedy obchodzimy Polski Dzień Osób Niebinarnych.

<a href="/kalendarz/2023-03-09"><img src="/calendar/2023-03-09.png" alt="Kalendarz na marzec z zaznaczonymi queerowymi świętami i wylistowanymi tymi z 9-go marca"></a>

Jestem rodzicem, jestem osobą rodzicielską, jestem mamuś. Jestem sobą. 

- [Część I: Niebinarna ciąża](/blog/niebinarne-rodzicielstwo-ciąża)
- [Część III: Imię dla dziecka](/blog/niebinarne-rodzicielstwo-imie)
