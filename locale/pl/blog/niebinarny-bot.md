# Niebinarny bot to nie jest inkluzywność!

<small>2024-10-22 | [@Tess](/@Tess)</small>

**OFF Radio Kraków wprawdzie zwalnia realnych ludzi, ale udaje, że generatywna AI jest osobą \[sic!] queerową, która działa na rzecz praw człowieka i równości.**

![Screenshot profilu bota, z nałożonym tekstem "To jakiś żart?"](/img/pl/blog/niebinarny-bot.png)

Absurdalną i skandaliczną sytuację nagłośnił Mateusz Demski, dziennikarz i krytyk filmowy, pracujący w mediach od niemal dekady. Jego teksty były publikowane m.in. na łamach „Przekroju”, „Przeglądu”, „Gazety Wyborczej”, „Newsweeka”, „Tygodnika Powszechnego”, „Polityki”, miesięcznika „KINO”, czasopisma „Ekrany”, „Dziennika Zachodniego”, a także w portalach Dwutygodnik, Onet, Interia, NOIZZ, newonce i Mint Magazine. Do września 2024 roku prowadził audycję w OFF Radiu Kraków.

Dziś na Facebooku wrzucił posta uchylającego kulisy zwolnień we wspomnianym radiu.

– Niedawno pisałem o tym, że OFF Radio Kraków (w którym prowadziłem audycję przez blisko trzy lata) przestaje nadawać w znanej dotąd formule, że wszyscy, którzy dotąd tworzyli to miejsce wylecieli z pracy. Wczoraj poznaliśmy szczegóły – kilkanaście osób z redakcji (mowa o doświadczonych dziennikarkach i dziennikarzach, artystkach i artystach, osobach twórczych) zastąpi sztuczna inteligencja. Od 22 października na antenie usłyszymy głosy trojga nowych prowadzących. 20-letniej Emilii „Emi” Nowa, studentki dziennikarstwa, ekspertki od popkultury; 22-letniego Jakuba „Kuby” Zielińskiego, specjalisty od technologii i muzyki oraz 23-letniego, zaangażowanego społecznie Alexa – poruszającego tematy związane z tożsamością i kulturą queer. Żadna z tych osób nie istnieje. Wszystkie zostały wygenerowane przez AI – pisze Mateusz Demski.

<div class="text-center my-3"><div class="fb-post" data-href="https://www.facebook.com/matdemski/posts/pfbid0DS1rRgH6PLUBkpmv8rTMGbNZvhtb5bzvs4yiuXAY84tz2ULMDhKG6Qnh7q1vXzkAl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/matdemski/posts/8467844423296937" class="fb-xfbml-parse-ignore"><p>Kochani, zwracam się do Was z ważną dla mnie (i wierzę, że w pewnym sensie dla Was wszystkich) sprawą. Niedawno pisałem...</p>Posted by <a href="https://www.facebook.com/matdemski">Mateusz Demski</a> on&nbsp;<a href="https://www.facebook.com/matdemski/posts/8467844423296937">Tuesday, October 22, 2024</a></blockquote></div></div>

Zastąpienie realnych osób botami to pomysł Marcina Pulita powołanego na likwidatora Radia Kraków, który obecnie określa się jako redaktor naczelny stacji OFF Radio Kraków. Twierdzi, że to „eksperyment badawczo-medialny” oraz zapewnia, że żadna ze wspomnianych przez Mateusza Demskiego osób pracy nie straciła.

Twory wygenerowane przez sztuczną inteligencję nazywa „OFF-ludźmi”, publikuje ich fikcyjne biogramy, zdjęcia (oczywiście wyprodukowane przez AI) oraz zajawki audycji.

O samej sytuacji piszę więcej na portalu [Niestatystyczny.pl](https://niestatystyczny.pl/2024/10/22/kilkanascie-osob-zwolnionych-zamiast-nich-audycje-radiowe-poprowadza-boty-udajace-ze-reprezentuja-pokolenie-z), więc osoby zainteresowane rozwinięciem tematu zachęcam do lektury tamtego tekstu. Tutaj chcę skupić się na perspektywie queerowej.

Otóż Marcin Pulit postanowił zabawić się w inkluzywność na pokaz i uznał, że jeden z botów będzie… osobą niebinarną.

„Alex to osoba pełna zaangażowania społecznego, która z pasją porusza tematy związane z tożsamością, queer kulturą i wpływem mediów na społeczeństwo. Jako aktywist_ka na rzecz praw człowieka i równości, Alex prowadzi podcast oraz regularnie uczestniczy w wydarzeniach artystycznych. W swoich audycjach analizuje współczesną kulturę, często zapraszając do studia uznanych ekspertów” – czytamy w dziale „OFF-ludzie”.

Osoby queerowe od lat walczą o reprezentację – zarówno w mediach, jak i w tekstach kultury. Na co dzień spotykamy się z hejtem, wykluczeniem i przemocą. W 2024 roku w Polsce wciąż nie ma nawet najbardziej okrojonej możliwości zarejestrowania związku partnerskiego, o równości małżeńskiej nie mówiąc – [i choć powstała ustawa, to środowiska konserwatywne mają nadzieję, że nie uda się jej uchwalić.](https://zaimki.pl/blog/zwiazki-partnerskie-2)

Dyskryminacja ma też przełożenie na nasze funkcjonowanie w miejscu pracy: Kampania Przeciw Homofobii odnotowuje, że 70 procent osób transpłciowych ukrywa swoją tożsamość przed przełożonymi i współpracującymi z nimi osobami, a w gronie osób wyoutowanych – aż 36 procent padło ofiarami dyskryminacji. [Z tą formą wykluczenia próbuje walczyć Federacja Znaki Równości organizująca Beyond Labels, czyli Inkluzywne Targi Pracy.](https://zaimki.pl/blog/beyond-labels-relacja) To fantastyczna inicjatywa, ale pojawiło się na niej zaledwie kilkanaście firm. Zaledwie kilkanaście miejsc w Polsce pokazało, że chętnie zatrudni wyoutowane osoby queerowe.

W tym samym miesiącu OFF Radio Kraków postanowiło „zatrudnić” fikcyjną osobę niebinarną, która nie ma czynszu do opłacenia, nie musi kupować jedzenia ani spać. OFF Radio Kraków przedstawia wygenerowanego bota jako osobę walczącą o równość. Równość w bezrobociu? Równość w braku realnej reprezentacji? Równość w odbieraniu nam głosu?

Nie czarujmy się, niewiele osób queerowych może pozwolić sobie na przywilej zawodowego aktywizmu. Zazwyczaj zajmujemy się nim kosztem pracy, wypoczynku, bliskich. Często przypłacamy aktywizm wypaleniem, czasem przemocą. Wykluczenie ma przełożenie na nasz dobrostan – chorujemy na depresję, trafiamy do szpitali psychiatrycznych. A jednak robimy to, bo wiemy, że nasza walka jest potrzebna.

Nie czarujmy się – potrzebujemy pracy jak wszyscy inni, ale założę się, że znalazłyby się osoby aktywistyczne gotowe prowadzić audycję za darmo, byle tylko wykorzystać okazję do edukowania społeczeństwa. Tymczasem OFF Radio Kraków śmieje się nam w twarz, nazywając bota aktywist_ką na rzecz praw człowieka, który_a z pasją porusza tematy związane z tożsamością.

Tożsamością, której nie ma, bo – kurwa! – nie istnieje.

Czyż to nie symboliczne? W czasach nagonki na społeczność queerową zwolnić realne osoby, żeby wygenerowany program kradnący treści stworzone przez inne osoby dziennikarskie (przecież Alex niczego nie wymyśli samodzielnie, może tylko powielać rzeczy wyczytane w sieci), żeby udawać, że przecież nikogo się nie wyklucza. Parytet idealny: mężczyzna, kobieta, osoba niebinarna. Koszt: zero złotych. Zakwaterowanie: dysk twardy „redaktora” „naczelnego”.

Ta żenująca parodia inkluzywności nas po prostu obraża. Marcin Pulit mógł nazwać swoje wytwory na przykład: Bot21, Bot37 oraz Bot69 – nadal zastępowanie nimi ludzi byłoby obraźliwe i szkodliwe, ale przynajmniej nie byłoby szyderą z wykluczenia.

[Mateusz Demski przygotował list otwarty kierowany m.in. do Krajowej Rady Radiofonii i Telewizji oraz Rady Etyki Mediów. Zachęcamy do podpisania jego apelu.](https://www.petycjeonline.com/apel_rodowiska_kultury_i_mediow_w_sprawie_sytuacji_w_off_radiu_krakow_i_zastpienia_pracownikow_sztuczn_inteligencj)

Anna Tess Gołębiowska (ono/jego)

– realna osoba, która pisze ten tekst kosztem odpoczynku po pracy i opiece nad chorym dzieckiem, ponieważ nie można tej sytuacji tak zostawić
