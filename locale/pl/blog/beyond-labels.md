# Zapraszamy na Beyond Labels, Inkluzywne Targi Pracy w Krakowie!

<small>2024-10-10 | [@Tess](/@Tess)</small>

![](/img/pl/blog/beyond-labels.png)

**Już w ten weekend w Krakowie odbędzie się trzecia edycja Beyond Labels, pierwszych w Polsce Inkluzywnych Targów Pracy. Z radością oznajmiamy, że osoba reprezentująca nasz kolektyw weźmie w ich udział.**

[Anna Tess Gołębiowska (ono/jego)]( https://zaimki.pl/@tess), czyli osoba reprezentująca Radę Języka Polskiego, czyli kolektyw prowadzący Zaimki.pl, weźmie udział w dwóch punktach programu; oba będą miały miejsce w sobotę, 12 października 2024 roku.

 - Rozmowy o samorzecznictwie, czyli panel dyskusyjny z osobami przedstawicielskimi organizacji Trans-Fuzja, Zaimki.pl, Wiara i Tęcza i edukatorem Mikołajem Jabłońskim z podcastu Beza Wstydu
 - Targi Queerowych i Sojuszniczych NGO-sów

Poniżej prezentujemy dokładne opisy tych dwóch punktów programu, a także informację prasową na temat targów oraz cały program Beyond Labels. [Opisy wszystkich wydarzeń znajdziecie na stronie oficjalnej wydarzenia.](https://beyondlabels.pl/targi-pracy/)

## 12.10.2024, 11:45 – 12:45

**STREFA ROZMÓW: Rozmowy o samorzecznictwie, czyli panel dyskusyjny z osobami przedstawicielskimi organizacji Trans-Fuzja, Zaimki.pl, Wiara i Tęcza i edukatorem Mikołajem Jabłońskim z podcastu Beza Wstydu.**

Queerowe wyzwolenie nie ma miejsca bez samrorzecznictwa – bez osób ze społeczności LGBTQIAP+ mówiących o postulatach i potrzebach społeczności, bez osób aktywistycznych gotowych stawiać czoła systemowi wykluczeń i nierównych szans, ale też bez nas samych, mówiących na co dzień o swoich doświadczeniach. Samorzecznictwo może mieć wiele oblicz i środków przekazu: to przekazywanie sobie nawzajem informacji, edukacja, udział w procesach legislacyjnych i chodzenie na marsze. To wlepy, ziny i codzienne bycie sobą. Co jeszcze? O tym opowiedzą nam:

### Grzegorz Żak (on/jego) z Fundacji Trans-Fuzja

Osoba prezesująca Zarządu Fundacji Trans-Fuzja. Ogarniacz społeczności krakowskiej. Administrator Grupy Wsparcia dla osób Transpłciowych na Facebooku, autor FanPage Angry Trans gdzie edukuje o kwestiach transpłciowych i opisuje swoją drogę tranzycji, dawniej współtwórca filmików na kanale YouTube Transbros. W wolnym czasie cosplayer, artysta i gracz.

### Anna Tess Gołębiowska (ono/jego)

Osoba członkowska kolektywu „Rada Języka Neutralnego” prowadzącego zaimki.pl. Aktywiszcze, feminiszcze, dziennikarze, lewacze, małżonie, mamuś, queer. Zawodowo pisze, hobbystycznie czyta, ogląda i czasami gra. W ramach kolektywu „Września jest kobietą” walczy o prawa reprodukcyjne na ulicach małego miasta.

### Mikołaj Jabłoński (on/jego)

W internecie znany jako @bezawstydu, autor podcastu o tej samej nazwie w którym opowiada o wstydzie i bezwstydności. Od 4 lat jako samorzecznik szkoli i edukuje na tenat osób z niepełnosprawnością wzroku a także osób LGBT+. Edukator seksualny wyróżniony na liście Sexed.pl i Forbes women 100 osób wspierających edukację seksualną. Absolwent Filologii włoskiej na Uniwersytecie Kardynała Stefana Wyszyńskiego w Warszawie oraz studiów podyplomowych z Seksuologii Praktycznej na Uniwrrsytecie SWPS w Poznaniu. Obecnie student Psychologii.

### Artur Kapturkiewicz (on/jego)

Urodzony w roku 1961, GT z akronimu LGBT+ , lekarz, specjalista pediatrii i chorób zakaźnych. Współzałożyciel nieformalnej Grupy Polskich Chrześcijan LGBTQ+ Wiara i Tęcza, która powstała w roku 2010, a następnie w roku 2018 została sformalizowana jako Fundacja Wiara i Tęcza. Chrześcijanin, aktualnie bez określonego wyznania, wierzy w dobro w każdym człowieku, w nadrzędną rolę sumienia oraz w piękno wewnętrznej wolności. Wraz z Wiarą i Tęczą pracuje dla pełnej inkluzywności dla osób LGBT+ w społeczeństwie oraz w kościołach chrześcijańskich. Kocha przyrodę, piesze wędrówki i jazdę na rowerze.

### Rozmowę poprowadzi: Dawid Wojtyczka (on/jego; ona/jej)

Aktywista działający na rzecz praw osób LGBTQIAP+, specjalista ds. różnorodności, równości i inkluzji (DEI), trener biznesowy oraz edukator oddany kwestiom sprawiedliwości społecznej. Jego szerokie doświadczenie w tej dziedzinie zostało docenione Polską Nagrodą Różnorodności 2023. Współautor książki „Transformacja”, wiceprezeska Federacji Znaki Równości, współzałożyciel Krakowskiego Centrum Równości DOM EQ oraz osoba członkowska III kadencji Rady ds. Równego Traktowania przy Urzędzie Miasta w Krakowie. Współproducent i współtwórca formatu 1. w Polsce Inkluzywnych Targów Pracy – Beyond Labels.

## 12.10.2024, 10:00 – 17:00

**Targi Queerowych i Sojuszniczych NGO-sów**

12 października 2024 roku w ramach trzeciej edycji Beyond Labels odbywającej się pod hasłem Wspólność, spotkamy się w strefie NGO, gdzie pojawią się różnorodne organizacje pozarządowe, kolektywy i stowarzyszenia działające na rzecz i w ramach społeczności LGBTQIAP+. To świetna okazja do spotkań, poznania organizacji i ich działań, zgarnięcia wlep, sieciowania się, a może nawet aktywistycznego zaangażowania.
Spotkamy się między innymi z organizacjami: Niebinarność.PL, Tęcza po Burzy, TęczUJ, Fundacja Fajrant, LGBT Film Festival, Grupa Nieustającej Pomocy, Trans-Fuzja, Zaimki.pl, Asfera, Spółdzielnia Ogniwo, Tęczowa Drużyna, Queerowy Maj, My, Rodzice, Federacja Znaki Równości, DOM EQ Kraków.
[[Link do wydarzenia]](https://www.facebook.com/events/1261221175252055/)

**11 i 12 października**, pod hasłem **“Wspólność”**, odbędzie się t**rzecia edycja Beyond Labels – 1. w Polsce Inkluzywne Targów Pracy**. Spotkamy się **w Krakowie, w Klastrze Innowacji Społeczno-Gospodarczych** (ul. Zabłocie 20), podczas paneli dyskusyjnych, wykładów, warsztatów, projekcji filmowych i w strefie targowej oraz w strefie NGO.

Beyond Labels powstało jako format przede wszystkim **transinkluzywny**, ale zorientowany na potrzeby wszystkich osób LGBTQIAP+ – są to targi pracy z ofertą włączających, sprawdzonych pracodawców, oraz przestrzeń warsztatów i rozmów o postulatach, potrzebach i działaniach osób LGBTQIAP+ na rynku pracy.

Beyond Labels to przestrzeń szans na znalezienie akceptującego pracodawcy, przyjaznego środowiska pracy, włączającego zespołu. To miejsce, w którym spotykają się queerowe talenty i otwarci pracodawcy. To pretekst do dialogu, do wyrażania dumy i bycia sobą, ale także do poruszania tematów związanych z doświadczeniem marginalizowanych społeczności na polskim rynku pracy.

Na naszej scenie i w strefie warsztatów gościć będą m.in. Maja Heban, Ala Uwarowa, Piotrka Masierak, Lu Olszewski, Mikołaj Jabłoński, Grzegorz Żak, drag queen Twoja Stara, Karol Wilczyński, kolektyw Future Simple, Ewa Furgał, osoby przedstawicielskie Związku Pracowników Branży Gier, OZZ Inicjatywy Pracowniczą UJ i inne, fantastyczne osobistości.

W strefie targowej i strefie NGO dostępnych będzie ponad 30 firm i instytucji oraz organizacji pozarządowych.

W poprzednich edycjach gościłyśmy wiele fantastycznych osób ze świata mediów, kultury, sztuki, polityki i aktywizmu, w tym m.in. posłankę Annę Grodzką, dziennikarza Antona Ambroziaka, modelkę Adriannę Hyzopską, polityczkę Magdę Dropek, artystki i artystów jak Liliana Zeic i Filip Kijowski oraz osoby reprezentujące przekrój Polskich organizacji pozarządowych i firm.

Za organizację 1. w Polsce Inkluzywnych Targów Pracy Beyond Labels odpowiada Federacja Znaki Równości – pochodzące z Lrakowa zrzeszenie organizacji samorządowych działających na rzecz i w ramach społeczności LGBTQIAP+.

Beyond Labels to inicjatywa krakowskiej Federacji Znaki Równości, zrzeszającej organizacje działające na rzecz i w ramach społeczności LGBTQIAP+. To projekt, w ramach którego chcemy radykalnie zmieniać sposób w jaki osoby LGBTQIAP+ wchodzą na rynek pracy i na nim funkcjonują. Według raportu Kampanii Przeciw Homofobii, 70% transpłciowych osób ukrywa swoją tożsamość przed przełożonymi i współpracującymi z nimi osobami. Spośród wyoutowanych osób, aż 36% padło ofiarą gorszego traktowania w miejscu pracy. Tymczasem, jak podaje raport “Bezpieczne przystanie” przygotowany przez Miłość Nie Wyklucza, osoby pracujące w bardziej akceptującym środowisku pracy cieszą się lepszym zdrowiem – fizycznym i psychicznym. Spotkaj się z nami 11 i 12.10.2024 w Klastrze Innowacji Społeczno-Gospodarczych, razem stworzymy przestrzeń na czułą rewolucję rynku pracy!

## Rozkład jazdy

### 11 października 2024

10:00 – 17:00

Inkluzywne Targi Pracy

11:45 – 12:45

Corporate Snatch Game, gra dyskusyjna prowadzona przez Drag Queen, Twoją Starą

13:30 – 14:30

Nieopowiedziane Queerstorie, wykład Emanueli Lewandowskiej i Dawida Wojtyczki

14:45 – 15:45

Dobre praktyki w biznesie, wykład Oli Tomaszewskiej z AON Polska

### Strefa wytchnienia

10:45 – 12:45

Projekcja filmu Sweetheart (reż. Marley Morrison)

13:15 – 14:15

Ciałożyczliwa joga z Magdaleną Malik

14:30 – 15:20

Wieloreligijna Medytacja z Karolem Wilczyńskim

15:30 – 18:00

Ćwiczenia z towarzystwem, warsztat z Future Simple

### 12 października 2014

10:00 – 17:00

Targi Queerowych i Sojuszniczych NGO-sów

10:30 – 11:30

Alternatywne Metody Kolektywnej Współpracy, panel dyskusyjny z Prideshop.pl i Spółdzielnią Ogniwo

11:45 – 12:45

Rozmowy o samorzecznictwie, panel dyskusyjny z osobami przedstawicielskimi organizacji Trans-Fuzja, Zaimki.pl, Wiara i Tęcza i edukatorem Mikołajem Jabłońskim z podcastu Beza Wstydu

13:30 – 14:30

Nie ma równości bez różnorodności, dyskusja o tworzeniu queerowych społeczności z Lu Olszewski, Dagmarą Koryluk, Lerą Putylią i Gretą Pękałą

14:45 – 15:45

Jesteśmy w tym razem, panel dyskusyjny o związkach zawodowych ze Związkiem Pracowników Branży Gier, OZZ Inicjatywą Pracowniczą UJ i Weroniką Szwugier

16.00 – 17.00

Być sobą w pracy – Queerowe Strategie Wspólności, panel dyskusyjny z Mają Heban, Alą Urwał i Piotrką Masierak

### Strefa wytchnienia

10:45 – 11:45

Warsztat z wytchnienia

12:00 – 14:30

Nieświadomy bias – jak rozpoznać własne uprzedzenia? – warsztat z Sylvamo

14:45 – 15:45

Warsztat samoregulacji z Dziewczynami w Spektrum

16:15 – 18:15

Projekcja filmu Sweetheart (reż. Marley Morrison)
