# Łosie o skrajnie lewicowych poglądach terroryzują powiatowe miasteczko

<small>2023-06-29 | [@Tess](/@Tess)</small>

![Kolaż fragmentów maili cytowanych w artykule na fle flagi neutratywów](/img/pl/blog/łosie-zagłady-kolaż.png)

**Istnieje stereotyp, że aktywizm jest domeną wielkich miast.
Tak też pewnie myśleli Panowie Drukarze z Wrześni, miasta powiatowego liczącego 30 tysięcy osób.
Niestety, pewnego dnia zetknęli się z totalitarnymi łosiami, które – nie żartujemy! –
dysponowały środkami masowej zagłady. Jeśli wydaje Wam się, że robimy sobie żarty, zapewniamy,
że to autentyczne poglądy queerfobów – my byśmy czegoś takiego nie wymyśliły.**

Zaczęło się zupełnie niewinnie, bo od przygotowań na Marsz Równości w Poznaniu odbywający się w ramach
[Poznań Pride Week 2023](https://www.facebook.com/events/838271864140071).
Okazało się, że jedna z osób należących do Rady Języka Neutralnego, [Anna Tess Gołębiowska](/@Tess) ([ono/jego](/ono), [ona/jej](/ona)), 
najprawdopodobniej na ten marsz dotrze. Słowem wyjaśnienia – Tess jest rodzicem małego dziecka,
więc to, czy będzie mogło pozwolić sobie na wyjazd, zwykle potwierdza się dopiero na ostatnią chwilę.

Byłośmy świadome, że czasu jest mało, ale uznałośmy, że może się nam poszczęści i mimo wszystko
zdążymy zamówić niebinarny banner – jeśli będzie gotowy na Poznań, to super, a jeśli nie,
to przecież niejeden marsz przed nami! Tess wyszukało informację, która z wrzesińskich firm
wykonuje odpowiednie materiały reklamowe. Szczególnie ważna była dla nas informacja,
że firma podkreślała szanowanie czasu osób zamawiających. Mejl poszedł w świat we wtorek,
o godzinie 22:36. Zawierał opis tego, co jest nam potrzebne, w tym informację, że banner jest nam potrzebny na marsz
i musi być z jednej strony sztywny i wytrzymały, a z drugiej – na tyle lekki, żeby mogła nieść go jedna osoba.

Odpowiedź z wyceną pojawiła się w środę o godzinie 12:16. W treści mejla znalazł się opis najlepszej dla nas techniki,
oraz przedziwna wiadomość:

> Wykonanie na piątek po południu jest możliwe, jednak projekt musi być dostarczony i zaakceptowany najpóźniej do jutra do 12:00.
>
> Informacyjnie nadmienię, że drukuję przede wszystkim materiały promocyjno-reklamowe, niekoniecznie „marszowe”.
> Być może będziemy mieć podobne gusta, być może nie.

Przyjęłośmy tę informację z zaskoczeniem – dlaczego gusta Pana Drukarza miałyby mieć dla nas znaczenie?
Może pomyślał, że to on będzie projektować banner i że jego estetyka nie trafi w nasz gust?

Tess (środa, 13:28):

> Obawiam się, że nie rozumiem ostatniego akapitu. Czy mógłby mi Pan wyjaśnić, jaki znaczenie ma Pana gust
> w wykonaniu tego zlecenia? 

Pan Drukarz (środa, 14:39):

> Transparenty z założenia mają jakiś przekaz, jakąś treść. Jeżeli będzie neutralna i oczywiście zaakceptuje Pani wycenę,
> to przystąpimy do realizacji.

Zdumiewające, prawda? Transparenty mają treść! To niemalże jak materiały promocyjno-reklamowe, którymi Pan Drukarz
zajmuje się na co dzień. Postanowiłośmy jednak nie oceniać Pana Drukarza pochopnie – może obawiał się haseł
nawołujących do przemocy? Tym bardziej, że pojawiło się słowo-klucz: „neutralna”.
Jesteśmy osobami językoznawczymi, naprawdę trudno o coś bardziej neutralnego niż polszczyzna. 

Tess (środa, 15:13):

> Obawiam się, że nadal nie rozumiem, jakie ma to znaczenie, ale proszę, przesyłam obie strony grafiki.

Tutaj musimy uczciwie zaznaczyć, że mogło skołować Pana Drukarza – 
przypadkiem załączyło folder, w którym znajdowały się trzy wersje grafiki na banner zamiast wybranych dwóch.
Nie ma to wielkiego znaczenia, ponieważ i tak planowałośmy w przyszłości wykorzystać także odrzucony wariant,
po prostu wyjaśniamy, dlaczego będziemy mówić o trzech grafikach, nie o dwóch. 

Przypominamy – kluczowe do zrealizowania zlecenia na czas było dostarczenie materiałów do czwartku, do godziny 12:00.
Nadmieńmy też, że Tess wysłało pliki w czasie pracy drukarni.

Odpowiedź pojawiła się w czwartek o godzinie 11:14. Równiutko 20 godzin i 1 minutę od wysłania projektów,
cztery godziny i 1 minutę, jeśli liczyć tylko czas pracy drukarni. Jak łatwo zauważyć, do oszacowanego
przez Pana Drukarza okienka zostało 46 minut. Cóż orzekł Pan Drukarz?

> Dopiero teraz miałem możliwość otwarcia załącznika. Niestety tego się obawiałem.
> Pani nie rozumie, ale już wyjaśniam – zwyczajnie nie realizuję zleceń materiałów, które uznam za skrajnie lewicowe.

Odpowiedź Tess: 

> Z przyjemnością dowiem się, co uważa Pan za skrajnie lewicowe na załączonych grafikach :)
>
> Oraz zdaje się, że zapomniał Pan wspomnieć na swojej stronie, że Pana działalność jest dyskryminująca.
> 
> Z ciekawości: naprawdę uważa Pan, że uwierzę, że nie miał Pan czasu odpisać i zwlekał zupełnie przypadkiem tak długo, by zablokować nas z szukaniem niedyskryminującej firmy?
> Serdecznie dziękuję za szczerość, te screeny będą bardzo pouczające :)

I wtedy do akcji wkroczył Drugi Pan Drukarz, właściciel firmy, noszący zresztą to samo nazwisko co Pierwszy Pan Drukarz.
Nie wiemy, jakie relacje ich łączą, ale podejrzewamy, że to raczej bracia, kuzyni lub ojciec z synem niż para małżeńska. 

Drugi Pan Drukarz:

> Uprzejmie informuję, że w naszym kraju obowiązuje pełna wolność poglądów i przekonań.
> Pani uwagi wskazują, że nie nie szanuje Pani tych zasad, co jest przejawem totalitaryzmu.
> 
> Proszę uprzejmie zachować sobie swoje uwagi dla siebie, gdyż narusza Pani polskie prawo w tym zakresie
> (**Art. 121 kodeksu karnego**)!!!
> Ostrzegam, że wszelkie próby siania nienawiści spotkają się ze stanowczą reakcją!

Dodajmy, że informacja o kodeksie karnym została pogrubiona przez Drugiego Pana Drukarza.
Jeśli nie chce się Wam sprawdzać, jaki to artykuł, z przyjemnością Was wyręczymy.

> **Art. 121. KK**
>
> Wytwarzanie, gromadzenie i obrót środkami zakazanymi
> 
> § 1.
> Kto, wbrew zakazom prawa międzynarodowego lub przepisom ustawy, wytwarza, gromadzi, nabywa, zbywa, przechowuje,
> przewozi lub przesyła środki masowej zagłady lub środki walki bądź prowadzi badania mające na celu
> wytwarzanie lub stosowanie takich środków, podlega karze pozbawienia wolności od roku do lat 10.
> 
> § 2.
> Tej samej karze podlega, kto dopuszcza do popełnienia czynu określonego w § 1.

To jest ten moment, gdzie musiałośmy sprawdzić załączniki w skrzynce nadawczej, bo co takiego Tess tam nawysyłało,
że uznano to za **środki masowej zagłady**? Upchnęło w zipa broń biologiczną?
Nie, zdecydowanie `transparent.zip`, trzy pliki graficzne.

Okazało się, że Tess potrafi zachować kamienną twarz i zamiast wybuchnąć śmiechem
(o kurde, czy wybuch śmiechu to już środek masowej zagłady?), uprzejmie dopytało:

> Czy zaktualizował już Pan stronę internetową i poinformował, że dyskryminuje osoby o poglądach innych niż Pańskie,
> czy nadal zamierza Pan z premedytacją marnować czas osób zainteresowanych usługami drukarkami, nie opowieściami o Pana poglądach?

Drugi Pan Drukarz:

> Przepraszam uprzejmie, co to ma znaczyć?
> 
> O tym, co ma być na stronie decyduje właściciel...
> 
> Pani może decydować o swojej stronie, a ja o swojej i na tym zakończmy dyskusję.

Przypomnijmy, na stronie podkreślone było szanowanie osób zamawiających, ponieważ czas to pieniądz.

A teraz zagadka: co przedstawiały te skrajnie lewicowe grafiki, te środki masowej zagłady, które tak przeraziły
Panów Drukarzy z powiatowego miasteczka? Zróbmy chwilę pauzy, dajcie wyobraźni popłynąć, dajcie znać, co przyszło Wam do głów.

Otóż na grafikach znajdowały się łosie.

ŁOSIE MASOWEJ ZAGŁADY.

SKRAJNIE LEWICOWE ŁOSIE.

ŁOSIOLITARYZM.

![Trzy projekty transparentu, opisane w tekście postu](/img/pl/blog/łosie-zagłady.png)

Oprócz symbolu neutratywów, czyli sympatycznego łosia z łomem, na grafikach znajdowały się hasła
„Wolność, szacunek, inkluzywność”, „Ja mówiłom, ty mówiłoś”, „Osoba też człowiek” oraz „zaimki.pl”.
W tle flaga osób niebinarnych, czyli w żółto-biało-fioletowo-czarne pasy.

Zachęcamy do podziwiania, oczywiście ostrożnie, bo wiecie – nigdy nie wiadomo, kiedy taki totalitarny łoś zapragnie kogoś zgładzić.
Masowo, rzecz jasna.  

---

PS. Jeśli chcecie się dorzucić na karmę dla łosi, to możecie zerknąć [tutaj](https://ko-fi.com/radajezykaneutralnego),
a jeśli chcecie wesprzeć Tess po męczących przejściach, zajrzyjcie [tutaj](https://patronite.pl/AnnaTess).

---

{gallery={
    "/img/pl/blog/drukarz-0.png": "Pytanie w sprawie zamówienia - sztywny transparent | Anna Tess Gołębiowska-Kmieciak | do: biuro Dzień dobry, | nazywam się Anna Tess Gołębiowska, zależy mi na wydruku (najlepiej dwustronnym) w formacie A2 na materiale sztywnym i trwałym, a jednocześnie jak najlżejszym, żeby nadawał się na transparent niesiony przez jedną osobę na marszach. Ideałem byłaby sztywność mniej więcej kartonu i trwałość bannera. Myślę o druku na cienkiej płycie PCV, ale może Państwo poleciliby mi coś lepszego. Od razu mam pytanie, jak wyglądałaby wycena. | Drugie pytanie - czy byłaby szansa na wykonanie w krótkim czasie, tak żebym mogła odebrać wydruk w piątek wieczorem. | Będę wdzięczna za odpowiedź. | Pozdrawiam serdecznie | Anna Tess Gołębiowska",
    "/img/pl/blog/drukarz-1.png": "[zamazane dane] | do: mnie | Dzień dobry, | Idealnym materiałem spełniającym wymienione kryteria jest płyta z polipropylenu kanalikowego. Jest to materiał o wytrzymałości porównywalnej do PCV, a jednocześnie bardzo lekki. Grubość 5mm. Jest podatny na oddziaływanie ciepła, ale nie byłoby to problemem, bo jako transparent nie będzie wystawiony na działanie np. promieni słonecznych 24/7. | Opcje wykonania to sama płyta w cenie [zamazana wycena] netto lub płyta z dodatkowym zabezpieczeniem nadruku laminatem z filtrem UV w cenie [zamazana wycena] netto. | Do cen należy doliczyć 23% podatku VAT. | Wykonanie na piątek po południu jest możliwe, jednak projekt musi być dostarczony i zaakceptowany najpóźniej do jutra do 12:00. | Informacyjnie nadmienię, że drukuję przede wszystkim materiały promocyjno-reklamowe, niekoniecznie „marszowe”. Być może będziemy mieć podobne gusta, być może nie. | [zamazany podpis]",
    "/img/pl/blog/drukarz-2.png": "Anna Tess Gołębiowska-Kmieciak | do: biuro | Dzień dobry, | dziękuję za odpowiedź i wycenę. Obawiam się, że nie rozumiem ostatniego akapitu. Czy mógłby mi Pan wyjaśnić, jaki znaczenie ma Pana gust w wykonaniu tego zlecenia? | Pozdrawiam serdecznie | Anna Tess Gołębiowska",
    "/img/pl/blog/drukarz-3.png": "[zamazane dane] | do: mnie | Witam ponownie, | transparenty z założenia mają jakiś przekaz, jakąś treść. Jeżeli będzie neutralna i oczywiście zaakceptuje Pani wycenę, to przystąpimy do realizacji.",
    "/img/pl/blog/drukarz-4.png": "Anna Tess Gołębiowska-Kmieciak | do: biuro | Obawiam się, że nadal nie rozumiem, jakie ma to znaczenie, ale proszę, przesyłam obie strony grafiki. | Jeden załącznik * Przeskanowane przez Gmaila | transparent.zip",
    "/img/pl/blog/drukarz-5.png": "[zamazane dane] | do: mnie | Witam ponownie, | Dopiero teraz miałem możliwość otwarcia załącznika. | Niestety tego się obawiałem. Pani nie rozumie, ale już wyjaśniam – zwyczajnie nie realizuję zleceń materiałów, które uznam za skrajnie lewicowe.",
    "/img/pl/blog/drukarz-6.png": "Anna Tess Gołębiowska-Kmieciak | do: biuro | Z przyjemnością dowiem się, co uważa Pan za skrajnie lewicowe na załączonych grafikach 🙂 | Oraz zdaje się, że zapomniał Pan wspomnieć na swojej stronie, że Pana działalność jest dyskryminująca. | Z ciekawości: naprawdę uważa Pan, że uwierzę, że nie miał Pan czasu odpisać i zwlekał zupełnie przypadkiem tak długo, by zablokować nas z szukaniem niedyskryminującej firmy? | Serdecznie dziękuję za szczerość, te screeny będą bardzo pouczające 🙂",
    "/img/pl/blog/drukarz-7.png": "[zamazane dane] | do: mnie | Dzień dobry, | uprzejmie informuję, że w naszym kraju obowiązuje pełna wolność poglądów i przekonań. Pani uwagi wskazują, że nie nie szanuje Pani tych zasad, co jest przejawem totalitaryzmu. | Proszę uprzejmie zachować sobie swoje uwagi dla siebie, gdyż narusza Pani polskie prawo w tym zakresie (Art. 121 kodeksu karnego)!!! | Ostrzegam, że wszelkie próby siania nienawiści spotkają się ze stanowczą reakcją! | [zamazany podpis]",
    "/img/pl/blog/drukarz-8.png": "Anna Tess Gołębiowska-Kmieciak | do: [zamazane dane] | Czy zaktualizował już Pan stronę internetową i poinformował, że dyskryminuje osoby o poglądach innych niż Pańskie, czy nadal zamierza Pan z premedytacją marnować czas osób zainteresowanych usługami drukarkami, nie opowieściami o Pana poglądach?",
    "/img/pl/blog/drukarz-9.png": "[zamazane dane] | do: mnie | Przepraszam uprzejmie, co to ma znaczyć? | O tym, co ma być na stronie decyduje właściciel... | Pani może decydować o swojej stronie, a ja o swojej i na tym zakończmy dyskusję. | [zamazany podpis]",
}}
