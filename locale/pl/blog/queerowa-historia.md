# Anna Brzezińska wyłuskuje queerowe osoby z kart historii

<small>2024-05-28 | [@Tess](/@Tess)</small>

„Kiedyś tego nie było”, „nowomodne wymysły” – to popularne reakcje na dowolną kwestię związaną z tożsamością, nim ta trafi do mainstreamu. Pytanie brzmi, co my właściwie wiemy o tym „kiedyś”…

Od razu zaznaczę – niezwykle trudne (czy wręcz niemożliwe) jest nadawanie etykiet osobom z kart kronik. Nie wiemy przecież, co myślały o sobie. Widząc kobietę żyjącą z kobietą nie rozstrzygniemy, czy była lesbijką czy osobą biseksualną, czy też może jedna z nich tak naprawdę była osobą niebinarną czy transpłciowym mężczyzną. Ot, najbardziej klasyczny przykład Marii Konopnickiej i Marii Dulębianki. Pierwsza z Marii nazywała drugą per „Piotrek”, mówiła, że to „Pietrek z powycieranymi łokciami”. Dulębianka nosiła surduty i binokle, polowała, jeździła konno, krótko ścinała włosy. Bez poznania myśli obu Marii nie jesteśmy w stanie się dowiedzieć, Dulębianka była butch lesbijką (i czy na pewno lesbijką) czy może transpłciowym Piotrkiem.

Co jednak wiemy? Wiemy, że Konopnicka i Dulębianka przeżyły wspólnie około dwudziestu lat oraz że pochowano je we wspólnym grobie, co dość jednoznacznie pokazuje, że nie była to relacja dwóch przyjaciółek.

Anna Brzezińska, mediewistka i pisarka, w swoich książkach chętnie oddaje głos osobom wykluczanym, marginalizowanym i dyskryminowanym – czy to z powodu płci, czy pochodzenia. [Prowadzi też fanpage](https://www.facebook.com/AnnaSigridBrzezinska), na którym publikuje krótkie notki, pozwalające odkryć nam nieznane strony historii. Pozwala uchylić kotarę skrywaną przez „kiedyś”.

Dzięki niej udało nam się dowiedzieć, że w historii Polski istniała osoba imieniem Wojciech – według sądu w Kazimierzu – lub Wojciecha – według sądu w Poznaniu. Urzędy, poważne źródła.

Osoba Wojciesza funkcjonowała naprzemiennie jako kobieta i mężczyzna. A to została mnichem, a to czyjąś żoną. Osoba prowadziła niezwykle bujne życie, często sprzeczne z prawem, więc skazano ją na karę śmierci, a sentencja wyroku się nie zachowała. Pozostały skrawki.

> „Nic więcej nie wiem. Nie wiem, kim się czuł(a)(o) ani czego pragnął(ęła)(ęło) – pisze Anna Brzezińska. – Ale jeśli się Wam wydaje, że przeszłość była taka porządna – panowie na prawo, panie na lewo – to pamiętajcie o Wojciechu lub Wojciesze, bo poznański magistrat uznał za kobietę, a kazimierski sąd skazał jako mężczyznę. Płeć bywa bardzo trudną kategorią.
>
> Bardzo rzadko widzimy takie osoby z przeszłości. Zwykle pojawiają się w źródłach sądowych, więc spoglądamy na nie przez pryzmat przestępstw, które popełniały lub które popełniano przeciwko nim. To niesprawiedliwe, ale natura źródeł sprawia, że są zredukowani do tych przestępstw. Cała reszta nam umyka. Całe zwyczajne, codzienne życie i wszyscy, którzy przeżyli je z dala od sądów”.

Dziś mamy już pojęcie genderfluid – a kiedyś? Po relacji możemy domniemywać, że osobie Wojcieszej przy narodzeniu przypisano płeć żeńską, ponieważ po oględzinach położnych nakazano jej noszenie kobiecego stroju. Czy zatem mamy do czynienia z dwupłciowością lub niebinarnością, czy był to transpłciowy mężczyzna zmuszony do życia w szafie? Jednocześnie kolejne śluby pokazywały, że osoba chętnie wchodziła w rolę żony. Czy była to kwestia homo- lub biseksualności? Czy realizacja kobiecej części tożsamości? Więcej pytań niż odpowiedzi. Nigdy nie dowiemy się, kim był, była lub było.

<div class="text-center mb-3"><div class="fb-post" data-href="https://www.facebook.com/AnnaSigridBrzezinska/posts/pfbid0PUKBxobRnAho4uBivMfctonuAjSJn1UYwXYsey5N9b5sz6K7P86mEKzRhCb61amrl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/AnnaSigridBrzezinska/posts/913945500735840" class="fb-xfbml-parse-ignore"><p>W 1561 r. przed sąd w Kazimierzu rozpatrywał sprawę Wojciecha – lub Wojciechy, choć tu uznano go za mężczyznę, więc...</p>Posted by <a href="https://www.facebook.com/AnnaSigridBrzezinska">Anna Brzezińska</a> on&nbsp;<a href="https://www.facebook.com/AnnaSigridBrzezinska/posts/913945500735840">Tuesday, March 5, 2024</a></blockquote></div></div>

Nie wiemy, jaki los spotkał Eleanor. Eleanor nosiła kobiece stroje oraz używała kobiecego imienia. Jako pracownica seksualna żyła wśród innych kobiet, mogła liczyć na ich wsparcie. Zajmowała się haftem, popełniała typowo kobiece grzechy – pracę seksualną. Jest tylko jeden szczegół: urodziła się jako John Rykener. 

> „Zeznanie raczej nie oddaje wiernie słów Eleanor, bo jest po łacinie, a mało prawdopodobne, by ją znała. Spośród odnoszących się do niej 37 zaimków osobowych w tekście 2 są kobiece, 13 męskich, a 20 nie daje się określić (np. »se« po łacinie jest wspólne dla różnych rodzajów) lub są pominięte. Zdaje się, że skryba sądowy zgłupiał i nie wiedział, jak sobie z Eleanor radzić.
>
> Wymiar sprawiedliwości również zgłupiał, bo nic nie słychać, żeby na podstawie tego zeznania wszczęto dalsze postępowanie. Chyba po prostu Eleanor wypuścili i mam nadzieję, że żyła dalej długo i szczęśliwie, nosząc nadal kobiece stroje i nazywając się Eleanor”.

Dziś możemy ze sporą dozą prawdopodobieństwa powiedzieć, że Eleanor była transpłciową kobietą. Czy binarną? Tego nie wiemy, bo wątpliwe, by znała pojęcie niebinarności. Niezwykłe jest natomiast, że jej kobiecą stronę uznał nawet sąd, stosując naprzemiennie formy męskie, żeńskie, nieokreślone lub pomijając je całkowicie. Eleanor zeznawała w londyńskim sądzie w 1394 roku. 

<div class="text-center mb-3"><div class="fb-post" data-href="https://www.facebook.com/AnnaSigridBrzezinska/posts/pfbid0qcjSkbpaiP9gS6e4AnT6oNvsey2JREzP7Mck3xJzSXeN7jxZDVEtrgDzGRpPHmEsl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/AnnaSigridBrzezinska/posts/916482273815496" class="fb-xfbml-parse-ignore"><p>11 listopada 1394 w londyńskim sądzie zeznawali John Britby rodem z hrabstwa York oraz John Rykener, który nazywał...</p>Posted by <a href="https://www.facebook.com/AnnaSigridBrzezinska">Anna Brzezińska</a> on&nbsp;<a href="https://www.facebook.com/AnnaSigridBrzezinska/posts/916482273815496">Friday, March 8, 2024</a></blockquote></div></div>

Jest wreszcie Jan – i zagubienie jego ojca, ponieważ Jan przyszedł na świat jako osoba interpłciowa, mająca dwa zestawy genitaliów. 

> „Ale notariusz zapewnia, że jeśli w naszym Jasiu – zauważasz, że mówi o naszym Jasiu, więc nie jesteś ze swoim strapieniem tak zupełnie osamotniony – płeć męska przeważy, to będzie żyć jak mężczyzna, pomimo kobiecego naddatku. Tak się czasami zdarza, tłumaczy. Zasięgnął języka wśród notariatu i powziął wiedzę, że w sądzie kościelnym w Konstancji całkiem niedawno była taka sprawa. Jakiś czas wcześniej urodziła się pewnemu dobremu obywatelowi w Rottweil córka, której na chrzcie nadano imię Katarzyny. A kiedy podrosła, nałożyła męskie odzienie, ogłosiła, że jest mężczyzną i kazała się wołać Hans. Później ten Hans wziął żonę, śliczną młodą dzieweczkę: oboje mieli po jakieś 12 lat. Ale Hansowi urosły piersi, zupełnie jak jego żonie, więc obywatele Rottweil wysłali oboje do sądu kościelnego w Konstancji, żeby tam rozstrzygnięto, czy aby są małżeństwem. Obejrzano więc Hansa i okazało się, że ma i penis i pochwę. Odesłano go następnie z żoną z powrotem do domu.
>
> > A jak poznać, która płeć przeważa? – frasujesz się. Notariusz zamawia kolejny dzban wina. Otóż wedle Hugoccio w Summa decretorum, tłumaczy, rozstrzygające jest badania ciała oraz zwyczaje. Jeśli ktoś posiada brodę i zawsze chce wykonywać męskie czynności, nie zaś kobiece, woli rozmawiać z mężczyznami, nie zaś kobietami, to oznacza, że płeć męska w nim przeważa, a zatem może być świadkiem w tych sprawach, gdzie świadectwo kobiety nie jest dopuszczane, w testamentach i ostatnich wolach i może być wyświęcony. Poza tym użyteczne jest obejrzenie genitaliów przez doświadczone położne lub chirurgów.
>
> No więc żyjesz w swojej epoce, a poza tym kręci ci się w głowie od wypitego wina i całego tego ambarasu z Jasiem, więc nie pojmujesz wagi faktu, że jeden z najbardziej wpływowych kanonistów średniowiecza twierdzi, że przy określaniu płci znaczenie ma nie tylko anatomia, nie tylko wygląd genitaliów, ale zachowanie i preferencje też.
>
> Ale jesteś porządnym człowiekiem, więc bez względu na wszelkich kanonistów, pies im mordę lizał, zamierzasz zadbać o swoją krew. Tylko w głowie kręci ci się coraz bardziej.
>
> A co jeśli płci dominującej nie ma? – docieka jakiś wesołek. A to jest kwestia sporna, notariusz z zafrasowaniem drapie się pod biretem. Niektórzy wierzą, że istnieją doskonali hermafrodyci, tacy, którzy mogą uprawiać seks zarówno z kobietami, jak i mężczyznami, wykorzystując na przemian różne ze swych organów. Pisze o takim przypadku Hostiensis (XIII w.), twierdząc, że żył taki w diecezji turyńskiej i z rozkazu miejscowego biskupa musiał przysiąc, że będzie odtąd używał tylko jednego zestawu genitaliów, wedle własnego wyboru, ale pojedynczego. To wydaje się uczciwe, kiwają głowami pijący, bo co by to było, gdyby każdy miał dwa zestawy genitaliów i wedle widzimisię zażywał rozkoszy a to z mężczyzną, a to z kobietą?
>
> Ale z Jasiem tak nie będzie, widać, że rośnie nam tęgi chłopak. I wypijmy za to”.

<div class="text-center mb-3"><div class="fb-post" data-href="https://www.facebook.com/AnnaSigridBrzezinska/posts/pfbid0dXPJP4uvixXN77cttEvKNR8a4RdkyooNCGu7WDQq81SQZ1bkXbMwFWzjVT4Wce6jl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/AnnaSigridBrzezinska/posts/972162168247506" class="fb-xfbml-parse-ignore"><p>Żyjesz więc sobie bogobojnie w średniowieczu. Zapewne na jakimś zadupiu, bo metropolii nie ma. W życiu nie słyszałeś o...</p>Posted by <a href="https://www.facebook.com/AnnaSigridBrzezinska">Anna Brzezińska</a> on&nbsp;<a href="https://www.facebook.com/AnnaSigridBrzezinska/posts/972162168247506">Tuesday, May 28, 2024</a></blockquote></div></div>

Interpłciowość oczywiście nie jest oczywiście niebinarnością, natomiast niezwykłe są zapisy z „Summa decretorum” – pokazujące, że ma znaczenie, czy dana osoba żyje jak mężczyzna czy jak kobieta. O ileż to bardziej postępowe niż okaleczanie interpłciowych dzieci po narodzeniu, by na siłę wpasować je w określoną płeć, z którym spotykamy się dziś! (Prawami osób interpłciowych zajmuje się [Fundacja Interakcja](https://www.interakcja.org.pl/o-nas/).)

{gallery={
    "/img/pl/blog/summa-artis-notariae.png": "! Pierwsza strona „Summa artis notariae” Rolandusa Passagerii",
}}


Niebinarności może natomiast dotyczyć zapis z XIII wieku, w którym dowiadujemy się o osobach, które są „hermafrodytami doskonałymi”. Oczywiście mogą to być osoby, które były po prostu biseksualne, ale istnienie pomiędzy płciami lub poza płcią mogło dotyczyć też innej strony ich tożsamości. Biskup sprowadzał tę kwestię do korzystania z konkretnego zestawu genitaliów, więc nie dowiemy się, jak żyły te osoby – i jak żyłyby, gdyby mogły robić to po swojemu.

Przeprowadzając [wywiad z Anną Brzezińską](https://niestatystyczny.pl/2024/04/15/literatura-powinna-byc-przygoda-radoscia-i-intelektualnym-wyzwaniem-wywiad-z-anna-brzezinska-autorka-mgly/) po premierze książki „Mgła”, zapytałom o jej działalność publicystyczną. 

> ATG: Wróćmy jeszcze do Twojej działalności w sieci. (…) Wyciągasz na światło dzienne też queerową część naszej historii: jest Wojciech lub też Wojciecha, przez poznański magistrat uznana za kobietę, a przez kazimierski sąd – za mężczyznę. Jest i Eleanor, która żyła, nosząc kobiece ubrania, wykonując kobiecy zawód, żyjąc wśród kobiet i mogąc liczyć na ich wsparcie, choć jej dokumenty głosiły, że urodziła się jako John Rykener. To sprawy z XVI i XIV wieku, a są środowiska, w których kontrowersyjne byłyby i dzisiaj!
>
> AB: Mamy źródła, które poświadczają istnienie takich osób, a kategorie płci w przeszłości funkcjonowały inaczej niż dzisiaj, więc zaskoczyła mnie wyłącznie awantura, jaka się rozpętała pod tymi wpisami, bo przecież nikogo nie przekonywałam, żeby się przebierał w kieckę i hulał w Oxfordzie. Tymczasem musiałam odpalić nożyczki i ciąć komentarze, bo pojawiały się wypowiedzi obraźliwe i raniące. To nic nie mówi o przeszłości, ale o teraźniejszości sporo. Natomiast uważam – i jest to jeden z nielicznych przypadków, kiedy wychynęłam spoza źródeł i wprost opisałam swoją intencję – że kwestionowanie istnienia takich osób w przeszłości czy tłumaczenie ich zeznań chorobą psychiczną ma oczywisty i aktualny wymiar polityczny.
>
> ATG: Czy po tej pierwszej awanturze nie kusiło Cię, by odpuścić, darować sobie queerową tematykę i zająć się czymś PR-owo bardziej bezpiecznym?
>
> AB: Nie widzę powodu. I nie jest to żadna szczególna odwaga, bo nic mi nie grozi: najwyżej przeczytam kilka niemiłych komentarzy, że nie jestem historyczką, tylko zwykłą grafomanką. Nie pęknie mi od tego serce z trzaskiem.

Działalność Anny Brzezińskiej potwierdza to, co wiemy od dawna – że queerowe osoby nie tylko są, ale i były od zawsze. Że nasze istnienie nie jest modą czy kaprysem, że mamy naszą własną historię. Queerstoria nie jest niestety dobrze zbadana – ponieważ czyjejś tożsamości nie da się sprowadzić do dat. Nie poznamy świadectw osób niepiśmiennych, nie poznamy pamiętników, które spłonęły lub zawilgotniały i przez lata uległy rozkładowi. To, co wiemy, przefiltrowane zostaje przez oficjalne dokumenty, które redagowały zupełnie inne osoby, mogące dowolnie wymazywać to, co dla nich niewygodne. A jednak mimo wszystkich tych trudności wciąż da się odnaleźć namacalne dowody na nasze istnienie bez względu na epokę czy krainę geograficzną. A dzięki osobom takim jak Anna Brzezińska, możemy odkrywać te skrawki queerstorii. 

– Anna Tess Gołębiowska ([@tess](/@tess))
