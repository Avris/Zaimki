# Ministrzyszcze zasrane, zajebane! – czyli Wojciech Orliński zabiera nas w podróż w czasie

<small>2024-05-20 | [@Tess](/@Tess)</small>

![](/img/pl/blog/ministrzyszcze-1.jpg)

To, że na łamach „Polityki” ukazał się felieton zatytułowany [„Ministrzyszcze zwietrzałe”](https://www.polityka.pl/tygodnikpolityka/kraj/2256013,1,ministrzyszcze-zwietrzale.read), jest interesujące samo w sobie. Ale chyba jeszcze ciekawsze jest to, że tekst… nie dotyczył języka neutralnego czy neologizmów.

Wojciech Orliński, długoletni dziennikarz „Gazety Wyborczej”, z którą ostatecznie pożegnał się w tym roku, autor bloga [„ekskursje w dyskursie”](https://ekskursje.pl) oraz kilkunastu książek, rozpoczął właśnie działalność jako felietonista „Polityki”. W tekście skupił się właśnie na… felietonach – tym, czym są i jak wymykają się definicjom. Przytoczył też przykład tekstów, w których widział proto-felietony, choć poprzedziły one o dwa stulecia powstanie pojęcia „felieton”.

>Jeszcze zanim w Polsce zaczęły się ukazywać regularne gazety, wydawano jednorazowe druki ulotne, bardzo popularne wśród prowincjonalnej drobnej szlachty. Często były to przypominające prasę biuletyny z bieżącymi nowinami, typu „książę Saskie w wielką melankoliją wpadł, a pono oszalał”.

> To było dziennikarstwo bez redakcji. Dziś byśmy powiedzieli: obywatelskie (…).

> Prof. Konrad Zawadzki napisał kiedyś pracę o tych drukach, która dziś może posłużyć jako zasób gotowych cytatów komuś, kto chce na Twitterze zabłysnąć staroświeckim hejtem. Na przykład pisząc o kimś per „ministrzyszcze jedno marne, głupie, zwietrzałe”. Tak ksiądz Kacper Sawicki w 1614 r. hejtował swojego protestanckiego polemistę.

„Ministrzyszcze” to piękny przykład neutratywu. Pojawia się w nim ten sam sufiks, z którego korzystamy i dzisiaj, mówiąc [„aktywiszcze”](https://zaimki.pl/neutratywy#aktywiszcze) czy [„wiedźmiszcze”](https://zaimki.pl/neutratywy#wiedźmiszcze)!

<div class="text-center mb-3"><div class="fb-post" data-href="https://www.facebook.com/wojciech.orlinski.5/posts/pfbid0UAc9VTV2LCoc8nPCPR9eE23bbXegFAC2xvUkd7qmF55i5z4Pt6cWC7YMNsCnBxy1l" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/wojciech.orlinski.5/posts/7498158526968886" class="fb-xfbml-parse-ignore"><p>Jest już w sieci mój pierwszy felieton w najnowszej &quot;Polityce&quot;. Link w komciu, a w obrazku źródło cytatu wykorzystanego...</p>Posted by <a href="#">Wojciech Orlinski</a> on&nbsp;<a href="https://www.facebook.com/wojciech.orlinski.5/posts/7498158526968886">Tuesday, May 14, 2024</a></blockquote></div></div>

Neutratywy to zazwyczaj neologizmy (nie: nowomowa, jak często zarzucają nam osoby o konserwatywnych poglądach, które przeczytały „Rok 1984” bez zrozumienia lub też nie przeczytały go wcale), ale w tym konkretnym przypadku mamy do czynienia z archaizmem – i to jest fascynujące.

<div class="text-center mb-3"><div class="fb-post" data-href="https://www.facebook.com/neutratywy/posts/pfbid02vgB56MrjaMvnyvGdBQNoGKMnfNPvo6eW2NvUYztK43EN4i1AgaUp19qT71ZbpwJYl" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/neutratywy/posts/582654874020164" class="fb-xfbml-parse-ignore"><p>Gdy w sieci pojawiają się inkluzywne formy, zawsze przybywają! Kto? – zapytacie. Niezmiennie: osoby, które myślą, że wiedzą, o czym był „Rok 1984”. Pozdrawiamy Prawicowy Orwell :)</p>Posted by <a href="https://www.facebook.com/neutratywy">Słownik Neutratywów Języka Polskiego • zaimki.pl</a> on&nbsp;<a href="https://www.facebook.com/neutratywy/posts/582654874020164">Saturday, June 24, 2023</a></blockquote></div></div>

Użycie słowa „ministrzyszcze” w tekście z 1614 roku pokazuje coś, co tak naprawdę wiemy – że neutratywy są formą dla języka polskiego naturalną i zrozumiałą. Jeśli ktoś upiera się, że [nie rozumie](https://zaimki.pl/neologizmy) języka używanego przez osoby niebinarne, pokazuje, że polszczyzna jest mu po prostu obca, bo chyba trudno o lepszy przykład na uniwersalność jakiejś formy co spontaniczne tworzenie jej w ten sam sposób w XVI i w XXI wieku.

Oczywiście tamto „ministrzyszcze” zostało użyte w sposób pogardliwy i obraźliwy – ale nijak nie znaczy to, że dziś neutratywy nie mogą być neutralne znaczeniowo. Słowa zmieniają znaczenia na przestrzeli lat. Obraźliwe stają się neutralne, neutralne mogą stać się obraźliwe. Przykładem tego jest… „kobieta”. Tak, było to słowo obraźliwe wobec białek czy niewiast. Nie musicie wierzyć nam na słowo, posłuchajcie [Marka Łazińskiego](https://sjp.pwn.pl/ciekawostki/haslo/kobieta;5757019.html)!

> Najbardziej znany polski etymolog Aleksander Brückner (pierwodruk słownika w roku 1927) wywodzi kobietę od koby ‘kobyły’ albo od kobu ‘chlewu’ i przypisuje temu słowu znaczenie bardzo obraźliwe: niemoralne myśli i czyny, także nierząd. Tym samym tropem idzie Andrzej Bańkowski (2000). (…)

> Historia kobiety w polszczyźnie jest nie tylko tajemnicza i kontrowersyjna, ale też wyjątkowa na tle innych określeń płci żeńskiej. Określenia te w różnych językach ulegają z biegiem czasu deprecjacji. Pierwotnie neutralne słowo dziewka zaczęło oznaczać służącą, a w nieznacznie zmienionej formie dziwka jest wulgarnym określeniem prostytutki. (…) Tymczasem w polszczyźnie słowo kobieta przeszło drogę odwrotną. Prawdopodobnie nie zaczynało jej na pozycji tak niskiej, jak chciał Brückner, ale było na pewno pejoratywne lub wręcz pogardliwe (w znaczeniu stanowym).

Wojciech Orliński, odnajdując ten przykład, sprawił nam niezwykły prezent. Zrobił też coś jeszcze – użył neutratywu, czyli [formy budzącej kontrowersje](https://zaimki.pl/blog/konserwatywny-prezent), w kontekście całkowicie niejęzykoznawczym, w tytule tekstu. W felietonie z neutratywem w tytule nie pisał o języku neutralnym płciowo czy o niebinarności. Pokazał, że „ministrzyszcze” to po prostu słowo. Owszem, ciekawe i zwracające uwagę, ale istniejące w polszczyźnie słowo, którego można w poważnym tygodniku, pisząc o historii prasy.

– Anna Tess Gołębiowska ([@tess](/@tess))

{gallery={
    "/img/pl/blog/ministrzyszcze-2.jpg": "",
    "/img/pl/blog/ministrzyszcze-3.jpg": "",
}}
