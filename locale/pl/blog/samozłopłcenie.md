# Automisgendering – gdy robisz sobie, co tobie niemiłe (a także o skutkach ubocznych)

<small>2023-08-26 | [@Tess](/@Tess)</small>

![Zdjęcie główne jest połączeniem fragmentu zdjęcia Red Neon Signage autorstwa Marah Bashir (Unsplash.com) oraz flagi osób niebinarnych. Przedstawia neon będący wizualizacją złamanego serca na tle barw żółtej, białej, fioletowej i czarnej.](/img/pl/blog/broken-heart.jpg)

**Tak paradoksalnie się składa, że zarówno obecność neutratywów w języku, jak i ich brak, wiąże się u mnie z działalnością aktywistyczną. Zaczynam do perfekcji opanowywać unikanie form nacechowanych płciowo, ponieważ coraz częściej ich użycie wiąże się dla mnie z rozważaniem: „czy mogę pozwolić sobie na formę, która jest dla mnie komfortowa?”.**

W „Niebinarnym Spisie Powszechnym” badamy m.in. powodu nieużywania form niebinarnych przez osoby niebinarne i poszukujące.
[W 2023 roku](/blog/spis-2023) aż 40% badanych osób zaznaczyło „nie jestem wyoutowanx”,
zaś najczęstszą przyczyną rezygnacji z niebinarnych form – wśród osób, które chciałyby ich używać! – był strach przed transfobią i enbyfobią.
Tę odpowiedź zaznaczyło aż 60,3% osób respondenckich! Częsty był też strach przed stygmatyzacją z powodu „niepoprawnego” języka – aż 41,4%.

{graph=/docs/pl/spis-2023/general/obstacles}

W komentarzach osób respondenckich pojawiły się dodatkowe powody, m.in. niechęć do robienia zamieszania czy stwarzania problemów
czy zmęczenie koniecznością ciągłego wyjaśniania tematyki języka inkluzywnego.
(Najpopularniejsze z dopisków uwzględnimy w przyszłorocznym „Niebinarnym Spisie Powszechnym” –
jeśli nie chcesz go przegapić, możesz [zapisać się do naszego newslettera](/spis) – tylko dwie wiadomości rocznie!)

Od trzech lat działam w Radzie Języka Neutralnego, pracując nad projektem zaimki.pl; jestem wyoutowaną osobą niebinarną,
a jednak zauważam, że poruszany problem nie jest mi obcy. 

Kiedy się przedstawiam, podaję parę zaimków: ono/jego i ona/jej, w tej właśnie kolejności. Jest nieprzypadkowa – 
forma ono/jego jest dla mnie najbardziej komfortowa, jest moja. Z żeńskich form korzystam coraz rzadziej i przychodzi mi to z coraz większym trudem. 

Nie mam nic przeciwko temu, że w czasie dłuższej rozmowy albo w obszernej wiadomości ktoś raz czy dwa przekręci zaimki i użyje formy ona/jej.
Gdy widzę, że dana osoba stara się uszanować moją tożsamość i próbuje korzystać z poprawnych form, nie widzę problemu.
Każdy może się pomylić, błędy się zdarzają. Tak naprawdę to jest podstawowy powód podawania przeze mnie dwóch form – by osoba,
która dopiero uczy się języka neutralnego płciowo, nie bała się, że przeoczy jakąś końcówkę i przypadkiem sprawi mi przykrość. 

Coraz częściej jednak obserwuję uporczywe zwracanie się do mnie w żeńskich formach motywowane „przecież w profilu są podane dwie opcje!”.
Żeby nie było: nie mam pretensji, że ktoś nie czyta mi w myślach. Rozumiem, że można uznać, że kolejność form jest przypadkowa
i wybrać tę wygodniejszą, bardziej oswojoną. Mówię o sytuacjach, kiedy zwracam komuś uwagę, że moje preferowane formy to ono/jego,
a dana osoba uparcie to ignoruje, używając profilu jako wymówki – przecież wymieniasz też ona/jej, będę tak mówić i kropka!
To chyba jeszcze bardziej przykre niż zwykły misgendering – inkluzywność na pokaz („sprawił_m profil!”)
przy ignorowaniu sedna (informacja ode mnie w czasie rzeczywistym).

Czasami jednak samo używam form żeńskich, mówiąc o sobie. Dlaczego? 

Nie mam wątpliwości w zwykłej rozmowie – nie ma znaczenia, czy to wiadomość do wąskiego grona, czy to grupa dyskusyjna
licząca kilkaset osób, czy publiczny profil – używam rodzaju neutralnego. W ten sposób myślę, w ten sposób czuję.
Ten sposób pisania to dla mnie brak autocenzury. Jestem jednak osobą dziennikarską i aktywistyczną –
pisuję teksty przeznaczone dla szerszego grona. I za każdym razem, nim kliknę „wyślij” czy „opublikuj”,
zastanawiam się – mogę sobie pozwolić na szczerość i otwartość?

Nie czarujmy się – używanie rodzaju neutralnego wiąże się narażaniem się na enbyfobię i transfobię.
Ta towarzyszy mi często. Czasami próbuję rozmawiać, kiedy indziej po prostu banuję.
Przy artykułach pojawia się jednak pytanie dodatkowe – czy mogę sobie pozwolić, żeby rozmowa o formach przysłoniła treść?

Przykład pierwszy z brzegu – prawa reprodukcyjne. Antyludzkie prawo obowiązujące w naszym kraju sprawia,
że kolejne Polki umierają z powodu komplikacji w ciąży. Gdy piszę tekst o tematyce aborcji,
zależy mi na dotarciu z postulatami jak najszerzej. Muszę liczyć się z tym, że część osób, które ten tekst przeczyta,
to osoby gardzące niebinarnością. Jeśli użyję rodzaju neutralnego, z dużym powodzeniem dyskusja skupi się na nim,
a sedno tekstu zostanie zignorowane. Osoby o poglądach konserwatywnych odrzucają też neutratywy, mogę więc się spodziewać,
że jeśli ktoś szanuje formy neutralne, to prawdopodobnie ma też progresywne poglądy.
Pisząc o prawach reprodukcyjnych do osób o progresywnych poglądach, przekonywałobym przekonanych. 

Są też teksty, które piszę do zupełnie innego grona – recenzje książek, wywiady z osobami autorskimi.
Mogą je czytać osoby w różnym wieku, o różnych poglądach politycznych. Używanie rodzaju neutralnego w czasie rozmowy
z pisarką konfudowałoby zapewne zarówno rozmówczynię, jak i osoby czytelnicze. 

Niko Graczyk ([@niko.graczyk](/@niko.graczyk)) używającu form onu/jeno będące [osobą redaktorską portalu Noizz.pl](https://noizz.pl/autorzy/niko-graczyk)
konsekwentnie korzysta z tych form w swoich artykułach. Przyznaję, że i podziwiam, i zazdroszczę nu odwagi.
Ja poza tekstami o queerowej tematyce oraz luźnymi przemyśleniami ciągle nie potrafię zdobyć się na pełną otwartość. 

Korzystam z neutratywów znacznie częściej niż kiedyś – gdy używałom ich tylko w prywatnych rozmowach z kilkorgiem zaufanych osób,
w większości także niebinarnych lub przynajmniej transpłciowych – i czuję się z tym dobrze. Nie wiem jednak,
czy dalej czułobym się w ten sposób, gdyby okazało się, że rodzaj neutralny sabotuje moją pracę, zamykając mnie w wąskiej bańce.
Nie jestem jeszcze gotowe, by to sprawdzić…

Jest też drugi aspekt, którym jest rodzina. O ile najbliższe mi osoby (a także wszystkie te, które mają mnie w znajomych na Facebooku)
wiedzą o mojej niebinarności, o tyle trudno mi sobie wyobrazić coming out przed babcią i dziadkiem, liczącymi niemal 90 lat.
Nie chodzi nawet o strach przed dyskryminacją, a o to, że nie jestem w stanie wytłumaczyć im, czym jest niebinarność.
Jest między nami przepaść pokoleniowa – to osoby, które nie korzystają z komputerów i smartfonów, dla których internet jest czarną magią,
w dodatku mające coraz większe problemy z pamięcią. Nigdy nie udało mi się wytłumaczyć im różnicy między adresem mejlowym a stroną www,
więc jak tu wchodzić w queerowe etykietki?

Pozostając w kręgu rodzinnym – mam też dziecko. Biorąc pod uwagę, że ono dopiero uczy się mówić, mogłobym spróbować
uczyć go rodzaju neutralnego, ale pojawia się strach – co, kiedy pójdzie do przedszkola?
Czy nie zostanie wyśmiane, kiedy okaże się, że mówi inaczej niż wszyscy? Czuję, że nie mam prawa go na to narażać,
dlatego uczę go słowa „mama”. Kiedyś, kiedy podrośnie, wyjaśnię mu, że wolę być nazywane „mamuś” lub po prostu rodzicem.

Chciałobym żyć w świecie, gdzie takie problemy są dawno za nami, a osoby niebinarne cieszą się pełnią praw,
w tym prawem do swobodnego korzystania z form, które są im najbliższe. Niestety – rzeczywistość tu i teraz jest inna.
Niebinarność w dyskursie społecznym jest absolutną nowością, a jeszcze trzy lata temu nikt poważnie nie zajmował się
językiem osób niebinarnych. Być może powinnom poświęcić własny komfort i używać wyłącznie form ono/jego, 
żeby bezkompromisowo przecierać szlaki. Czasem mam wyrzuty sumienia, że tego nie robię. Czasem marzę, 
że ta zmiana dokona się bez mojego udziału, a za Niko Graczyk podążą kolejne osoby.
Od trzech lat staram się walczyć o język neutralny płciowo w przestrzeni publicznej – działając w ramach Rady Języka Neutralnego,
pracując nad projektem zaimki.pl, publikując i udostępniając teksty, uwzględniając osoby niebinarne w wystąpieniach na demonstracjach, 
od niedawna: idąc w Marszach Równości z transparentem w rodzaju neutralnym. Wdając się w dyskusje w sieci,
choć często wiem, że nie mam szans przekonać osoby, która mnie właśnie obraża, ale mam nadzieję, 
że w ten sposób inne osoby zetkną się z ideą języka neutralnego płciowo.

To nie jest tekst o rozwiązaniach, raczej o pytaniach, które sobie zadaję i na które nie mam dobrych odpowiedzi.

Chciałobym robić więcej, ale nie potrafię, dlatego czasem idę na kompromis – samo ze sobą. To samo w sobie nie jest przyjemne. 
Boli, że dodatkowo bywa wykorzystywane jako argument, by misgenderować mnie nawet w teoretycznie bezpiecznej przestrzeni.

Jest jednak rzecz, o którą na marginesie chciałobym zaapelować:

Drogie osoby, także te queerowe i świadome – jeśli ktosio mówi Wam, że używa konkretnych form, po prostu uszanujcie to, 
nie próbując „zagiąć” tej osoby, że wczoraj w innym miejscu i w innym kontekście używała innych zaimków. 
Tym bardziej, że zwyczajnie możecie nie wiedzieć, że dana osoba jest genderfluid i poprzedniego dnia nie sprawiało jej to bólu,
a dziś już może dać nawet atak dysforii. Korzystajcie z form, których dana osoba używa tu i teraz, 
a jeśli je przeoczycie, po prostu zareagujcie na prośbę. Tylko tyle i aż tyle. 
