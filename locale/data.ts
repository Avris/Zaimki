import type { gendersWithNumerus } from '~/src/nouns.ts';

export type PronounData<M extends string> = {
    key: string;
    description: string;
    normative: boolean;
    plural: boolean;
    pluralHonorific: boolean;
    pronounceable: boolean;
    history?: string;
    thirdForm?: M;
    smallForm?: M;
    sourcesInfo?: string;
    hidden?: boolean;
} & { [morpheme in M]: string | null };

export interface PronounGroupData {
    key?: string;
    name: string;
    pronouns: string;
    description?: string;
    hidden?: boolean;
}

export interface PronounExamplesData {
    singular: string;
    plural?: string;
    null?: string;
    null_plural?: string;
    isHonorific?: boolean;
    categories?: string;
}

export type NounTemplatesData = {
    [G in typeof gendersWithNumerus[number]]?: string;
};
