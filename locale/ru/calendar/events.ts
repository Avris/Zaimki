import { Event, day, EventLevel } from '~/src/calendar/helpers.ts';

export default [
    // --- static date ---
    new Event('Годовщина декриминализации гомосексуальности в России (1993)', { type: 'flag', name: '_law', class: 'invertible' }, 4, day(29), EventLevel.Day),
    new Event('Годовщина декриминализации гомосексуальности в Беларуси (1994)', { type: 'flag', name: '_law', class: 'invertible' }, 3, day(1), EventLevel.Day),
];
