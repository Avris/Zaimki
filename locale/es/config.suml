locale: 'es'

style:
    fontHeadings: ['Quicksand']
    fontText: ['Nunito']

header: true

pronouns:
    enabled: true
    morphemes:
        - 'pronoun'
        - 'direct_object_pronoun'
        - 'definite_article'
        - 'indefinite_article'
        - 'plural_pronoun'
        - 'plural_definite_article'
        - 'plural_indefinite_article'
        - 'plural_direct_object_pronoun'
        - 'inflection'
        - 'inflection_c'
    route: 'pronombres'
    any: 'cualquiera'
    plurals: false
    honorifics: false
    generator:
        enabled: true
        startPronoun: 'él'
        slashes: false
    multiple:
        name: 'Formas intercambiables'
        description: 'Muchas personas no binarias usan más de una forma intercambiablemente y se sienten cómodas con cualquiera de las opciones.'
        examples: ['él&ella', 'él&elle', 'ella&elle']
    null:
        routes: ['evitar']
        ideas:
            -
                header: 'Usar nombres o iniciales en lugar de pronombres'
                normative: true
                examples:
                    - ['Ayer hable con el', 'Ayer hable con Sky']
                    - ['Ella es muy inteligente', 'Soph es muy inteligente']
                    - ['Ella se graduará pronto', 'J se graduará pronto']
            -
                header: 'Voz pasiva'
                normative: true
                examples:
                    - ['El contestó al teléfono.', 'El teléfono fue contestado.']
                    - ['Paula cuida bien de su gato', 'El gato de Paula es bien cuidado']
            -
                header: 'Reestructurar la frase (paráfrasis)'
                normative: true
                examples:
                    - ['Lior lo hizo él solo.', 'Lior lo hizo sin ayuda']
                    - ['Gael habló sobre ell_ mism_ en su sueño', 'Gael habló sobre su persona en su sueño']
            -
                header: 'Reemplazar los pronombres con un adjetivo descriptivo o una frase'
                normative: true
                examples:
                    - ['Ella aterrizó el avión', 'La persona pilotando aterrizó el avión']
                    - ['Esta es Lea, ella es artista', 'Esta persona es Lea, es artista']
                    - ['Ella debate sobre…', 'La persona que empezó la conversación debate sobre…']
            -
                header: 'Omitir pronombres'
                normative: true
                examples:
                    - ['¿Le has comprado un regalo a ella?', '¿Le has comprado un regalo?']
                    - ['Si, le compré un regalo a ella', 'Si, lo le compré un regalo']
    emoji: false
    mirror:
        route: 'espejo'
        name: 'Pronombres Espejo'
        description: >
            Una persona que use pronombres espejo quiere que se le trate con los pronombres de la persona con quien está hablando.
        example:
            - 'La Persona A usa pronombres espejo.'
            - 'La Persona B usa {/ella=ella/la}, así que cuando ella hable sobre la Persona A, ella usará “ella/la”.'
            - 'La Persona C usa {/elli=elli/li} junto con {/ellæ=ellæ/læ} indistintamente, así que cuando elli hable sobre la Persona A, ellæ usará elli/li o ellæ/læ.'
    others: 'Otros pronombres'
    sentence:
        subdomains: ['mis', 'nuestros']
        prefixes: ['/son']
        examples:
            - 'mis.pronombr.es/son/elle/le'
            - 'mis.pronombr.es/son/xelle&ella'
            - 'nuestros.pronombr.es/son/elli/li'

pronunciation:
    enabled: true
    voices:
        ES:
            language: 'es-ES'
            voice: 'Lucia'
            engine: 'standard'
        MX:
            language: 'es-MX'
            voice: 'Mia'
            engine: 'standard'

sources:
    enabled: true
    route: 'fuentes'
    submit: true
    mergePronouns: {}

nouns:
    enabled: true
    route: 'diccionario'
    collapsable: false
    plurals: false
    pluralsRequired: false
    categories: []
    declension: false
    submit: true
    templates:
        enabled: false

community:
    route: 'terminologia'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: true
    categories:
        -
            key: 'orientación sexual'
            text: 'orientación sexual'
        -
            key: 'orientación romántica'
            text: 'orientación romántica'
        -
            key: 'orientación terciaria'
            text: 'orientación terciaria'
        -
            key: 'género'
            text: 'género'
        -
            key: 'expresión de género'
            text: 'expresión de género'
        -
            key: 'modelo de relación'
            text: 'modelo de relación'
        -
            key: 'lenguaje'
            text: 'lenguaje'
        -
            key: 'atracción'
            text: 'atracción'
        -
            key: 'política'
            text: 'política'
        -
            key: 'prejuicio'
            text: 'prejuicio'
    route: 'terminologia'

names:
    enabled: false

people:
    enabled: false

english:
    enabled: false

faq:
    enabled: true
    route: 'preguntas'

links:
    enabled: true
    route: 'enlaces'
    blogRoute: 'blog'
    links:
        -
            icon: 'comment-alt-edit'
            url: 'https://www.aacademica.org/1.congreso.internacional.de.ciencias.humanas/1500'
            headline: 'Fenómenos de convergencia y divergencia en el uso del lenguaje no binario'
            extra: ' – Descripción lingüística del uso de formas no binarias, en especial {/elle=elle/le}'
        -
            icon: 'comment-alt-edit'
            url: 'https://accedacris.ulpgc.es/handle/10553/73757'
            headline: 'El género no binario en la traducción al español: análisis del uso del lenguaje inclusivo no binario'
            extra: ' – Este trabajo aborda el uso de formas no binarias como resultado de traducciones del inglés al español.'
        -
            lang: ['en']
            icon: 'presentation'
            url: 'https://www.queerterpreter.com/lavlang27'
            headline: 'Direct and Indirect Non-binary Language in English to Spanish Translation'
            extra: ' – Ártemis López'
        -
            icon: 'comment-alt-edit'
            url: 'https://drive.google.com/file/d/1m2D2URuaUpTgudSHA9tKg_2dRQ3RAZUM/view'
            headline: 'Innovaciones al género morfológico en el español de hablantes genderqueer'
            extra: ' – Benjamin Papadopoulos, B.A., Universidad de California, Berkeley'
        -
            icon: 'comment-alt-edit'
            lang: ['en']
            url: 'https://www.genderinlanguage.com/spanish'
            headline: 'Gender in Language Project'
    academic:
        generic:
            name: ~
            entries:
                -
                    icon: 'comment-alt-edit'
                    url: '/docs/es/Trabajo de fin de grado – Aleksandra Szmidt – Pronombres no binarios.pdf'
                    headline: 'Szmidt, A. (2022) Los pronombres no binarios en español: análisis de las opiniones de hispanohablantes sobre los pronombres neutros en cuanto al género.'
                    extra: 'Kraków.'
                    quote: >
                        Es conveniente indicar también que elle no es la única propuesta.
                        En la página <em>pronouns.page</em>, creada por un colectivo multinacional de personas LGBT
                        con el fin de promover el lenguaje no binario, en cuanto a la lengua española,
                        salvo el pronombre <em>elle</em> se proponen también otros neopronombres, como:
                        <em>ellu, elli, elloa, il, ól, xelle</em> \[CLN, 2021].
                        Con todo, el conocimiento de estas formas no es tan difundido.
                -
                    icon: 'comment-alt-edit'
                    url: '/docs/es/Michał Koźmiński – Los neutrativos en estadística.pdf'
                    headline: 'Koźmiński, M. (2022) Los neutrativos en estadística: ¿cómo se habla de las personas no binarias?'
                    extra: 'Kraków. {https://www.umcs.pl/pl/nowy-numer,20408.htm=Reflections. Students'' Scientific Papers UMCS}'
                    quote: >
                        El estudio presentado revela dos conclusiones principales. La primera, quizás un tanto
                        sorprendente, es la prevalencia del nuevo paradigma morfológico con x, cuyo uso supera la
                        alternativa con e casi dos veces. Si es verdad que los neutrativos en -x no difieren de los en -e
                        en lo que respecta a su pronunciación, efectivamente podríamos estar hablando de una
                        evolución gráfica de los neutrativos o incluso de su fusión. La segunda conclusión es que el
                        fenómeno del género neutro tiene una extensión nimia –con las estimaciones más optimistas
                        cayendo más de mil veces debajo de los grupos de control, el masculino y el femenino. Una
                        observación bastante evidente al margen de las calculaciones es que en todos los casos el grupo
                        de control femenino apunta menos ocurrencias que el masculino, lo que bien confirma la
                        premisa y el porqué de todas las innovaciones postuladas.
    mediaGuests:
        -
            icon: 'play-circle'
            url: 'https://open.spotify.com/episode/286zTXBHA0ChWNaWSwgFa0'
            headline: 'El lenguaje inclusivo de lxs polacxs – Ausir y Dante en un podcast <strong>Proyecto Polonia</strong>'
    mediaMentions: []
    recommended: []
    blog: true

contact:
    enabled: true
    route: 'contacto'
    team:
        enabled: true
        route: 'team' # TODO

support:
    enabled: true

user:
    enabled: true
    route: 'cuenta'
    termsRoute: 'terminos'
    privacyRoute: 'privacidad'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: ~
            values: ['señor', 'señora', 'señore', 'don', 'doña', 'doñe', 'usted', 'tú', 'vos']
        -
            header: ~
            values: ['persona', 'hombre', 'mujer', 'chico', 'chica', 'chique', 'muchach_', 'tip_', 'herman_', 'tí_', 'güey', 'pib_']
        -
            header: ~
            values: ['lind_', 'guap_', 'bonit_', 'mon_', 'hermos_', 'sexi']
        -
            header: ~
            values: ['pareja', 'novi_', 'marid_', 'espos_', 'media naranja']
    flags:
        defaultPronoun: 'ell_'

calendar:
    enabled: true
    route: 'calendario'

census:
    enabled: false

redirects:
    - { from: '^/diccionario/terminologia', to: '/terminologia' }

api: ~
