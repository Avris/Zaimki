# Cómo crear una nueva versión de pronombr.es en otro idioma

<small>2020-12-06 | [@andrea](/@andrea) | Traducido el 2024-06-19 por [@Kaix0](/@Kaix0)</small>

![Captura de pantalla del menú de idiomas arriba de la página, con el botón "+ Añadir versiones" seleccionado (en inglés)](/img/es/blog/new-version.png)

¿Así que te interesa ayudarnos a extender la plataforma a tu lengua materna? ¡Genial! 🥰

Pero, te preguntarás: ¿Cómo lo hago?

En primer lugar, por favor contáctanos en [localisation@pronouns.page](mailto:localisation@pronouns.page).
¿Tal vez alguien ya comenzó a trabajar en esa lengua?
Sería útil coordinarles antes de que comiences a trabajar en ello.
Les invitaremos a nuestro servidor de Discord y les ofreceremos apoyo.

Si ya sabes utilizar herramientas como Git, Pnpm, JavaScript, etc.,
puedes dirigirte al [código fuente](https://gitlab.com/PronounsPage/PronounsPage/),
clonar el repositorio y configurar el proyecto de acuerdo con las instrucciones en `README.md`.
Podrás ver los cambios en tiempo real conforme los hagas.
Hazles push en una rama separada y envía una petición de pull.

De otra manera, **no te preocupes por el código**. Puedes ayudar a localizar el proyecto aún sin saber programar.
Prepararé todos los archivos necesarios y los compartiré en el mensaje fijado de un canal del Discord dedicado a ello - puedes sencillamente comenzar a editar.

Alternativamente:
Haz click [aquí](https://gitlab.com/PronounsPage/PronounsPage/-/archive/main/Zaimki-main.zip) para descargar un archivo zip con el código.
Ignora todo lo demás, sólo haz una copia del directorio `/locale/_base`,
y ponle nombre conforme al código de la neuva lengua.
En vez de`_base` (una versión en inglés modificada para este propósito) puedes usar otro idioma como base,
si crees que será más fácil – 
p. ej. para traducir a lenguas eslávicas puede ser más fácil comenzar con `pl` (polaco).

Dentro de ese directorio se esconde toda la configuración y el código relacionado con ese idioma en específico.
Los archivos más importantes son:

## [translation.suml](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/translations.suml)

Este archivo contiene las claves de traducción con los valores correspondientes.
Básicamente, manten todo lo que hay detrás del punto y coma como lo encuentres, y traduce todo lo que hay después del punto y coma a tu idioma.

O más bien **localízalo, no lo traduzcas**. Todas las lenguas son únicas, así que recuerda _ajustar_
el contenido a lo que sea que tenga más sentido, no sólo digas lo mismo en otro idioma.
Por ejemplo, el español no sólo marca el género a través de los pronombres, sino también de otras formas –
en este caso, no sólo se traducen las oraciones que usan la palabra "pronombres" como "pronombres" y ya,
sino más como “formas con género”. Otro ejemplo: Hay un párrafo donde comparamos respetar los pronombres de alguien
con respetar su nombre, en el cual usamos nombres como “Lucía” y “Alicia” –
por favor usa en su lugar nombres más populares en tu cultura cuando localices.
En resumen, siéntete libre de añadir, eliminar, y editar todo para hacer el contenido
tan útil, adecuado y relevante para tu lengua como sea posible.
Otras claves de interés para localizar son las de `quotation`, que describen el marcador de cita popularizado o estandarizado de la lengua,
o `calendar.date` y `calendar.dates`, que, en conjunto, describen el formato adecuado para las fechas.

Ya que somos una página sobre lenguaje neutral respecto al género, por favor intenta usarlo siempre que puedas
– incluso cuando no sean formas normativas.

Algunas traducciones tienen marcadores de posición especiales:
- Las palabras delimitadas por `%`, como `%día%`, describen un marcador de posición donde el valor se inserta dinámicamente,
  úsalas para definir la posición en la cual deba ir el valor insertado
- Los enlaces se definen con `{enlace=texto a visualizar}`
  Obviamente deberías traducir el texto a visualizar con normalidad, pero en el caso de enlaces internos
  (que no salgan del sitio actual) deberías usar el mismo nombre de ruta como en `config.suml`
- Los íconos se generan con `[nombre]`. No hace falta traducir aquí
- Puedes añadir morfemas en los nombres de banderas, p. ej. cuando una bandera refiere a un adjetivo
  y lleva marcación de género en tu idioma, como en español: `'Arrománti{inflection_c}'` (más al respecto abajo)

También recuerda que no todo en este archivo requerirá necesariamente una traducción.
Por ejemplo, la localización en polaco contiene traducciones del módulo “names”,
las cuales un nuevo idioma probablemente no tendrá inmediatamente (aunque se podría añadir más tarde).

## [config.suml](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/config.suml)

Aunque este archivo es muy importante, no espero que lo manejes si no tienes el conocimiento técnico.
Aún así, algunas traducciones también se incluyen aquí, así que por favor revísalo y ve si puedes hacer algo útil ahí.
El formato debería ser bastante intuitivo.

Este archivo contiene, por ejemplo, una definición de [enlaces](/links), [artículos académicos](https://zaimki.pl/nauka) etc.
También puedes ajustar esas definiciones directamente en este archivo, si puedes,
o puedes sencillamente ponerlos en un archivo de Word o Excel para que yo me encargue.

## [pronouns.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/pronouns.tsv), [pronounGroups.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/pronounGroups.tsv), [examples.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/examples.tsv)

En este contexto, con "pronombres" nos referimos a cualquier forma con género en tu idioma,
ya sean realmente pronombres o algo como terminaciones en adjetivos o verbos, etc. –
siempre que sean regulares. Con regulares" nos referimos a que sea posible poner reglas en una tabla y que puedan ser aplicadas
a todas (o la mayoría de) las palabras similares; p. ej. en español: querid**a** – querid**o**; rápid**a** – rápid**o**, etc.;
a diferencia de, por ejemplo, algunos sustantivos en inglés: chair**woman** – chair**man**; wait**ress** – wait**er**; **wife** – **husband**, etc. –
para esos, es mejor usar la sección de diccionario donde una base de datos más compleja puede ser manejada de manera más dinámica.

Puedes editar los archivos `.tsv` en Excel, Numbers etc., o incluso en un editor de texto (sólo mantén las tabulaciones donde estén).

Pra construir la sección de "pronombres", te sugiero comenzar creando algunas oraciones de ejemplo concretas
que después puedan ser abstraídas para hacer templetes que el código pueda usar. Por ejemplo, en español una oración podría ser:

 - Ella es muy querida. _(feminine)_
 - Él es muy querido. _(masculine)_
 - Elle es muy queride. _(neuter)_

Comienza sólo con femenino, masculino, y opcionalmente formas neutras – podemos añadir otras (neopronombres, etc.) después.
Crea tantas oraciones como haga falta para mostrar todas las maneras (regulares) posibles en que tu idioma marca el género.

Después, identifica qué partes de esas oraciones se diferencían por género y cuáles se quedan igual:

- **Ella** es muy querid**a**. _(feminine)_
- **Él** es muy querid**o**. _(masculine)_
- **Elle** es muy querid**e**. _(neuter)_

Por último, reemplaza todas esas partes con género con "etiquetas", o "ejemplos" que el código pueda usar como lienzos para ir llenando.
Dale nombres descriptivos a dichas etiquetas (en inglés, para que miembros del equipo como programadorxs que no hablen tu lengua puedan entender su propósito)
y márcalas con `{…}`, por ejemplo: `{pronoun_nominative}`, `{adjective_inflection}`…

- {pronoun_nominative} es muy querid{adjective_inflection}.

Ahora puedes poner todas esas oraciones en `examples.tsv` y empezar a trabajar en `pronouns.tsv`.

Cada fila en este archivo representa un conjunto de pronombres, como elle/le, ella/la etc. Y las opciones más populares
(todas las demás podrán ser creadas por usuaries dinámicamente en el generador).

La columna `key` contiene “claves”, identificadores únicos que el código buscará con base en el URL.
Por ejemplo, si la clave es `elle,elle/le` entonces tanto el enlace [pronombr.es/**elle**](https://pronombr.es/elle)
como [pronombr.es/**elle/le**](https://pronombr.es/elle/le) identificarán esa columna como la adecuada para esa página.
Las columnas con valores `TRUE` y `FALSE` deberían quedarse en inglés porque se supone que sean entendidas por el código,
no mostradas a les visitantes. Las columnas entre `normative` y `plural` deberán ser añadidas o suprimidas de manera que
cada “etiqueta” que hayas identificado en el paso anterior tenga una columna propia.

También ten en cuenta que, una vez más, se trata de localisar, no traducir, así que no sólo copies y pegues neopronombres del inglés o del español,
a menos que realmente sean usados en tu idioma también, usa en su lugar lo que sí es realmente usado o propuesto.
Las columnas como `history` deben contener información extra sobre cómo se origina esa forma en tu idioma,
y no ser una traducción de la historia del they singular en inglés, etc. etc.

En nuestro ejemplo simplificado en español, la tabla de `pronouns.tsv` debería verse así:

{wide_table}

| key          | description                | normative | pronoun_nominative | adjective_inflection | plural | pluralHonorific | pronounceable | history | thirdForm | smallForm | sourcesInfo |
|--------------|----------------------------|-----------|--------------------|----------------------|--------|-----------------|---------------|---------|-----------|--------------|--------------|
| ella,ella/la | Femenino                   | TRUE      | ella               | a                    | FALSE  | FALSE           | TRUE          |         |           |           |             |
| él,él/lo     | Masculino                  | TRUE      | él                 | o                    | FALSE  | FALSE           | TRUE          |         |           |           |             |
| elle,elle/le | Neopronombre neutro “elle“ | FALSE     | elle               | e                    | FALSE  | FALSE           | TRUE          |  Forma neutra, también llamada “lenguaje inclusivo”. Este pronombre es utilizado para referirse a personas no binarias, y también para grupos mixtos en cuanto a género, para no excluir a nadie.  |           |           |             |

{/wide_table}

Para finalizar, en `pronounGroups.tsv` puedes especificar qué pronombres deberán ser mostrados y descritos en conjunto.

## Administrar entradas de la base de datos

Todo lo descrito arriba es más o menos “estático”: la configuración, las traducciones, las definiciones de formas regulares con género, etc.
Sin embargo, muchas partes del sitio web requieren una aproximación más “dinámica” approach, donde las adiciones y modificaciones sean frecuentes
y estén abiertas a trabajo comunitario – cosas como el diccionario de [formas con género](/dictionary), el de [terminología queer](/terminology),
el de [sugerencias de formas inclusivas](/inclusive), etc.

Una vez que el primer borrador de las partes “estáticas” (incluso sólo una traducción) esté listo,
por favor hazme ping en Discord ([@andrea](/@andrea)),
o envíanos un correo electrónico a [localisation@pronouns.page](mailto:localisation@pronouns.page),
o envía una petición de pull, etc. – y configuraré un servidor de prueba
donde podrás ver tu trabajo en práctica así como comenzar a añadir las partes “dinámicas” directamente a través del sitio web.

Una vez que el servidor esté en marcha, también ofrecerá una interfaz para proponer traducciones faltantes de nuevas características y corregir errores en las existentes.

## Y más…

Te advierto: Crear una versión en un nuevo idioma es mucho trabajo 😅
Los pasos anteriores describen la mayoría del proceso, pero ten en mente que habrán otros detalles y procesos menores que irán surgiendo.

Si no te intimida la cantidad de esfuerzo requerida, entonces, pues, ¡buena suerte! ¡Y gracias por contribuir! 🥰
