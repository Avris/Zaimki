locale: 'it'

style:
    fontHeadings: ['Quicksand']
    fontText: ['Nunito']

format:
    timezone: 'Europe/Rome'

header: true

pronouns:
    enabled: true
    morphemes:
        - 'pronoun_n'
        - 'pronoun_d'
        - 'pronoun_a'
        - 'inflection'
        - 'article'
        - 'article_v'
        - 'article_s'
        - 'indefinite_article'
        - 'indefinite_article_v'
        - 'indefinite_article_s'
        - 'articled_preposition'
        - 'articled_preposition_v'
        - 'articled_preposition_s'
        - 'possessive'
        - 'possessive_m'
        - 'plural_inflection'
        - 'plural_article'
        - 'plural_article_vs'
        - 'plural_articled_preposition'
        - 'plural_articled_preposition_v'
        - 'plural_articled_preposition_s'
        - 'plural_possessive'
        - 'plural_possessive_m'
        - 'l'
        - 'h'
        - 'i'
    route: 'pronomi'
    any: 'qualsiasi'
    plurals: true
    honorifics: false
    generator:
        enabled: true
        startPronoun: 'lui'
        slashes: true
    multiple:
        name: 'Forme intercambiabili'
        description: >
            Molte persone non binarie usano più forme pronominali in modo intercambiabile
            alternando fra queste.
        examples: ['lui&lei', 'lui&ləi', 'lei&ləi']
    null:
        routes: ['evitare', 'niente-pronomi', 'nessun-pronome', 'nome']
        morphemes:
            pronoun_n: '#'
            pronoun_d: 'a(d) #'
            pronoun_a: '#'
        examples: [':Di', ':Andrea']
        ideas:
            -
                header: 'Usare nomi o iniziali invece che i pronomi'
                normative: true
                examples:
                    - ['Gli ho parlato ieri', 'Ho parlato a {/:Sky=Sky} ieri']
                    - ['Penso a lei spesso', 'Penso a {/:Soph=Soph} spesso']
                    - ['Potresti chiamarlo?', 'Potresti chiamare {/:Max=Max}?']
                    - ['La cerimonia di graduazione di mio figlio inizierà presto', 'La cerimonia di graduazione di {/:J=J} inizierà presto']
            -
                header: 'Usare epiteti per non genderare gli aggettivi'
                normative: true
                examples:
                    - ['È simpatico', 'È una persona simpatica']
                    - ['È meravigliosa', 'È un essere umano meraviglioso']
                    - ['Sei una ragazza strana!', 'Sei uno strano individuo!']
            -
                header: 'Parafrasando la frase (circonlocuzione)'
                normative: true
                examples:
                    - ['Ti sei davvero impegnata!', 'Ci hai messo davvero molto impegno!']
                    - ['Lior l’ha fatto tutto da solo', 'Lior l’ha fatto tutto da sé/in autonomia/senza aiuto']
                    - ['È veramente bella', 'È veramente di bell’aspetto.']
                    - ['Questa è Lea', 'Ecco Lea/Ti presento Lea']
            -
                header: 'Rimpiazzare il pronome con un nome descrittivo'
                normative: true
                examples:
                    - ['Le piace dipingere', 'A quella persona piace dipingere']
                    - ['Gli sta parlando di…', 'Sta parlando a quella persona di…']
            -
                header: 'Omettere il pronome'
                normative: true
                examples:
                    - ['Le hai comprato un regalo?', 'Hai comprato il regalo per (nome, oppure semplicemente “Hai comprato il regalo?)']
            -
                header: 'Usare delle “x”'
                normative: false
                examples:
                    - ['Le piace dipingere', 'Lx piace dipingere']
                    - ['Gli sta parlando di…', 'Lx sta parlando di…']
    emoji:
        description: 'Pronomi con le emoji'
        history: '{https://lgbta.wikia.org/wiki/Emojiself_Pronouns=Emojiself} pronouns are intended for online communication and not supposed to be pronounced.'
        morphemes: {}
        examples: ['💫', '💙']
    mirror:
        route: 'specchio'
        name: 'Pronomi specchio'
        description: >
            Una persona che usa i pronomi specchio vuole che le ci si riferisca con gli stessi pronomi della persona con cui sta parlando.
        example:
            - 'A usa i pronomi specchio'
            - 'B usa {/lei=lei/le}, quindi quando A parla con B, A userà i pronomi lei/le per riferirsi a se stessa.'
            - 'C usa {/ləi=ləi/lɜ} in modo intercambiabile con {/loi=loi/lu}, quindi quando A parla con ləi, A può usare sia ləi/lɜ che loi/lu per riferirsi a se stessu.'
    others: 'Altri pronomi'
    shortMorphemes: 3

pronunciation:
    enabled: true
    voices:
        IT:
            language: 'it-IT'
            voice: 'Bianca'
            engine: 'neural'

sources:
    enabled: true
    route: 'sources'
    submit: true
    mergePronouns: {}
    extraTypes: ['evitare', 'nomestesso']

nouns:
    enabled: true
    route: 'dizionario'
    collapsable: false
    plurals: true
    pluralsRequired: false
    categories: []
    declension: false
    submit: true
    templates:
        enabled: false

community:
    route: 'Terminologia'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: true
    categories:
        - 'Orientamento sessuale'
        - 'Orientamento romantico'
        - 'Orientamento terziario'
        - 'Genere'
        - 'Espressione di genere'
        - 'Modello di relazione'
        - 'Lingua'
        - 'Attrazione'
        - 'Politica'
        - 'Pregiudizio'

    route: 'Terminologia'

names:
    enabled: false

people:
    enabled: false

# optional, but would be nice to have
english:
    enabled: true
    route: 'italian'
    pronounGroups:
        -
            name: 'Forme normative'
            description:
                - >
                    A causa delle limitazioni della grammatica in  <language>, o semplicemente perché alcune persone preferiscono così,
                    molte persone non binarie usano “lui” ({/on=„on”}) o “lei” ({/ona=„ona”})
                    – possono usare i pronomi che combaciano col genere assegnato alla nascita oppure del genere opposto.
                    Questo però non rende loro meno non-binary! Pronomi ≠ genere.
            table: { on: 'Masculine', ona: 'Feminine' }

faq:
    enabled: true
    route: 'faq'

links:
    enabled: true
    split: false
    route: 'links'
    blogRoute: 'blog'
    links:
        -
            icon: 'globe-europe'
            url: 'https://pronoun.is/'
            headline: 'Pronoun.is'
            extra: '– ispirazione per questo sito.'
        -
            icon: 'atlas'
            url: 'https://italianoinclusivo.it/'
            headline: 'Italiano Inclusivo'
    academic: {}
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false
    zine:
        enabled: false

contact:
    enabled: true
    route: 'contatti'
    team:
        enabled: true
        route: 'team'

support:
    enabled: true

user:
    enabled: true
    route: 'account'
    termsRoute: 'termini'
    privacyRoute: 'privacy'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: 'Onorifici'
            values: ['[Nessun onorifico]', 'Sig.', 'Sig.ra', 'Sig.rə', 'Sig.rx', 'signor', 'signora', 'signorə', 'signorx', 'signorina', 'signorino', 'signorinə', 'signorinx']
        -
            header: 'Descrizioni personali e familiari'
            values: ['persona', 'uomo', 'donna', 'individuo', 'signore', 'signora', 'signorə', 'ragazzo', 'ragazza', 'ragazzə', 'enby', 'non-binariə', 'maschio', 'femmina', 'fratello', 'sorella', 'germanə', 'fra’', 'sorè', 'germà', 'amo']
        -
            header: 'Complimenti'
            values: ['carinə', 'bellə', 'molto mascolinə', 'attraente', 'zucchero', 'figə', 'sexy']
        -
            header: 'Descrizioni di relazioni'
            values: ['amicə', 'partner', 'fidanzatə', 'dolce metà', 'compagnə', 'carə', 'miə amatə', 'amore', 'amo', 'marito', 'moglie', 'coniuge']
    flags:
        defaultPronoun: 'ləi'

calendar:
    enabled: true
    route: 'calendario'

census:
    enabled: false

redirects: []

api: ~
