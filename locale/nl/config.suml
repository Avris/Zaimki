locale: 'nl'

style:
    fontHeadings: ['Quicksand']
    fontText: ['Nunito']

header: true

pronouns:
    enabled: true
    morphemes:
        - 'nominative'
        - 'accusative'
        - 'pronominal_poss'
        - 'predicative_poss'
        - 'dative'
    route: 'voornaamwoorden'
    any: 'elk'
    plurals: false
    honorifics: false
    generator:
        enabled: true
        startPronoun: 'hij'
        slashes: true
    multiple:
        name: 'Uitwisselbare vormen'
        description: 'Veel non-binaire personen gebruiken afwisselend meer dan een vorm en vinden het prima dat je beiden gebruikt.'
        examples: ['hij&zij', 'hij&hen', 'zij&hen']
    null:
        routes: ['geen-voornaamwoorden']
        morphemes:
            nominative: '#'
            accusative: '#'
            pronominal_poss: '#'
            predicative_poss: '#e'
        examples: [':Andrea', ':S']
    emoji:
        description: 'Emojizelf voornaamwoorden'
        history: '{https://lgbta.wikia.org/wiki/Emojiself_Pronouns=Emojizelf} voornaamwoorden zijn bedoeld voor online communicatie en niet bedoeld om uitgesproken te worden.'
        morphemes:
            nominative: '#'
            accusative: '#'
            pronominal_poss: '#'
            predicative_poss: '#e'
        examples: ['💫', '💙']
    others: 'Andere voornaamwoorden'
    shortMorphemes: 3

pronunciation:
    enabled: true
    voices:
        GB:
            language: 'nl-NL'
            voice: 'Ruben'
            engine: 'standard'

sources:
    enabled: true
    route: 'bronnen'
    submit: true
    mergePronouns:
        hen/hun/hun: 'hen'

nouns:
    enabled: true
    route: 'woordenboek'
    collapsable: false
    plurals: true
    pluralsRequired: false
    categories: []
    declension: false
    submit: true
    templates:
        enabled: false

community:
    route: 'terminologie'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: true
    categories:
        -
            key: 'seksuele geaardheid'
            text: 'seksuele geaardheid'
        -
            key: 'romantische geaardheid'
            text: 'romantische geaardheid'
        -
            key: 'tertiaire geaardheid'
            text: 'tertiaire geaardheid'
        -
            key: 'gender'
            text: 'gender'
        -
            key: 'gender expressie'
            text: 'gender expressie'
        -
            key: 'relatiemodel'
            text: 'relatiemodel'
        -
            key: 'taal'
            text: 'taal'
        -
            key: 'aantrekking'
            text: 'aantrekking'
        -
            key: 'politiek'
            text: 'politiek'
        -
            key: 'vooroordeel'
            text: 'vooroordeel'
    route: 'terminologie'

names:
    enabled: false

people:
    enabled: false

english:
    enabled: false

faq:
    enabled: true
    route: 'faq'

links:
    enabled: false
    route: 'links'
    blogRoute: 'blog'
    links: []
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false

contact:
    enabled: true
    route: 'contact'
    team:
        enabled: true
        route: 'team'

support:
    enabled: true

user:
    enabled: true
    route: 'account'
    termsRoute: 'voorwaarden'
    privacyRoute: 'privacy' # TODO

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: ~
            values: ['[geen aanspreekvorm]', 'dhr.', 'hr.', 'mr.', 'm.', 'mevr.', 'mw.', 'mej.', 'mx.']
        -
            header: ~
            values: ['persoon', 'man', 'vrouw', 'dame', 'gozer', 'vent', 'jongen', 'meisje', 'maatje', 'makker', 'bro', 'zus']
        -
            header: ~
            values: ['mooi', 'knap', 'schattig', 'lief', 'lekker ding', 'sexy']
        -
            header: ~
            values: ['vriend', 'vriendin', 'vriendje', 'partner', 'schat(je)', 'lief(je)', 'duifje']
    flags:
        defaultPronoun: 'hen'

calendar:
    enabled: true
    route: 'kalender'

census:
    enabled: false

redirects:
    - { from: '^/woordenboek/terminologie', to: '/terminologie' }

api: ~
