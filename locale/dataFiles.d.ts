declare module '*/config.suml' {
    import type { Config } from '~/locale/config.ts';

    const config: Config;
    export default config;
}

declare module '*/translations.suml' {
    import type { Translations } from '~/locale/translations.ts';

    const translations: Translations;
    export default translations;
}

declare module 'locale/pl/nouns/dukatywy.tsv' {
    import type { NounTemplatesData } from '~/locale/data.ts';

    const data: NounTemplatesData[];
    export default data;
}

declare module 'locale/pl/nouns/iksatywy.tsv' {
    import type { NounTemplatesData } from '~/locale/data.ts';

    const data: NounTemplatesData[];
    export default data;
}

declare module '*.tsv' {
    const data: Record<string, any>[];
    export default data;
}
