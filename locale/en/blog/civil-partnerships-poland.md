# We're consulting on a civil partnerships bill in Poland!

<small>2024-03-21 | [@Tess](/@Tess), [@andrea](/@andrea)</small>

![](/img/pl/blog/kprm-konsultacja.png)

It's a historical moment for Polish queers – a bill guaranteeing at least a makeshift replacement for marriage equality
_finally_ has a real chance of getting passed! Representatives of fifty LGBTQ+ organisations have been invited
to take part in pre-consultations about its content by the Minister for Equality, Katarzyna Kotula – 
and among them was [Anna Tess Gołębiowska](/@Tess) from our Collective!

The meeting took place on March 18th 2024 in the Chancellery of the Prime Minister, in the building's biggest hall.
It was the first ministerial meeting of that scale with the queer community –
but not the first official meeting of the new minister with queer people –  two weeks earlier she met with the foundation
[“Rainbow Families”](https://teczowerodziny.pl/) to talk about their life in Poland, their needs, and children waiting for a safer future.

We invite you to read Tess's report from the meeting:

**I believe that the meeting was not only important, but also fruitful. The Minister for Equality showed
that the voice of the queer community is important to her, that she knows our needs, 
and she takes the law on civil partnerships seriously and does not intend to accept a bill watered-down
by the coalition partners.**

The meeting of the queer community with the government at the ministerial level is new, but by no means a one-time thing –
further pre-consultations have already been announced; Until then, we as organizations should think about specific issues
so that we can consciously vote on them. We heard that we were at home in the Chancellery of the Prime Minister.
I realize that politics is politics and I may be wrong, but at this point I sincerely believe that Katarzyna Kotula
wants to fight for our rights and is outraged that they are not yet obvious to everyone.

{embed=//youtube.com/embed/Dpaj1DBG3ek=Polish news station TVN24 on the meeting}

The meeting consisted of two parts: presentation and discussion. First, people from our community, i.e. Mirosława Makuchowska
([“Campaign Against Homophobia”](https://kph.org.pl/en/)) and Hubert Sobecki ([“Love Does Not Exclude”](https://mnw.org.pl/en/)), briefly recalled the history of the Polish LGBTQIAP+ movement,
with particular emphasis on the demands of civil partnerships and marriage equality, as well as previous draft bills,
along with their shortcomings and advantages. Minister Joanna Knapińska from the Government Legislation Center, in turn,
presented what exactly the work on each act looks like, with particular emphasis on the consultation and pre-consultation processes.

From the very beginning, Minister Kotula and Magdalena Dropek from the Coordination and Processes Department
of the Office of the Director General of the Chancellery of the Prime Minister
(who together with Dawid Dobrogowski from the Office of the Ministry for Equality, representative of the National Council of the Razem party)
stated that this meeting is primarily about listening – because what matters is the demands of our community,
and queer people should say what they expect. Each person present in the room could speak on behalf of their collective, organization or foundation.

An extremely wide range of issues was discussed, both regarding the content of the act and legislative issues.
In many cases, the minister replied that she was already working on it. She did not present her own bill as part of the presentation,
as she wants to talk to the queer community first to hear our demands and only then start working on the bill.
She spoke about specific demands, and her statements showed that she would fight for this law – but to do that well
she must know what we care about the most.

Of course, our baseline postulate is simple: we want marriage equality.
But at the same time, we know perfectly well that the right-wing-conservative Sejm, in which the Left is a minority,
will prohibit marriage equality for as long as it can. We may be offended by that reality, 
or we could decide to work on the best civil partnership act possible, with the widest package of rights.
I believe that it is in our interest to have a good, refined bill preceded by pre-consultations,
and not a watered-down, meaningless act that will only serve as a “ticked off” item for the Prime Minister
to show off on the international arena.

As a person representing the [“Neutral Language Council” collective](/team), I was of course talking, among others, on issues of inclusivity –
we cannot imagine a situation in which the civil unions act does not cover the rights of transgender and nonbinary people,
and therefore also inclusive language.

Moreover, I personally consider the possibility of adopting a partner's child to be an extremely important issue –
it's not an issue that we can treat as a “controversial issue”, it is simply something that Polish society
owes to children living in rainbow families, whose rights are violated every day.
It is a scandal that when a child loses one parent, which is a huge trauma in itself, Poland condemns them
to be torn away from their family, from their parent who is a person they knows, loves and who could support them in dealing with grief.
Instead, they end – best case – with a foster family or – worst, more realistic case – in an institution.
This is horrendous and our state has no right to continue their suffering.
I hope that in 2024 this is something obvious, even for people with conservative views:
after all, the well-being of children and families is proudly carried on their banners.
A large part of the discussion was devoted to this issue.

The meeting was initially scheduled for two hours, but it was extended by an hour and a half.
At the very beginning, minister Kotula promised that she would listen to the demands of each and every person,
and despite the big delay she kept her word. One may think that it's a minor thing or that my bar is set too low,
but I believe that this is how politicians should respect the rest of society – 
since the political class has accustomed us to different standards, it is worth noting that this does not always have to be the case.

Here's a nice little detail: the catering was entirely vegetarian, mostly vegan, which –
if I understood correctly from the conversations on the sidelines – was a completely new thing
at the Chancellery of the Prime Minister. I think this is a great idea!
Meat has a huge carbon footprint and while consumers' personal choices are their business,
the government should not promote this business. A salad instead of kabanos sausages
during a meeting doesn't hurt anyone, and it is definitely more inclusive.

**The Minister for Equality announced that this will not be our last meeting – and I hope that our cooperation
will develop effectively, and Katarzyna Kotula will be our voice in the government.
She has already shown that although she is a minister who doesn't lead government administration department,
she has the enthusiasm to work hard, it is clear that she is a person who started her political career with activism.
The most important issue left to us now is to formulate the most important demands for us,
five from each organization, which, in our opinion, must be included in the civil partnerships act.
So we're turning to the Polish nonbinary community, asking what do they think is most important to bring up –
after all the act will not apply to the organizations, but to all LGBTQIAP+ people in the country,
so those organizations must speak with _your_ voice.**

— Anna Tess Gołębiowska (ono/jego, they/them)

{gallery={
    "/img/pl/blog/kprm-konsultacje-1.jpg": "! Tess and Ali Kopacz (on/jego) from the collective Równoważni_k",
    "/img/pl/blog/kprm-konsultacje-2.jpg": "! Lu Olszewski (ona/jej) from niebinarnosc.pl, Katarzyna Kotula and Tess",
    "/img/pl/blog/kprm-konsultacje-3.jpg": "! Minister Joanna Knapińska from the Government Legislation Center",
    "/img/pl/blog/kprm-konsultacje-4.jpg": "! In front of the Chancellery of the Prime Minister",
}}
