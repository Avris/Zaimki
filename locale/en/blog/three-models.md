# Three models of gender

<small>2024-06-16 | [@andrea](/@andrea)</small>

![Illustration of the three models. The first part consists of two boxes, a pink one with a drawing of a vulva and a ♀ symbol, and a blue one with a drawing of a penis and a ♂ symbol. The second part consists of a vertival line going in gradient from pink (marked with ♀) through purplue (marked with ⚧) to blue (marked with ♂). The third part is a purple icon of a raised fist.](/img/en/blog/three-models.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warnings:</strong>
    mentions of genitals, patriarchal oppression, queerphobic slurs and violence
</div>

Someone has sent us an email recently, asking for a clarification about some definitions from our [dictionary of queer terminology](/terminology).
And I realised that it's hard to answer their question without addressing the fact that people can't even agree
on _what gender is_ in the first place.

The reality is, of course, more complex than what I'm about to present, but I still think it might be helpful
to distinguish three general schools of thought.

## The patriarchy

The patriarchal system of gender is simple and definitive: **gender = genital sex at birth**.

**If you were born with a vulva, you're a woman**.
You can't change it. You're supposed to be delicate, caring, meek etc., you're supposed to wear dresses, make-up etc.,
you're supposed to marry one man, be obedient to him, give birth to his children, bring them up,
cook and clean for the family, and provide other kinds of reproductive labour.
**If you were born with a penis, you're a man**. You can't change it. You're supposed to be brave, strong, aggressive, decisive etc.,
you're supposed to wear pants, be interested in cars and sports and whatnot, you're supposed to marry one woman,
impregnate her, be the head of the household, provide for the family economically.
And **if you were born with ambivalent genitalia, you need to be “fixed”**.
If you want to be in a romantic or sexual relationship with someone of the same sex, you need to be shunned and punished and fixed.
If you don't want to pursue any relationships – or to pursue multiple consenting relationship at once – there's something wrong with you.
If you don't equate sex with reproduction, you're a slut and should be shamed.
If you don't equate gender with biological sex, you're a tranny and shouldn't have rights.

Thankfully, thanks to the women's suffrage movement, the feminist movement, queer liberation, and other struggles,
we observe a slow fall of patriarchy: with growing social acceptance and legal codification of things like
divorce, abortion, marriage equality, transgender rights etc. But this fight is far from over:
not only do some geographical areas have it way worse than others, not only do some minoritised groups have it way worse than others,
but even in supposedly liberal and progressive societies much of the old, patriarchal way of thinking is still present
in people's minds and biases…

## The gender spectrum

We often hear that catchphrase: “gender is a spectrum”. And there's undeniable truth to it:
there are men with some feminine traits, women with masculine traits, and there are people whose identity or expression
lies somewhere between those two patriarchal archetypes.
Whether such people are accepted and respected for who they are, or rather socially pressured to get closer to their
designated end of the spectrum, is a constant struggle between the patriarchy and queer liberation.

Personally, I'm not the biggest fan of seeing gender as a spectrum – because [my gender](/terminology#agender) is nowhere on it.
The spectrum is a useful simplification, but not the whole story. Still, it's a massive step forward from the strict patriarchal binary.
It's a shift from defining gender purely through biological attributes to a more nuanced view of a multitude of factors,
to more freedom in defining oneself. **You're a woman if you say you're a woman. You're a man if you say you're a man.
You are the gender that you say you are.**

Btw, I find it hilariously ironic that even the most hardline conservatives seem to _implicitly_ agree that gender is a spectrum.
After all, it's hard to argue about who's manlier than other men or to label oneself an “alpha male” or “sigma male”
without also admitting that it takes more than having had a penis at birth to be a man.
However unrealistic and harmful their ideal of masculinity and femininity might be,
their desperate attempts to keep people in patriarchal boxes also prove that the boxes have never been so airtight.

## Beyond gender

[The Gender Accelerationist Manifesto](/blog/gender-accelerationism) defines gender as a system reinforcing
the division of reproductive labour – in other words: the patriarchy. The authors of the manifesto argue that we should
**say “no” to gender**. Just don't participate in the system. Do things because _you want to_ do them,
and not because “you're a woman” or because “that's what men are supposed to do”. Be yourself, not your “gender”.

Gender is made up. While [we can't pretend that it doesn't exist](/blog/does-gender-exist), otherwise we couldn't
properly address sexual violence, inequalities or attacks on reproductive rights, we can imagine a perfect world
in which your genitals at birth have as little impact on your body, outfit, love life, sex life, social life or human rights
as the colour of your eyes. And we can push our world closer and closer towards that ideal.

## Final thoughts

The lines between those three models are very blurry. I can simultaneously admit the influence that the patriarchy has
on our perception of gender, reject that concept, and also genuinely respect the identities of those, who don't.
I often find myself defending the spectrum and the multitude of labels, while personally wishing that neither of them was necessary anymore.
People can be a gender outside of the masculine-feminine spectrum, while acknowledging it exists and not trying to abolish the system.
Many cultures outside of the Western world actually distinguish(ed) more than two genders,
but sometimes it's more fluid and permissive, and sometimes it's just a bigger number of boxes that are similarly restrictive.

But despite those complexities, I think it's still a useful distinction to make.
When someone asks me to define “a woman”, I know there are at least three vastly different ways of looking at it:
a person born with a vulva; a person that says that she's a woman; a member of a social class controlled by men.
When someone asks me what gender I am, depending on the situation I might say “none, fuck gender” and have
a lively discussion on queerness, or I can just say “nonbinary” and move on with my life,
or I can roll my eyes at the terribly constructed form, make a mental calculation which of the binary options
I'm more fine with in a given context, and pretend to be a man / a woman for a day.

Basically: most questions about gender, identities and definitions don't have a simple, straight-forward answer;
we might not like it, but we need to acknowledge that and see each issue in a broader context.
