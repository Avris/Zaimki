# Nonbinary Eurovision

<small>2024-03-06 | [@andrea](/@andrea), [@T_Vos](/@T_Vos)</small>

![](/img/pl/blog/eurovision-2024.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warnings:</strong>
    This text contains a mention of sexual violence
</div>

The [Eurovision Song Contest](https://eurovision.tv/) has long been popular in the queer community.
It's also full of representations of diverse queer identities: in terms of gender nonconformity,
[Dana International](https://www.youtube.com/watch?v=Fv83u7-mNWQ), a transgender winner from 1998 representing Israel, and Austrian drag queen [Conchita Wurst](https://www.youtube.com/watch?v=SaolVEJEjV4),
who won the contest in 2014, come to mind.

A very positive manifestation of the growing acceptance of non-binary in our society is that this year's
Eurovision can boast an unprecedented representation of openly non-binary and gender-nonconforming individuals –
as many as four countries have nominated such persons! We invite you to get to know them and their songs:

## 🇬🇧 Olly Alexander – Dizzy

Great Britain will be represented by pop music star, vocalist of the band Years & Years – Olly Alexander.

Olly comes from England, he's most well-known as the lead voice of Years & Years and his later solo career.
He is also a recognized actor, starring in a series about the HIV/AIDS epidemic in the 80s “It's a sin”.
He often uses his platform to address issues important to the LGBTQ+ community.

Olly is a non-binary person who uses [he/him](/he) pronouns:

> I feel very nonbinary, and you know, I identify as gay and queer and nonbinary <sup>[[link]](https://nonbinary.wiki/wiki/Olly_Alexander)</sup>

Olly wrote his Eurovision song together with Danny L. Harle, a well-known UK producer.
“Dizzy” is a song about losing oneself in love. It will be heard both during the first semi-final of Eurovision
and during the Saturday final.

{embed=//youtube.com/embed/mvs92WfR8lM=Olly Alexander – Dizzy}

## 🇮🇪 Bambie Thug – Doomsday Blue

Irish representation to the 2024 Eurovision will be Bambie Thug – singer and songwriter, who describes fær musical style
as ouija-pop and hyperpunk. In their work, they often tackle themes of breakups, witchcraft, and drug addiction.

Bambie uses [they/them](/they) and [fae/faer](/fae) pronouns.

Fær song “Doomsday Blue” can be described as a “genre breaker”.
In a short, 3-minute (Eurovision requirement) song, Bambie includes alternative rock, pop, and jazz.
They describe it as an “electric-metal breakdown”. The song contains numerous references to spells
including the famous Avada Kedavra. The lyrics are about an unnamed close person who raped Bambie in May 2023;
writing the song and performing it at Eurovision in May is meant to “curse” those memories and the perpetrator.

{embed=//youtube.com/embed/eA2fKlT8Khw=Bambie Thug – Doomsday Blue}

## 🇨🇭Nemo – The Code

Switzerland is represented this year by rapper, singer, and musician performing under the mononym Nemo.
Nemo's career began in 2015 with the song “Clownfisch”, and in 2017 they achieved a big success with the single “Du”.
In the following years, they were nominated for many awards in the Swiss music market. Nemo is 24 years old.

Nemo uses [they/them](they) pronouns in English, but in their native German, they prefer to be referred [by name, without pronouns](https://pronomen.net/:Nemo).

“The Code” is a song about coming out and living as a non-binary person, about the difficulties and benefits that come with it.
Nemo sings how coming out gave Nemo a sense of freedom to break the "code" of society, to be themselves.
We'll hear Nemo's song during the Thursday semi-final and probably during the Saturday final.
At the time of writing this article, Nemo is among the favorites to win the contest.

{embed=//youtube.com/embed/kiGDvM14Kwg=Nemo – The Code}

## 🇦🇺 Electric Fields - One Milkali (One Blood)

Electric Fields is an Australian duo creating electronic music that combines contemporary soul with Australian Aboriginal culture.
It consists of vocalist Zaachariaha Fielding and keyboardist and producer Michael Ross.

They describe themselves as “two feminine brothers” and both use [he/him](/he) pronouns.

> Michael: We wrote that we are “two feminine brothers” in our bio and some people questioned if we should keep it in.
> Running and throwing “like a girl” was always an insult and that’s total horse shit. Girls and woman are total bosses and we love that part of us.
>
> Zaachariaha: There’s room for everybody but the modern world loves building walls and categorizing everything.
> Am I a man, a woman, are we an Indigenous band, a queer band? All these boxes feel like barriers and we just fly right over the top on them… sorry suckers!
>
> <sup>[[link]](https://www.dnamagazine.com.au/electric-fields/)</sup>

One Mikali is a song promoting interpersonal dialogue and resolving conflicts through conversation,
praising the culture of Australian Aboriginal People (from which Zaachariaha comes).
The lyrics are in English and in the Yankunytjatjara language.

{embed=//youtube.com/embed/tJ2IaHxCvdw=Electric Fields - One Milkali}
