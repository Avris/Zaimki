# Announcing Pronouns.page Community Discord server!

<small>2024-07-25 | Collective</small>

![Logo of the Community server (PP logo on a pastel rainbow background) next to Discord's logo. Below text: discord.pronouns.page](/img/en/blog/community-discord.png)

We're excited to announce the the Pronouns.page Community Discord server!
We want it to be a safe haven for the members of the LGBTQIA+ and queer community where they can freely express themselves,
feel understood and accepted.
We're planning to host discussions, games, exclusive events, and more.
Everyone's invited!<sup>*</sup>

<sup>* with an obvious exception for trolls and queerphobes</sup>

<p>
    <a href="https://discord.pronouns.page" class="btn btn-primary d-block-force">
        <span class="fab fa-discord"></span>
        Join the server here!
    </a>
</p>
