# Living life as a Christian who is a part of LGBTQ+

<small>2025-01-02 | [@Its_LilFroggo](/@Its_LilFroggo)</small>

<div class="alert alert-info">
    <span class="fal fa-info-circle"></span>
    <strong>This is an opinion piece by a member of the pronouns.page collective.</strong>
</div>

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warning:</strong>
    This blog post addresses religious intolerance against the LGBTQ+ community
</div>

Living life as a Christian who is a part of the LGBTQ+ community is certainly difficult, but not impossible.
This was not how it has always been and to be frank, it was not easy to see my feelings for what they were until I had freedom to think.
Growing up as a Christian means you go through a teaching of the Bible or the Sacraments according to the tradition you grew up with…
Catholic, Orthodox, Anglican, Lutheran. Eventually, you finished and were sent on your way. For me, my catechesis was not all that great.
So, naturally, I stopped practising. This allowed for me to explore myself out of these boundaries and rules that
I thought were the only things that came into faith. Now before you think that this will be like the movies
where you just know you’re gay right off the bat, you’re wrong. In fact, when I first experienced a crush on the same sex,
I felt scared. Sure, I was an atheist, but the morals I grew up with were still there. I felt alone. 
I wasn’t sure what exactly I was feeling and I will just say, if you feel this way… **You’re not alone**.

The feeling? The identity? Everyone has a different story of how they realised. 
Maybe you prefer hanging around the opposite gender and shy away from those of the same gender because subconsciously,
you know you will develop a crush… Or perhaps you felt jealous or angry when you saw a female, or male, or non-binary person
kissing someone you fancied… Or maybe you enjoy wearing clothing that’s effeminate or masculine…
Everyone has a different story. Some people blend in and stay in the closet and others are more open. 
It is true that Christians may be more closed due to the fear of persecution from their faith community,
guardians or even friends and colleagues. **But this is not how it should be**.

## Why am I not accepted by those who are religious?

**You should be.** If you read the Gospels, you would realise that when Jesus said to love your neighbour,
He meant everyone and that includes the LGBTQ+ community and those who may struggle with mental or physical illnesses
and disabilities, people of different races, etc. Don’t believe me? Let’s look at the story of the encounter with
the woman caught in adultery in John 8:1-11. In this story a group of religious leaders bring a woman caught in adultery to Jesus,
seeking to trap him. According to the law of Moses, she should be stoned, but they want to see if Jesus will defy the law.
Instead of condemning her or engaging in their debate, Jesus calmly says: “Let any one of you who is without sin be the first
to throw a stone at her.” One by one, her accusers leave, until only Jesus and the woman remain. Jesus says:
“Neither do I condemn you. Go now and leave your life of sin.” In this story, it is important to focus on Jesus' words “**Neither do I condemn you**”.
Sure, adultery is frowned upon and Christian tradition considers it a moral wrong, but no Christian has the right to judge or condemn you.
Only God can judge.

And **God does not judge you. He loves you.** Don’t believe me? Matthew 9:10-13.
“While Jesus was having dinner at Matthew’s house, many tax collectors and sinners came and ate with him and his disciples.
When the Pharisees saw this, they asked his disciples, ‘Why does your teacher eat with tax collectors and sinners?’
On hearing this, Jesus said, ‘It is not the healthy who need a doctor, but the sick. But go and learn what this means:
‘I desire mercy, not sacrifice.’ For I have not come to call the righteous, but sinners.’” Jesus deliberately associates with sinners,
showing that his mission is one of mercy, healing, and redemption rather than judgement.
Christians should be treating those of our community in the same way. Why? Luke 7:36-50.
A woman known for her sins anoints Jesus’ feet with perfume and tears. When a Pharisee judges her, Jesus responds:
“Therefore, I tell you, her many sins have been forgiven—as her great love has shown. But whoever has been forgiven little loves little.”
In this, He shows the Pharisee and honestly shows us that we should love rather than Judge, no matter our beliefs.
We are not the ones who should condemn others, even if we think they’re doing wrong because there’s parts of the Bible that say so…
So what? Does that give us the right to cast them out or to condemn them? Absolutely not.
They deserve the same love and respect we would give anyone else, no matter our beliefs.
Anyone who does not give you that is not a true Christian.

## How do I survive as a Christian LGBTQ+ person?

First and foremost, know that you are loved by God as you are. Your identity as LGBTQ+ is not a barrier to His love
but a part of the unique person He created. Scripture supports this yet again. Romans 8:38-39: 
“For I am convinced that neither death nor life, neither angels nor demons... nor anything else in all creation,
will be able to separate us from the love of God that is in Christ Jesus our Lord.” Nothing, 
I repeat nothing will separate you from His love. **He will love despite whatever rules He has created or people may judge you for.** You are not alone.

There are many Christians who share your journey and can provide support, whether online or in local affirming spaces.
Such examples include: The Reformation Project, Q Christian Fellowship, Believe Out Loud, The Gay Christian Network,
Inclusive Church, Reconciling Ministries Network (RMN), etc. Further, I believe wholeheartedly that you should develop your prayer life.
Even if you’re new to this, that is okay. Again, God loves you and no one has a right to judge you.
Prayer is your personal connection to God. Bring Him your joys and struggles, and trust that He listens with love and compassion. 
Think of it as a conversation with a best friend. No need for formality. No need for words. 
You can simply sit there and be with Him. Have a lot on your mind? Cry with Him. He can take it. Don’t believe me? 
Matthew 11:30 Jesus states “my burden is light and my yoke is easy”. **You are not alone in this.** 
It’s also okay to not have all the answers. Faith is a journey, and God walks with you through every step.

However, there may be communities that are more condemning. Please don’t use these as a baseline for how all Christians act towards the community, because it simply isn’t true. You are not obligated to remain in spaces that harm your mental or spiritual health. It’s okay to step back from relationships or communities that make you feel unwelcome or unloved. But, it is perfectly okay to continue being a part of the service if it brings you peace and joy and simply praying for those who are judging you. Simply remember, God will judge them for judging too! Also good to remember that Jesus calls us to love our neighbours, but that doesn’t mean tolerating mistreatment. Respecting yourself as a **beloved child of God** is just as important. God created humans (Genesis 1:26) and He knows us inside and out. He knows us so well that He knows the exact numbers of hairs on your head (Luke 12:7).

As I mentioned earlier, **take care of your faith life**. I would encourage you to deepen your faith through personal practices like meditation, journaling, or devotional reading. I personally love going out in nature and focusing on God’s creation. This is a form of mental prayer and it is perfectly valid. Spiritual practices can help you feel connected to God even when you face challenges in your faith community. Other such suggestions include celebrating your identity and faith through creative expressions like art, writing, or music… Whatever feels right to you. Your faith and your identity are gifts. Celebrate how both shape your unique relationship with God.

One inspiring story of hope is that of Father Mychal Judge, a Franciscan friar and chaplain for the New York City Fire Department, who is remembered as the first certified fatality of the 9/11 attacks. **Openly gay yet deeply devoted to his faith, Father Mychal lived his life embodying Christ’s love through service to others.** He was known for ministering to marginalised communities, including people living with HIV/AIDS during the height of the crisis, when many in society—including some in the Church—turned their backs on them. On September 11, 2001, Father Mychal ran toward the chaos at the World Trade Center to pray for the injured, comfort first responders, and offer last rites to the dying. He lost his life while performing these acts of compassion. His legacy reminds us that being **both LGBTQ+ and Christian is not a contradiction; rather, it is a testimony to the power of living authentically in love and faith**. Stories like his inspire us to reject fear and judgement, and instead focus on building bridges of hope, knowing that our faith can be a source of strength and healing, both for ourselves and for those around us.

## Navigating Faith and Family

When you come out to your family, it’s important to remember that their reactions might be shaped by their faith, upbringing, and cultural context. Be patient, as they may need time to process. Their initial response doesn’t define their ability to grow in understanding. If it helps, you can say something along the lines of: "I know this might be hard to understand at first, but I hope we can walk this journey together with love and patience." Furthermore, speak honestly and compassionately, letting them know that you’re sharing this because you care about your relationship. Even if their response isn’t ideal, you can keep the door open for future conversations where you can talk to them more. For example, you might say: “This is a big part of who I am, and I want to be open with you about it. I hope we can work through any questions or concerns together.”

If conversations start to become hurtful or unproductive, it’s okay to step back to protect your well-being. You can also make boundaries between yourself and them. Remember: Boundaries aren’t about rejection but about fostering mutual respect. If your family struggles to understand or accept you, seek out affirming Christian communities or LGBTQ+ support groups for guidance and encouragement, which has been mentioned earlier. Additionally, you can also turn to your faith for strength and wisdom in navigating these relationships. For example, prayer can help you find clarity and peace. Have in mind that initial reactions don’t define the long-term outcome. With love and time, families often grow in acceptance and understanding. Let them know you’re willing to walk this journey together and be patient with them. Remind yourself that **you are a beloved child of God**, fearfully and wonderfully made. Embrace your identity and faith as gifts, not contradictions.

## Conclusion

Your journey as an LGBTQ+ Christian is a testament to God’s boundless love and creativity. Embrace both your identity and faith as gifts that make you uniquely you and do not hate yourself just because society does not want to accept you. You are loved by God. Remember that. Further, it’s understandable that you will have struggles in your faith journey. But that’s okay. It’s not always easy, but walking in faith and authenticity is worth it. With each step forward, you’re showing that love—God’s love—can overcome fear and judgement. Remember, prayer and community is key. Also, remember, let your life be a reflection of Christ’s love. Treat yourself and others with kindness and compassion, and trust that God is working through you. This is a reminder that the commandment of loving others extends to ourselves as well, no matter how much people may love or hate us. Finally, know that you ought to be accepted and if you’re not, please remember that you are not alone and are very much loved and accepted as who you are. 
