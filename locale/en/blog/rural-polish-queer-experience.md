# Surviving the Modern World Starts with Leaning on Each Other. Perspective of a Polish Queers Self-organizing a Better Life.

<small>2025-02-09 | [@T_Vos](/@T_Vos)</small>

![Grayscale picture of a person sitting on a ground, with their back to the camera. The only colour that pops out is a rainbow splash behind their back.](/img/en/blog/rainbow-gray.jpeg)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warnings:</strong>
    minor mentions of homophobia and abuse
</div>

This times are tough for our community. We end up being a scapegoat for all sorts of evil creatures in this world.

Why us?

We make a perfect target for the far-right propaganda. A group big enough so that the victims of propaganda may stumble upon us on the streets, yet not big enough to alone make a mess and overthrow their narrative – and yet big enough so we will put up a fight so they can nitpick our words and throw them against us.

All this is true and was true in the past. Even before the latest ‘trans-panic’ reality wasn’t good for our community. We were shunned, our needs ignored, lack of rights was ridiculed. Back then some of us wanted to ‘keep it low profile and don’t take space’ in hope that this would make our oppressors lazy and unwilling to pursue active discrimination.

Those folks were wrong. _We_ see it clearly now.

Sitting quietly doesn’t make your bully go away, standing up to them, claiming your ground and being proud _despite_ the pain is what made the sexual revolution so successful in the last decades. It is what made _me_ successful and able to thrive, to be here with you today so you can read it.

I was born in rural Poland. 1h drive to the coast, in a town of 30.000 surrounded by nothing is where I spent my childhood and teenage years. Firstborn of a very conservative, catholic, pathologic family is where I got my start. There was no formal community in at least 100km radius. No support group, no online groups, no organizations or events, no pride, nothing to counterbalance Roman-Catholic propaganda coming at me from church, school, family, TV, city council. When I was outed during my middle school, I heard from pedagogues that “have I believed in God, I would be saved in afterlife” as remedy for very real bullies that were just outside consultation room. They weren’t even reprimanded for their behavior. It was not a fun town to grow up gay.

Yet when time has come for me to explore what my sexuality means, I discovered that I am not alone in what I am, that there is plenty of us even in my small little town.

So how does one find a community in a hopeless place?

As there were no organizations and no politician thought to give us anything themselves, we turned to one another. There was no queer safe space, so we created one organically. Plenty of small informal groups like a groups of friends formed, and people were bundling up in their little “packs”. We spent time just like friends would do, but we didn’t all like one another. You knew that you’re there for some that were your friends, and those folks always kept you reeled in, but you also spent a lot of time with other queers, they just kind of were in your proximity but there was no obligation to be friends close with them.

Real ‘chosen family’ vibes. But where chosen family is extended to entire community. As a gay/queer you always had a place in this groups, some were more conservative than others, some were very artsy, some were sciencey, … But that open spot was always there.

Before I found my pack I jumped around few times between people in search of my tribe. It was a normal part of the process, people expected that you will switch as soon as it was obvious that your vibe was not matching the rest of the group. But even then, there was a solidarity, no pushing out unless you were really standing out and for some time. There was a feeling of a wider community among us. We knew that we’re just loving differently and that we’re normal folks otherwise. But that small little difference made _all_ the difference.

When I finally grew up and was able to move out of my hometown, I did. Almost all of us left. Our group was supposed to move together but it fell apart quickly as all acquaintances tend to when you all move out. Especially when life throw you to different cities. And I immediately started doing the same thing in my new place. I craved that familiar safe queer space. I gravitated towards people who seemed like a leaders in the community, now real one, open and existing. They showed me the ropes of big city life and what is what within the community. I was still living in conservative and homophobic Poland, but I was content as I knew I am not alone.

This informal groups taught us solidarity and mutual support, respect, dignity, not settling down.

And it may be what we need more these days.

Sure, we need to fight oppression but not everyone can be a frontline soldier. But just like real army needs someone to feed them, clothe them, write software for them, take care of their home while they are gone, our rebels need the same!

Never underestimate the power of a collective! I’ve seen it working well for the last 4 years on Pronouns.page and it made me think about my past. And I realized that the way that this project came to life was exactly like those informal support groups in my hometown happened. Someone had a need that wasn’t met by already existing world around us. So, they sat down to make it a reality. And then other folks from community saw it and wanted to build upon it. Person by person it was growing, slowly new languages started to appear, personal cards pulled more people in, as a result, more people wanted to contribute and like a snowball we grew, later we started helping with translations, DEI education, later we expanded it and started to help academia with their challenges. People come and go just like they did in my hometown. We help each other and try to uplift as much as we can and as much as it’s reasonable. There are no hard feelings when life pushes someone away. There is understanding. We know that we’re all just humans, we’re all just as confused and sometimes afraid. And we know that sometimes life just makes some commitments unsustainable, even if you enjoy them. I am sharing all this as an example of collective thinking. We all work as one but just as your legs don’t question where your brain tells them to go, we don’t question others on their life. Trust into one another is a key.

Just because this is an open community, doesn’t mean that there are no requirements to be part of it. And as in my hometown, this page keeps happening just because you and I are putting work towards our community, our family. Important part of this mix is to not take others nonsense but to call it out.

Now, it is important to stay realistic. Will a strong queer community solve all our problems? No.

It may even come short of fixing most of them. But to be able to tackle all the challenges that life throws at us; we need a stable shore where we can anchor. We need a place to call home, cause to fight for.

Back in my days, old-school late 90’s style of internet forum (singular) was all I could hope for. And we succeeded. Before my kid days, previous generations of queers and gays had even less resources. They still succeeded. Now with all the tools in the world, community building is easier than ever. And it reaps the benefits! So many of peers from my groups now are having nice careers, lives, their own happy queer families and futures. Myself included. This foundation given to me by community allowed me to escape abuse and -phobias to my free and bountiful life. _You too can have it all_. But not when you and likeminded people don’t survive this bigotry hurricane season.

---

Now I call to you!

I call for your story! I call for your voice!

Speak and empower one another!

We can succumb or we can thrive. Purposely at the end of this post I am putting a call to action.

Lately we published stories of some of our team-members, how they manage to navigate their queer lives
[as an LGBTQ+ Christian](/blog/lgbtq-christians) and [as a nonbinary person in the Arab world](/blog/outside-the-binary).
Tell us how you and your local community uplift one another, how you manage to survive and hopefully thrive in the face of your own adversary! Tell us about queer struggle and life in your region of the world!

Our mailbox [contact@pronouns.page](mailto:contact@pronouns.page) is open for submissions of stories of your lived experience. 
We'd love to read and potentially publish your story. You can keep it anonymous or we can publish it under your name, your call! 
But we want to hear your story and relay it further to millions that visit this page yearly.

Thank you for reading this! I appreciate that you gave me time, as it is part of my story and my life.

We love you! Stay queer!
