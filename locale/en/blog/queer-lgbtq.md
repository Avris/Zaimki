# Between queer and LGBTQIA+

<small>2024-09-22 | [@andrea](/@andrea)</small>

![Two flags waving in the wind on a blue sky background: progress pride and anarcha queer](/img/en/blog/queer-lgbtq.png)
<p style="margin-top: -.75rem"><small class="text-muted">Flag waving effect made with <a href="https://krikienoid.github.io/flagwaver/" target="_blank" rel="noopener">Flagwaver</a>. Background photo by <a href="https://unsplash.com/@sklepacki">Stephanie Klepacki</a> on <a href="https://unsplash.com/photos/white-clouds-and-blue-sky-gXG_2TpSBOch" target="_blank" rel="noopener">Unsplash</a></small></p>

I've realised that it's been a while since I called myself “a member of the LGBTQIA+” community – instead, I would just call myself “queer”.
And sure, part of the reason why is how much easier the word rolls of the tongue compared to spelling out the acronym;
part of the reason why are the constant battles over which letters should be included in the acronym and which not.
But most importantly – _those terms simply aren't synonyms_.
While there's obviously a lot of overlap between the two, I find the differences to be quite significant.
And one describes me better than the other.

But what's the difference exactly, you might ask? Well, I've been asking myself the same thing:
why does one feel so much more fitting than the other? How would I define those terms? One of them is quite obvious:

> **LGBTQIA+** are lesbians, gays, bisexual, transgender, queer, questioning, intersex, asexual, aromantic people, and more.
> It generally includes everyone who's not cisgender, heterosexual, allosexual etc.

This initialism is self-explanatory, it just lists groups of gender, sexual and romantic minorities that unite
under a rainbow flag. The sheer fact that your identity is not “mainstream” makes you included by the acronym.
And if you [read Wikipedia](https://en.wikipedia.org/wiki/Queer), you might think that queer is basically the same.
After all: “Queer is an umbrella term for people who are not heterosexual or are not cisgender.”

But some observations from my life made me contemplate the question in more depth.
First of all, my friend circle tends to gravitate around queers. Of course it does.
We share similar struggles, similar goals, similar views. They make me feel understood and safe.
There's a distinct _vibe_ between us. Among queer people I simply can be myself.

But there's an important caveat that I couldn't quite put my finger on until recently.
There are LGBTQIA+ people with whom I don't share that connection at all – and there are also people that I'm immediately
vibing with in this way, even though the question of their gender or orientation never actually came up, nor do I care.
Why? Despite a _big_ overlap between this connection and being a member of a marginalised group,
the circles don't _fully_ overlap.

I was once told, by a gay person, that if LGB people in Poland want to have civil unions
(let alone proper marriage equality), they should stop parading around their weirdness,
and instead… go volunteer their free labour at a potato harvest – and maybe then the society
will mercifully grant them some rights. Yeah, no… You do you, but I'll skip, thank you very much.
LGBTQIA+ might include everyone of non-mainstream identity, but **queer is also anti-assimilationist.**

And the other observation is how much non-queer stuff I have in common with my queer friends.
It's not just this one common denominator between all of us – but we also generally tend to be leftists,
anti-capitalist, feminist, anti-racist, anti-ableist, etc. We've been on the minoritised side of the society,
we understand that until all of us are free, none of us are free.
LGBTQIA+ might include everyone of non-mainstream identity, but **queer is also intersectional.**

Under [a Reddit post](https://www.reddit.com/r/polyamory/comments/1fados7/gender_identity_of_the_poly_pool/)
asking for thoughts on why polyamorous people seem to be more likely to be queer, I found many comments that
said basically the same thing, but this one I think put it best:

> The Venn Diagram of polyamory, kink, queerness and neurodiversity is basically becoming a circle at this point lmao. Or at least that’s how my social circle jokes about it. In my experience these are also mostly politically left-leaning folks, from liberals to socialists to literal anarchists. Also lots of disabled folks.
>
> Forms of marginalisation (i.e. distance / exclusion / alienation from Normative Society™️) tend to increasingly intersect as they accumulate. It makes “common sense” sense if you sit and think about it for 5+ minutes. Hence intersectionality.
>
> Forms of resistance to (i.e. lack of participation in) Normative Society™️ also tend to increasingly intersect with time. It’s not a big leap to question all other social norms once you’ve questioned / liberated yourself from a few.

So, with all of this in mind, I've arrived at my definition of queer.
Small disclaimer, of course: it's just my personal definition based on my understanding of the communities I'm a member of;
queer might be understood differently in different countries and communities, you might disagree with them – and that's perfectly fine.
I'm only here to share my perspective, not preach any universal truths.

So anyways:

> **Queer is active subversion of the patriarchy.**

With this definition of “queer”, things start making sense to me.
A privileged cis gay guy in a monogamous relationship who's trying to be as “normal” as possible,
to fit in and be accepted by the heteronormative society, whose fight for equal rights ends
at the bare minimum he happens to need himself, without regard for the needs of the rest of his community – that's not my guy.
He's not subversive of the patriarchy, he's assimilating into it.
However, a guy who's active in feminist circles, who's painting his nails,
going to protests against genocide in a different part of the world, participating in mutual aid groups, etc. –
I don't even care about his identity to count him as queer in my book.
He's actively subversive of the patriarchy.
I don't care about endless discussions whether polyamory is an identity or a lifestyle choice,
whether cishet polyamorous people should be considered part of the LGBTQIA+ community.
Polyamory is actively subversive of the patriarchy – it's queer in my book.

Queer is a movement, not just an identity.

Queer is a verb.
