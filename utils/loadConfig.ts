import type { Config } from '~/locale/config.ts';

export default async (): Promise<Config> => {
    const runtimeConfig = useRuntimeConfig();
    return (await import(`~/locale/${runtimeConfig.public.locale}/config.suml`)).default;
};
