import type { Toggable } from '~/locale/config.ts';

export default (configModule: Toggable<{ route: string }> | undefined): string[] => {
    if (!configModule?.enabled) {
        return [];
    }
    return [`/${encodeURIComponent(configModule.route)}`];
};
