export default computed(() => {
    if (!import.meta.client) {
        return undefined;
    }
    return navigator.platform.toLowerCase().includes('mac');
});
