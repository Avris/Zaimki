import { defineVitestConfig } from '@nuxt/test-utils/config';

export default defineVitestConfig({
    test: {
        exclude: ['new', 'node_modules'],
        coverage: {
            include: ['plugins/**', 'server/**', 'src/**', 'store/**'],
            reporter: ['text', 'cobertura'],
        },
        environmentOptions: {
            nuxt: {
                domEnvironment: 'jsdom',
            },
        },
    },
});
