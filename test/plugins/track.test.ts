// @vitest-environment jsdom

import { describe, expect, test } from 'vitest';

import { normalizeUrl } from '~/plugins/track.client.ts';

describe('when tracking', () => {
    const base = 'https://pronouns.page';

    test('normal pages are tracked verbatim', () => {
        expect(normalizeUrl(new URL('/pronouns', base))).toEqual(new URL('/pronouns', base));
    });
    test.each([
        { given: '/@example', expected: '/@--redacted--' },
        { given: '/u/example', expected: '/u/--redacted--' },
        { given: '/card/example', expected: '/card/--redacted--' },
    ])('pages containing user name are redacted', ({ given, expected }) => {
        expect(normalizeUrl(new URL(given, base))).toEqual(new URL(expected, base));
    });
    test.each([
        { given: '/terminology?filter=queer', expected: '/terminology' },
        { given: '/@example?layout=basic', expected: '/@--redacted--' },
    ])('search params are stripped', ({ given, expected }) => {
        expect(normalizeUrl(new URL(given, base))).toEqual(new URL(expected, base));
    });
    test.each([
        { given: '/faq#nonbinary', expected: '/faq' },
        { given: '/@example#they/them', expected: '/@--redacted--' },
    ])('hashes are stripped', ({ given, expected }) => {
        expect(normalizeUrl(new URL(given, base))).toEqual(new URL(expected, base));
    });
});
