import type { ConfigWithEnabled } from '~/locale/config.ts';
import { Pronoun } from '~/src/classes.ts';

const he = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'he',
    'Normative “he/him”',
    true,
    {
        pronoun_subject: 'he',
        pronoun_object: 'him',
        possessive_determiner: 'his',
        possessive_pronoun: 'his',
        reflexive: 'himself',
    },
    [false],
    [false],
    ['he/him'],
    '',
    true,
);

const she = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'she',
    'Normative “she/her”',
    true,
    {
        pronoun_subject: 'she',
        pronoun_object: 'her',
        possessive_determiner: 'her',
        possessive_pronoun: 'hers',
        reflexive: 'herself',
    },
    [false],
    [false],
    ['she/her'],
    '',
    true,
);

const they = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'they',
    'Singular “they”',
    true,
    {
        pronoun_subject: 'they',
        pronoun_object: 'them',
        possessive_determiner: 'their',
        possessive_pronoun: 'theirs',
        reflexive: 'themselves',
    },
    [true],
    [true],
    ['they/them'],
    '',
    true,
    null,
    'reflexive',
);

export default {
    he,
    she,
    they,
};

const aer = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: 'aerself',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);
const aerPlural = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: 'aerselves',
    },
    [true],
    [false],
    [],
    '__generator__',
    false,
);
const aerPluralHonorific = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: 'aerselves',
    },
    [false],
    [true],
    [],
    '__generator__',
    false,
);
const aerWithDescription = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    'Neopronoun “ae” / “æ”',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: 'aerself',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);
const aerPluralWithDescription = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    'Neopronoun “ae” / “æ”',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: 'aerselves',
    },
    [true],
    [false],
    [],
    '__generator__',
    false,
);
const aerWithEmptyPossessivePronoun = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: '',
        reflexive: 'aerself',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);
const aerWithEmptyReflexive = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: 'aers',
        reflexive: '',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);
const aerWithUnsetPossessivePronoun = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    'ae/aer',
    '',
    false,
    {
        pronoun_subject: 'ae',
        pronoun_object: 'aer',
        possessive_determiner: 'aer',
        possessive_pronoun: null,
        reflexive: 'aerself',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);

const sSlashHe = (config: ConfigWithEnabled<'pronouns'>): Pronoun => new Pronoun(
    config,
    's/he/hir',
    '',
    false,
    {
        pronoun_subject: 's/he',
        pronoun_object: 'hir',
        possessive_determiner: 'hir',
        possessive_pronoun: 'hirs',
        reflexive: 'hirself',
    },
    [false],
    [false],
    [],
    '__generator__',
    false,
);

export const generated = {
    aer,
    aerPlural,
    aerPluralHonorific,
    aerWithDescription,
    aerPluralWithDescription,
    aerWithEmptyPossessivePronoun,
    aerWithEmptyReflexive,
    aerWithUnsetPossessivePronoun,
    sSlashHe,
};
