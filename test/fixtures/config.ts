import type { ConfigWithEnabled } from '~/locale/config.ts';

export const configWithPronouns: ConfigWithEnabled<'pronouns'> = {
    locale: 'en',
    style: {
        fontHeadings: ['Quicksand'],
        fontText: ['Nunito'],
    },
    header: true,
    pronouns: {
        enabled: true,
        morphemes: ['pronoun_subject', 'pronoun_object', 'possessive_determiner', 'possessive_pronoun', 'reflexive'],
        route: 'pronouns',
        any: 'any',
        plurals: true,
        honorifics: false,
        generator: {
            enabled: true,
            startPronoun: 'he',
            slashes: false,
        },
        multiple: {
            name: 'Interchangeable forms',
            description: '…',
            examples: ['he&she'],
        },
        null: false,
        emoji: false,
    },
    sources: {
        enabled: false,
    },
    nouns: {
        enabled: false,
    },
    inclusive: {
        enabled: false,
    },
    terminology: {
        enabled: false,
    },
    names: {
        enabled: false,
    },
    people: {
        enabled: false,
    },
    english: {
        enabled: false,
    },
    faq: {
        enabled: false,
    },
    links: {
        enabled: false,
    },
    contact: {
        enabled: false,
    },
    support: {
        enabled: false,
    },
    user: {
        enabled: false,
    },
    profile: {
        enabled: false,
    },
    census: {
        enabled: false,
    },
    redirects: [],
};
