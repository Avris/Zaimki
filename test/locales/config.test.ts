import { describe, expect, test } from 'vitest';

import type { Config } from '~/locale/config.ts';
import type { PronounData } from '~/locale/data.ts';
import allLocales from '~/locale/locales.ts';
import { loadSumlFromBase } from '~/server/loader.ts';
import { parsePronouns } from '~/src/buildPronoun.ts';
import { loadTsv } from '~/src/tsv.ts';

describe.each(allLocales)('config for $code', async ({ code }) => {
    const config = loadSumlFromBase(`locale/${code}/config`) as Config;
    const pronounsRaw = loadTsv<PronounData<string>>(`${__dirname}/../../locale/${code}/pronouns/pronouns.tsv`);
    const pronouns = parsePronouns(config, pronounsRaw);

    test('pronouns.generator.startPronoun must be a valid pronoun', () => {
        if (!config.pronouns.enabled || !config.pronouns.generator.enabled) {
            return;
        }
        expect(pronouns[config.pronouns.generator.startPronoun]).toBeDefined();
    });
    test('pronouns.null.morphemes contain valid morphemes', () => {
        if (!config.pronouns.null || config.pronouns.null.morphemes === undefined) {
            return;
        }
        expect(config.pronouns.morphemes).toEqual(expect.arrayContaining(Object.keys(config.pronouns.null.morphemes)));
    });
    test('pronouns.null.emoji contain valid morphemes', () => {
        if (!config.pronouns.emoji) {
            return;
        }
        expect(config.pronouns.morphemes).toEqual(expect.arrayContaining(Object.keys(config.pronouns.emoji.morphemes)));
    });
});
