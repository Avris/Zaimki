import { describe, expect, test } from 'vitest';

import { hbsLatinToCyrillic } from '~/locale/hbs/script.ts';

describe('naski script converter', () => {
    test.each([
        { latin: '', expectedCyrillic: '', description: 'empty string' },
        { latin: 'šeststo', expectedCyrillic: 'шестсто', description: 'simple string' },
        { latin: 'Šeststo', expectedCyrillic: 'Шестсто', description: 'upper case' },
        { latin: 's\u030Ceststo', expectedCyrillic: 'шестсто', description: 'NFD -> NFC' },
        { latin: 'džepčić', expectedCyrillic: 'џепчић', description: 'digraphs' },
        { latin: 'Džepčić', expectedCyrillic: 'Џепчић', description: 'digraphs' },
        {
            latin: 'Sva ljudska bića rađaju se slobodna i jednaka u dostojanstvu i pravima.',
            expectedCyrillic: 'Сва људска бића рађају се слободна и једнака у достојанству и правима.',
            description: 'sentence, special characters left as-is',
        },
    ])('$description', ({ latin, expectedCyrillic }) => {
        expect(hbsLatinToCyrillic(latin))
            .toEqual(expectedCyrillic);
    });
});
