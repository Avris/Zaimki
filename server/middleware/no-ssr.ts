import { getHeader } from 'h3';

import isHighLoadTime from '../overload.js';

const USER_AGENT_BOTS = /bot|crawler|baiduspider|80legs|ia_archiver|voyager|curl|wget|yahoo! slurp|mediapartners-google|facebookexternalhit|twitterbot|whatsapp|php|python|mastodon/;
const USER_AGENT_BROWSERS = /mozilla|msie|gecko|firefox|edge|opera|safari|netscape|konqueror|android/;

const isBrowser = (userAgent: string | undefined): boolean => {
    if (!userAgent) {
        return false;
    }
    const isProbablyBot = !!userAgent.toLowerCase().match(USER_AGENT_BOTS);
    const isProbablyBrowser = !!userAgent.toLowerCase().match(USER_AGENT_BROWSERS);

    return isProbablyBrowser && !isProbablyBot;
};

export default defineEventHandler((event) => {
    if (process.env.DISABLE_SSR &&
        !event.path.startsWith('/card/@') && !event.path.startsWith('/blog.atom')) {
        event.context.nuxt = event.context.nuxt ?? {};
        event.context.nuxt.noSSR = isBrowser(getHeader(event, 'user-agent')) || isHighLoadTime(global.config.locale);
    }
});
