import { registerFont } from 'canvas';

import type { Config } from '~/locale/config.ts';

const fontSources: Record<string, string> = {
    'Baloo Bhaijaan 2': 'baloo-bhaijaan-2-v19-arabic_latin-{weight}.ttf',
    'Nanum Gothic': 'nanum-gothic-v17-latin_korean-{weight}.ttf',
    'Nunito': 'nunito-v16-latin-ext_latin_cyrillic-ext_cyrillic-{weight}.ttf',
    'Quicksand': 'quicksand-v21-latin-ext_latin-{weight}.ttf',
    'Zen Maru Gothic': 'zen-maru-gothic-v4-latin_japanese-{weight}.ttf',
};

const weightsValues: Record<string, string> = {
    bold: '700',
};

export const registerLocaleFont = (
    config: Config,
    variable: 'fontHeadings' | 'fontText',
    weights: string[] = ['regular'],
): string => {
    const family = config.style[variable][0];
    const source = fontSources[family];
    for (const weight of weights) {
        registerFont(`public/fonts/${source.replace('{weight}', weightsValues[weight] || weight)}`, { family, weight });
    }
    return family;
};
