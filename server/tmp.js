import './setup.ts';
import SQL from 'sql-template-strings';
import { decodeTime } from 'ulid';

import dbConnection from './db.ts';

(async () => {
    const db = await dbConnection();

    let i = 0;
    for (const { id } of await db.all(SQL`SELECT id FROM users WHERE lastActive IS NULL`)) {
        await db.get(SQL`UPDATE users SET lastActive = ${decodeTime(id)} WHERE id = ${id}`);
        i++;
        if (i % 1000 === 0) {
            console.log(i);
        }
    }
    console.log(i);
})();
