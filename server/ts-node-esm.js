import { register } from 'node:module';
import { pathToFileURL } from 'node:url';

register('./server/load.js', pathToFileURL('./'));
