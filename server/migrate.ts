import dbConnection from './db.ts';

const __dirname = new URL('.', import.meta.url).pathname;

async function migrate(): Promise<void> {
    const db = await dbConnection();
    await db.migrate({ migrationsPath: `${__dirname}/../migrations` });
    await db.close();
}

await migrate();
