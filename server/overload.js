import { newDate } from '../src/helpers.ts';

const overloadPeriods = {
    // en: [[15, 24]],
    // pl: [[0, 24]],
};

export default (locale, timestamp = newDate()) => {
    if (overloadPeriods[locale] === undefined) {
        return false;
    }

    for (const [periodStart, periodEnd] of overloadPeriods[locale]) {
        if (timestamp.getUTCHours() >= periodStart && timestamp.getUTCHours() < periodEnd) {
            return true;
        }
    }

    return false;
};
