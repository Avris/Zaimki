export default defineEventHandler(async (event) => {
    const { isGranted } = await useAuthentication(event);
    if (!isGranted('code')) {
        throw createError({
            status: 401,
            statusMessage: 'Unauthorised',
        });
    }

    const keys = (await useStorage().getKeys())
        .filter((key) => !key.startsWith('build') && !key.startsWith('root') && !key.startsWith('src'))
        .toSorted();
    return await Promise.all(keys.map(async (key) => {
        const meta = await useStorage().getMeta(key);
        return {
            key,
            size: meta.size,
            atime: meta.atime,
            mtime: meta.mtime,
        };
    }));
});
