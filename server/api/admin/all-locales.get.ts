import fs from 'fs';

import Suml from 'suml';

import type { Config } from '~/locale/config.ts';
import buildLocaleList from '~/src/buildLocaleList.ts';

export default defineEventHandler(async (event) => {
    const { isGranted } = await useAuthentication(event);
    if (!isGranted('panel')) {
        throw createError({
            status: 401,
            statusMessage: 'Unauthorised',
        });
    }

    return Object.fromEntries(Object.entries(buildLocaleList(global.config.locale, true))
        .map(([locale, localeDescription]) => {
            return [locale, {
                ...localeDescription,
                config: new Suml().parse(fs.readFileSync(`./locale/${locale}/config.suml`).toString()) as Config,
            }];
        }));
});
