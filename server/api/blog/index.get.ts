import { getPosts } from '~/server/blog.ts';
import type { Post } from '~/src/blog/metadata.ts';

export default defineEventHandler(async (event) => {
    const query = getQuery(event);
    const posts = await getPosts();

    return posts.filter((post) => {
        if (query.shortcuts !== undefined) {
            if (!global.config.blog || !global.config.blog.shortcuts) {
                return false;
            }

            if (!Object.values(global.config.blog.shortcuts).includes(post.slug)) {
                return false;
            }
        }

        if (query.slugs !== undefined) {
            const slugs = Array.isArray(query.slugs) ? query.slugs : [query.slugs];
            if (!slugs.includes(post.slug)) {
                return false;
            }
        }

        return true;
    }).map((post): Post => ({
        slug: post.slug,
        title: post.title,
        date: post.date,
        authors: post.authors,
        hero: post.hero,
    }));
});
