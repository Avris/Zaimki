import fs from 'fs';

import SentryCli from '@sentry/cli';

import './setup.ts';
import type { Config } from '../locale/config.ts';
import allLocales from '../locale/locales.ts';

import { env } from './env.ts';
import { loadSuml } from './loader.ts';

const __dirname = new URL('.', import.meta.url).pathname;

const notifyRelease = async () => {
    if (!process.env.NUXT_PUBLIC_SENTRY_DSN) {
        process.stdout.write('SENTRY_DSN is not defined, skipping deployment information to Sentry');
        return;
    }

    const version = fs.readFileSync(`${__dirname}/../cache/version`, 'utf-8');

    const sentryCli = new SentryCli();
    await sentryCli.releases.new(version);
    await sentryCli.releases.setCommits(version, {
        auto: true,
    });
    await sentryCli.releases.finalize(version);
};

const notifyDeployment = async () => {
    if (!process.env.NUXT_PUBLIC_SENTRY_DSN) {
        process.stdout.write('SENTRY_DSN is not defined, skipping deployment information to Sentry');
        return;
    }

    const version = fs.readFileSync(`${__dirname}/../cache/version`, 'utf-8');
    const config = loadSuml('config') as Config;
    const environment = env === 'production' ? config.locale : env!;

    const sentryCli = new SentryCli();
    await sentryCli.releases.newDeploy(version, {
        env: environment,
        url: allLocales.find((locale) => locale.code === config.locale)?.url,
    });
    process.stdout.write(`Sent deployment information for environment ${environment} to Sentry`);
};

const main = async () => {
    switch (process.argv[1]) {
        case 'release':
            await notifyRelease();
            break;
        case 'deploy':
            await notifyDeployment();
            break;
    }
};

await main();
