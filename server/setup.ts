import * as Sentry from '@sentry/node';

import dotenv from './dotenv.ts';

dotenv();

Sentry.init({
    dsn: process.env.NUXT_PUBLIC_SENTRY_DSN,
    tracesSampleRate: 1.0,
    beforeSend(event, hint) {
        console.error(hint.originalException || hint.syntheticException);
        return event;
    },
});
