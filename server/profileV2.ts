import type { Profile, ProfileV1, ValueOpinion, NameOpinion, CustomFlag } from '../src/profile.ts';

const opinions: Record<string, number> = {
    yes: 1,
    jokingly: 2,
    close: 3,
    meh: 0,
    no: -1,
};

const upgradeOpinionsListToV2 = (opinionsListV1: Record<string, number> | undefined): ValueOpinion[] | undefined => {
    if (opinionsListV1 == null) {
        return undefined;
    }
    return Object.entries(opinionsListV1).map(([value, opinion]) => {
        return { value, opinion: Object.keys(opinions).find((key) => opinions[key] === opinion)! };
    });
};

const downgradeOpinionsListToV1 = (opinionsListV2: ValueOpinion[] | undefined): Record<string, number> | undefined => {
    if (opinionsListV2 == null) {
        return undefined;
    }
    const opinionsListV1: Record<string, number> = {};
    for (const { value, opinion } of opinionsListV2) {
        opinionsListV1[value] = opinions[opinion];
    }
    return opinionsListV1;
};

const upgradeCustomFlagsListToV2 = (customFlagListV1: Record<string, string> | undefined): CustomFlag[] | undefined => {
    if (customFlagListV1 == null) {
        return undefined;
    }
    return Object.entries(customFlagListV1).map(([value, name]) => {
        return { value, name } as CustomFlag;
    });
};

const downgradeCustomFlagsToV1 = (customFlagListV2: CustomFlag[] | undefined) => {
    if (customFlagListV2 == null) {
        return undefined;
    }
    const customFlagListV1: Record<string, string> = {};
    for (const { value, name } of customFlagListV2) {
        customFlagListV1[value] = name;
    }
    return customFlagListV1;
};

export const upgradeToV2 = (profileV1: ProfileV1): Profile => {
    return {
        ...profileV1,
        names: upgradeOpinionsListToV2(profileV1.names) as NameOpinion[],
        pronouns: upgradeOpinionsListToV2(profileV1.pronouns)!,
        words: profileV1.words && profileV1.words.map((column) => {
            return { header: null, values: upgradeOpinionsListToV2(column)! };
        }),
        customFlags: upgradeCustomFlagsListToV2(profileV1.customFlags)!,
    };
};

export const downgradeToV1 = (profileV2: Partial<Profile>): Partial<ProfileV1> => {
    return {
        ...profileV2,
        names: downgradeOpinionsListToV1(profileV2.names)!,
        pronouns: downgradeOpinionsListToV1(profileV2.pronouns)!,
        words: profileV2.words && profileV2.words.map(({ values }) => {
            return downgradeOpinionsListToV1(values)!;
        }),
        customFlags: downgradeCustomFlagsToV1(profileV2.customFlags)!,
    };
};
