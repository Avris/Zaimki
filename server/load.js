import { pathToFileURL } from 'url';

import { resolve as resolveTs } from 'ts-node/esm.mjs';
import { loadConfig, createMatchPath } from 'tsconfig-paths';

const { absoluteBaseUrl, paths } = loadConfig('.nuxt');
const matchPath = createMatchPath(absoluteBaseUrl, paths);

export async function resolve(specifier, ctx, defaultResolve) {
    const match = matchPath(specifier);
    return match
        ? resolveTs(pathToFileURL(`${match}`).href, ctx, defaultResolve)
        : resolveTs(specifier, ctx, defaultResolve);
}

export { load, transformSource } from 'ts-node/esm.mjs';
