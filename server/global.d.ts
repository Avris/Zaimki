import type { Config } from '../locale/config.ts';
import type { LocaleDescription } from '../locale/locales.ts';
import type { Translations } from '../locale/translations.ts';
import type { User } from '../src/user.ts';

import type { Database } from './db.ts';

declare global {
    type GeneratorFunc<R, Args extends unknown[]> = (...args: Args) => Generator<R>;

    namespace Express {
        export interface Request {
            rawUser: User | undefined;
            user: User | null;
            isGranted: (area?: string, locale?: string) => boolean;
            locales: Record<string, LocaleDescription>;
            db: Database;
            isUserAllowedToPost: () => Promise<boolean>;
        }

        export interface Response {
            spa?: boolean;
        }
    }

    var config: Config;
    var translations: Translations;
}

declare module 'express-session' {
    interface SessionData {
        socialRedirect: string;
        grant: {
            instance: string;
            client_id?: string;
            client_secret?: string;
            response?: any;
        };
    }
}
