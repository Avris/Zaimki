import { eventHandler } from 'h3';
import { defineNitroPlugin } from 'nitropack/runtime';

import { closeAuditLogConnection } from '../audit.ts';
import { LazyDatabase } from '../index.ts';

export default defineNitroPlugin((nitroApp) => {
    nitroApp.h3App.stack.unshift({
        route: '',
        handler: eventHandler(async (event) => {
            if (!event.context.db) {
                event.context.db = new LazyDatabase();
            }
            event.node.res.on('close', async () => {
                await event.context.db.close();
                await closeAuditLogConnection();
            });
        }),
    });
});
