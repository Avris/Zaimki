import { defineNitroPlugin, useStorage } from 'nitropack/runtime';
import fsLiteDriver from 'unstorage/drivers/fs-lite';

export default defineNitroPlugin(async () => {
    // should be resolved when Nitro releases a nev version https://github.com/nitrojs/nitro/issues/3017
    await useStorage().unmount('data');
    useStorage().mount('data', fsLiteDriver({ base: '.data/kv' }));
});
