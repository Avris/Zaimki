const env = process.env.APP_ENV || process.env.NODE_ENV;
process.env.APP_ENV = env;

export { env };
