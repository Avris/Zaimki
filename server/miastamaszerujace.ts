import './setup.ts';

import fs from 'fs';

import * as Sentry from '@sentry/node';
import { createEvents } from 'ics';
import { JSDOM } from 'jsdom';

import { Day } from '../src/calendar/helpers.ts';
import type { MiastamaszerujaceEvent } from '../src/calendar/helpers.ts';

import mailer from './mailer.ts';

const __dirname = new URL('.', import.meta.url).pathname;

const year = Day.today().year;

const fetchEvents = async (): Promise<MiastamaszerujaceEvent[]> => {
    const events: MiastamaszerujaceEvent[] = [];

    const html = await (await fetch('https://miastamaszerujace.pl/')).text();
    const dom = new JSDOM(html
        // manual fixes for irregular HTML
        .replace(new RegExp('ref=newsfeed</a><br /><b>', 'g'), '</a></p><p><b>')
        .replace(new RegExp('<span style="font-weight: 400;">(.*?)</span>', 'g'), '$1'));
    const eventsImgs = dom.window.document.querySelectorAll('img[src="https://miastamaszerujace.pl/wp-content/uploads/2021/11/Zasob-6@4x.png"]');
    for (const eventImg of eventsImgs) {
        const p = eventImg.closest('p')!;
        if (p.textContent!.includes('Daty kolejnych') || p.textContent!.includes('Marsz organizowany przez grupę współpracującą')) {
            continue;
        }

        const [day, month] = p.querySelector('b,strong')!.textContent!.trim().split('/');
        const date = new Day(year, parseInt(month), parseInt(day));

        const name = [...p.childNodes]
            .filter((c) => c.nodeType === 3 /* text node */ || c.nodeName === 'A')
            .map((c) => c.textContent!.trim())
            .filter((t) => !!t)
            .join(' ');

        let link = null;
        try {
            link = new URL(p.querySelector('a')!.getAttribute('href')!);
            link.hash = '';
            link.search = '';
        } catch (e) {}

        events.push({
            name,
            date,
            link: link ? link.toString() : null,
        });
    }

    return events;
};

(async (): Promise<void> => {
    const events = await fetchEvents();
    console.log(events);

    const dir = `${__dirname}/../public/calendar`;
    const path = `${dir}/miastamaszerujace-${year}.json`;
    const previous = fs.existsSync(path) ? JSON.parse(fs.readFileSync(path).toString('utf-8')) : [];
    if (JSON.stringify(events) !== JSON.stringify(previous)) {
        console.log('wykryto zmiany, wysyłam maila');
        mailer('kontakt@zaimki.pl', 'miastamaszerujace', {
            before: JSON.stringify(previous, null, 4),
            after: JSON.stringify(events, null, 4),
            maxwidth: '100%',
        });
    }
    if (previous.length > events.length) {
        console.error('stopping execution, number of events decreased');
        return;
    }
    console.log(`Saving to ${path}`);
    fs.writeFileSync(path, JSON.stringify(events, null, 4));

    createEvents(
        events.map((e, i) => {
            return {
                title: e.name,
                start: [e.date.year, e.date.month, e.date.day],
                end: [e.date.year, e.date.month, e.date.day],
                calName: `Marsze Równości ${year}`,
                sequence: i,
            };
        }),
        (error, value) => {
            if (error) {
                Sentry.captureException(error);
                return;
            }

            console.log(`Saving to ${dir}/miastamaszerujace.ics`);
            fs.writeFileSync(`${dir}/miastamaszerujace.ics`, value);
        },
    );
})();
