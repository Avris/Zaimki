import localeEventImports from '#virtual/calendar/events.ts';
import type { PronounExamplesData } from '~/locale/data.ts';
import { rootDir } from '~/server/paths.ts';
import { parsePronounGroups, parsePronouns } from '~/src/buildPronoun.ts';
import { buildCalendar } from '~/src/calendar/calendar.ts';
import type { Calendar } from '~/src/calendar/helpers.ts';
import { PronounLibrary } from '~/src/classes.ts';
import { loadTsv } from '~/src/tsv.ts';

const dataDir = `${rootDir}/locale/${global.config.locale}`;
export const pronouns = parsePronouns(global.config, loadTsv(`${dataDir}/pronouns/pronouns.tsv`));
const pronounGroups = parsePronounGroups(loadTsv(`${dataDir}/pronouns/pronounGroups.tsv`));
export const pronounLibrary = new PronounLibrary(global.config, pronounGroups, pronouns);

export const pronounExamples = loadTsv(`${dataDir}/pronouns/examples.tsv`) as PronounExamplesData[];

let calendar: Calendar | null = null;

export const loadCalendar = async () => {
    if (calendar === null) {
        const runtimeConfig = useRuntimeConfig();
        const localEvents = await localeEventImports[config.locale]();
        calendar = buildCalendar(localEvents, runtimeConfig.public.baseUrl);
    }
    return calendar;
};
