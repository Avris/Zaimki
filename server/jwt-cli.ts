import path from 'path';

import dotenv from './dotenv.ts';
import jwt from './jwt.ts';

// Command-line script for encrypting and decrypting JWTs. Useful for
// debugging and testing.

// Example usage:
//   echo '{"key": "value"}' | APP_ENV=development PORT=3001 pnpm run-file server/jwt-cli.ts encrypt
//   echo 'eyJ...' | APP_ENV=development pnpm run-file server/jwt-cli.ts decrypt

/**
 * Sets up environment variables.
 */
function setupEnv(): void {
    dotenv();

    // Replace port 3000 in BASE_URL and ALL_LOCALES_URLS with port from PORT
    if (process.env.PORT) {
        process.env.BASE_URL =
            process.env.BASE_URL?.replace(/:3000\b/g, `:${process.env.PORT}`);
        process.env.ALL_LOCALES_URLS =
            process.env.ALL_LOCALES_URLS?.replaceAll(/:3000\b/g, `:${process.env.PORT}`);
    }
}

/**
 * Main function.
 */
(async () => {
    setupEnv();

    if (process.argv.length < 3) {
        console.error(`Usage: ${path.relative(process.cwd(), process.argv[1])} {encrypt|decrypt}`);
        process.exit(2);
    }

    const operation = process.argv[2];
    const input = await new Promise<string>((resolve) => {
        let data = '';
        process.stdin.on('data', (chunk) => data += chunk);
        process.stdin.on('end', () => resolve(data));
    });

    switch (operation) {
        case 'encrypt':
            console.log(await jwt.sign(JSON.parse(input)));
            break;

        case 'decrypt':
            console.log(JSON.stringify(await jwt.validate(input), null, 2));
            break;

        default:
            console.error('Invalid operation! Use \'encrypt\' or \'decrypt\'.');
            process.exit(1);
    }
})();
