export default async (name: string, key: string = 'default') => {
    // https://nitro.unjs.io/guide/cache#cache-keys-and-invalidation
    await useStorage('cache').removeItem(`nitro:functions:${name}:${key}.json`);
};
