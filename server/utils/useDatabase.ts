import { LazyDatabase } from '~/server/index.ts';

export default () => {
    return new LazyDatabase();
};
