import type { H3Event } from 'h3';

import jwt from '~/server/jwt.ts';
import { isGrantedForUser } from '~/src/helpers.ts';
import type { User } from '~/src/user.ts';

export default async (event: H3Event) => {
    let rawUser = undefined;

    const authorizationHeader = getHeader(event, 'authorization');
    if (authorizationHeader && authorizationHeader.startsWith('Bearer ')) {
        rawUser = await jwt.validate<User>(authorizationHeader.substring(7));
    } else {
        const tokenCookie = getCookie(event, 'token');
        if (tokenCookie && tokenCookie !== 'null') {
            rawUser = await jwt.validate<User>(tokenCookie);
        }
    }

    const user = rawUser?.authenticated ? rawUser : null;
    const isGranted = (area: string = '', locale = global.config.locale): boolean => {
        return !!user && isGrantedForUser(user, locale, area);
    };
    return { rawUser, user, isGranted };
};
