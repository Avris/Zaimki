import { Router } from 'express';

import { handleErrorAsync } from '~/src/helpers.ts';

const router = Router();

router.post('/sentry/tunnel', handleErrorAsync(async (req, res) => {
    const envelope = req.body.toString('utf-8');

    const piece = envelope.slice(0, envelope.indexOf('\n'));
    const header = JSON.parse(piece);

    if (header.dsn !== process.env.NUXT_PUBLIC_SENTRY_DSN) {
        return res.status(400).send({ message: `Invalid Sentry DSN: ${header.dsn}` });
    }

    const dsn = new URL(header.dsn);

    const url = `https://${dsn.hostname}/api/${dsn.pathname.substring(1)}/envelope/`;
    const response = await fetch(url, {
        method: 'POST',
        body: envelope,
        headers: {
            'Content-Type': 'application/x-sentry-envelope',
        },
    });
    return res.status(response.status).send(response.body);
}));

export default router;
