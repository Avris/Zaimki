import fs from 'node:fs';

import dotenv from 'dotenv';

import type { Config } from '../locale/config.ts';
import buildLocaleList from '../src/buildLocaleList.ts';
import { buildList } from '../src/helpers.ts';

import { env } from './env.ts';
import { loadSuml } from './loader.ts';
import { rootDir } from './paths.ts';

export default () => {
    dotenv.config({ path: `${rootDir}/.env` });
    if (process.env.__INCLUDE) {
        dotenv.config({ path: `${rootDir}/${process.env.__INCLUDE}` });
    }

    const publicKeyFile = `${rootDir}/keys/public.pem`;
    process.env.NUXT_PUBLIC_PUBLIC_KEY = fs.existsSync(publicKeyFile)
        ? fs.readFileSync(publicKeyFile, 'utf-8')
        : undefined;

    process.env.NUXT_PUBLIC_CLOUDFRONT = `https://${process.env.AWS_CLOUDFRONT_ID}.cloudfront.net`;

    const config = loadSuml('config') as Config;
    const locales = buildLocaleList(config.locale);

    const allLocalesUrls = buildList(function*() {
        if (env === 'development') {
            if (process.env.NUXT_PUBLIC_BASE_URL) {
                yield process.env.NUXT_PUBLIC_BASE_URL;
            }
            yield 'http://pronouns.test:3000';
            yield 'http://localhost:3000';
        } else if (process.env.NUXT_PUBLIC_ENV === 'test') {
            if (process.env.NUXT_PUBLIC_BASE_URL) {
                yield process.env.NUXT_PUBLIC_BASE_URL;
            }
        } else {
            yield 'https://pronouns.page';
            for (const localeDescription of Object.values(locales)) {
                yield localeDescription.url;
            }
        }
    });
    process.env.NUXT_PUBLIC_ALL_LOCALES_URLS = JSON.stringify(allLocalesUrls);
};
