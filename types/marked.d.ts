declare module 'marked' {
    const marked: (markdown: string) => string;
    export default marked;
}
