/// <reference types="vite/client" />

interface ImportMetaEnv {
    /**
     * The value of the `RUN_SNAPSHOT_TESTS` environment variable at build time.
     */
    readonly RUN_SNAPSHOT_TESTS?: string;

    /**
     * The value of the `APP_ENV` environment variable at build time. By
     * default, `APP_ENV` matches the value of `NODE_ENV`, unless it has been
     * explicitly set.
     *
     * Use `APP_ENV` when you need to know what environment the application will
     * actually be running in. In other words, `production` would indicate that
     * the application is actually running in a production environment, or at
     * least in an environment that has everything that a production environment
     * has.
     */
    readonly APP_ENV?: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}
