declare module '#virtual/calendar/events.ts' {
    import type { Event } from '~/src/calendar/helpers.ts';

    declare const localEventsByLocale: Record<string, () => Promise<Event[]>>;
    export default localEventsByLocale;
}
