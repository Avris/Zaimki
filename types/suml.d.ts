declare module 'suml' {
    class Suml {
        constructor();

        parse(content: string): unknown;

        dump(content: unknown): string;
    }

    export = Suml;
}
