declare module 'avris-generator' {
    class GeneratorManager {
        generate(country: string, generator: string): string;
        validate(country: string, generator: string, value: string): boolean;
    }
    const generatorManager: GeneratorManager;
    export default generatorManager;
}
