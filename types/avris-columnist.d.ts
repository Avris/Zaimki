declare module 'avris-columnist' {
    class Columnist {
        constructor(element: Element);

        start(): void;
    }
    export default Columnist;
}
