declare module 'avris-sorter' {
    const sorter: () => void;
    export = sorter;
}
